-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 11-Maio-2018 às 17:22
-- Versão do servidor: 5.7.19
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `suporte`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `chats`
--

DROP TABLE IF EXISTS `chats`;
CREATE TABLE IF NOT EXISTS `chats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `chats`
--

INSERT INTO `chats` (`id`, `posted`, `name`, `message`) VALUES
(1, '2018-03-06 09:14:44', 'Filipe Covas', 'hey');

-- --------------------------------------------------------

--
-- Estrutura da tabela `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `departments`
--

INSERT INTO `departments` (`id`, `name`, `updated_at`) VALUES
(1, 'Tecnologias de Informação', '2018-03-29 07:16:59'),
(4, 'Farma', NULL),
(5, 'Armazém', NULL),
(6, 'Logística', NULL),
(7, 'Recursos Humanos', NULL),
(8, 'D3P', NULL),
(9, 'Zeltica', NULL),
(10, 'Controlo de crédito', NULL),
(11, 'Administração', NULL),
(12, 'Compras', NULL),
(13, 'Técnico', NULL),
(14, 'Nutrição ', NULL),
(15, 'Marketing e Design', NULL),
(16, 'Dietética', NULL),
(17, 'Financeiro', NULL),
(18, 'Qualidade', NULL),
(19, 'Internacional', NULL),
(20, 'Técnico', NULL),
(21, 'Comercial', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `events_user_id_idx` (`user_id`),
  KEY `events_project_id_idx` (`project_id`),
  KEY `events_ticket_id_idx` (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `events`
--

INSERT INTO `events` (`id`, `title`, `start`, `end`, `user_id`, `project_id`, `ticket_id`, `created_at`, `updated_at`) VALUES
(8, 'Integração do E-dieta com Dieta3Passos', '2018-03-14 09:00:00', '2018-03-14 18:00:00', 1, 2, 55, '2018-03-14 17:18:53', '0000-00-00 00:00:00'),
(9, 'Integração do E-dieta com Dieta3Passos', '2018-03-15 09:00:00', '2018-03-15 18:00:00', 1, 2, 55, '2018-03-16 11:11:04', '0000-00-00 00:00:00'),
(10, 'Integração do E-dieta com Dieta3Passos', '2018-03-16 09:00:00', '2018-03-16 18:00:00', 1, 2, 55, '2018-03-16 11:11:34', '0000-00-00 00:00:00'),
(12, 'Integração do E-dieta com Dieta3Passos', '2018-03-19 09:00:00', '2018-03-19 18:00:00', 1, 2, 55, '2018-03-19 10:18:10', '0000-00-00 00:00:00'),
(17, 'Integração do E-dieta com Dieta3Passos', '2018-03-20 09:00:00', '2018-03-20 18:00:00', 1, 2, 55, '2018-03-20 09:58:13', '0000-00-00 00:00:00'),
(18, 'D3P - Portal Nutrição', '2018-03-20 09:00:00', '2018-03-20 13:00:00', 6, 12, 45, '2018-03-20 18:01:27', '0000-00-00 00:00:00'),
(19, 'Front End - DES Farmácias', '2018-03-20 14:00:00', '2018-03-20 16:00:00', 6, 20, 131, '2018-03-20 18:09:05', '0000-00-00 00:00:00'),
(20, 'Front End - DB3 Dietéticas', '2018-03-20 16:00:00', '2018-03-20 18:00:00', 6, 24, 132, '2018-03-20 18:09:42', '0000-00-00 00:00:00'),
(21, 'Integração do E-dieta com Dieta3Passos', '2018-03-21 09:00:00', '2018-03-21 18:00:00', 1, 2, 55, '2018-03-21 09:05:50', '0000-00-00 00:00:00'),
(22, 'configuração de equipamentos para novos colaboradores', '2018-03-20 09:00:00', '2018-03-20 13:00:00', 7, 1, 133, '2018-03-21 09:36:44', '0000-00-00 00:00:00'),
(24, 'reconfiguracao de rede armazem mem martins', '2018-03-19 09:00:00', '2018-03-19 18:00:00', 7, 1, 134, '2018-03-21 09:57:13', '0000-00-00 00:00:00'),
(27, 'abertura dos pedidos/projetos Pendentes no Portal ; organização Pedidos/projetos pendentes;parametrizações do Portal de Pedidos', '2018-03-19 09:00:00', '2018-03-19 13:00:00', 3, 1, 121, '2018-03-21 10:10:04', '0000-00-00 00:00:00'),
(28, 'abertura dos pedidos/projetos Pendentes no Portal ; organização Pedidos/projetos pendentes;parametrizações do Portal de Pedidos', '2018-03-19 14:00:00', '2018-03-19 18:00:00', 3, 1, 121, '2018-03-21 10:10:44', '0000-00-00 00:00:00'),
(30, 'PHC-Pedido de listagem - Farmácias Norte', '2018-03-20 14:00:00', '2018-03-20 15:00:00', 3, 1, 98, '2018-03-21 10:12:28', '0000-00-00 00:00:00'),
(31, 'Abertura dos pedidos/projetos Portal, configuração de utilizadores da farmodietica', '2018-03-20 15:00:00', '2018-03-20 17:00:00', 3, 1, 121, '2018-03-21 10:14:47', '0000-00-00 00:00:00'),
(32, 'Reunião com a Francisca de apresentação do Portal Nutrição, com o Nuno e o Filipe', '2018-03-20 17:00:00', '2018-03-20 18:00:00', 3, 1, 121, '2018-03-21 10:17:47', '0000-00-00 00:00:00'),
(33, 'Testes ao Portal de nutrição;Elaboração do manual', '2018-03-20 09:00:00', '2018-03-20 13:00:00', 3, 16, 141, '2018-03-21 10:24:07', '0000-00-00 00:00:00'),
(34, 'Portais Externos (3 Dietas)', '2018-03-20 09:00:00', '2018-03-20 18:00:00', 2, 16, 147, '2018-03-21 11:54:56', '0000-00-00 00:00:00'),
(35, 'Portais Externos (3 Dietas)', '2018-03-21 09:00:00', '2018-03-21 18:00:00', 2, 16, 147, '2018-03-21 11:55:32', '0000-00-00 00:00:00'),
(37, 'Manutenção de Impressora Xerox-Nutricao', '2018-03-21 09:00:00', '2018-03-21 10:00:00', 7, 1, 142, '2018-03-21 12:29:41', '0000-00-00 00:00:00'),
(38, 'configuracao de impressoras nos macs Design', '2018-03-20 14:00:00', '2018-03-20 16:00:00', 7, 1, 136, '2018-03-21 12:31:24', '0000-00-00 00:00:00'),
(39, 'Marcia Cardoso - Falta de espaço na DropBox', '2018-03-21 10:00:00', '2018-03-21 10:30:00', 7, 1, 130, '2018-03-21 12:35:31', '0000-00-00 00:00:00'),
(40, 'Breadcrumbs, Paginação (Blog), investigação Banner Casos de Sucesso e início Página Casos de Sucesso', '2018-03-21 09:00:00', '2018-03-21 13:00:00', 6, 6, 148, '2018-03-21 13:10:17', '0000-00-00 00:00:00'),
(41, 'gestão suporte, abertura pedidos do email', '2018-03-21 09:00:00', '2018-03-21 13:00:00', 3, 1, 121, '2018-03-21 17:57:25', '0000-00-00 00:00:00'),
(44, 'Testes ao Portal Nutição', '2018-03-21 14:00:00', '2018-03-21 18:00:00', 3, 16, 141, '2018-03-21 18:03:12', '0000-00-00 00:00:00'),
(45, 'Desenvolvimento Página Casos Sucesso', '2018-03-21 14:00:00', '2018-03-21 18:00:00', 6, 6, 148, '2018-03-21 18:04:00', '0000-00-00 00:00:00'),
(46, 'Portal Farmácias', '2018-03-19 09:00:00', '2018-03-19 18:00:00', 2, 16, 147, '2018-03-21 18:16:51', '0000-00-00 00:00:00'),
(47, 'equipamento para formacao de ingles', '2018-03-21 11:00:00', '2018-03-21 13:00:00', 7, 1, 145, '2018-03-22 10:39:29', '0000-00-00 00:00:00'),
(48, 'Desenvolvimento Página Casos Sucesso', '2018-03-22 09:00:00', '2018-03-22 13:00:00', 6, 6, 148, '2018-03-22 18:04:53', '0000-00-00 00:00:00'),
(49, 'Desenvolvimento Página Casos Sucesso', '2018-03-22 14:00:00', '2018-03-22 18:00:00', 6, 6, 148, '2018-03-22 18:05:24', '0000-00-00 00:00:00'),
(50, 'Integração do E-dieta com Dieta3Passos', '2018-03-22 09:00:00', '2018-03-22 13:00:00', 1, 2, 55, '2018-03-23 09:19:29', '2018-04-20 10:06:00'),
(51, 'Melhorias na Plataforma (Paginação de pedidos, pesquisa, registo actividade)', '2018-03-22 14:00:00', '2018-03-22 18:00:00', 1, 4, 173, '2018-03-23 11:19:53', '2018-04-20 10:04:09'),
(52, 'reconfiguracao de servidor de encomendas', '2018-03-21 14:00:00', '2018-03-21 15:00:00', 7, 1, 152, '2018-03-23 11:21:07', '0000-00-00 00:00:00'),
(53, 'licenca office ana correia', '2018-03-22 09:00:00', '2018-03-22 10:00:00', 7, 1, 175, '2018-03-23 11:26:29', '0000-00-00 00:00:00'),
(54, 'instalacao e configuracao de office no servidor de encomendas', '2018-03-21 15:00:00', '2018-03-21 16:00:00', 7, 1, 150, '2018-03-23 11:28:10', '0000-00-00 00:00:00'),
(55, 'Fecho de Ano', '2018-03-19 09:00:00', '2018-03-19 13:00:00', 5, 1, 176, '2018-03-23 11:58:07', '0000-00-00 00:00:00'),
(56, 'Fecho do Ano', '2018-03-19 14:00:00', '2018-03-19 18:00:00', 5, 1, 176, '2018-03-23 11:58:53', '0000-00-00 00:00:00'),
(57, 'Fecho do Ano', '2018-03-20 09:00:00', '2018-03-20 18:00:00', 5, 1, 176, '2018-03-23 11:59:13', '0000-00-00 00:00:00'),
(58, 'Fecho do Ano', '2018-03-21 09:00:00', '2018-03-21 18:00:00', 5, 1, 176, '2018-03-23 11:59:55', '0000-00-00 00:00:00'),
(59, 'Fecho do Ano', '2018-03-22 14:00:00', '2018-03-22 18:00:00', 5, 1, 176, '2018-03-23 12:00:23', '0000-00-00 00:00:00'),
(60, 'Agenda', '2018-03-22 09:00:00', '2018-03-22 18:00:00', 2, 16, 147, '2018-03-23 12:00:38', '0000-00-00 00:00:00'),
(61, 'Encomendas e Farmácia Bem Estar', '2018-03-22 09:00:00', '2018-03-22 13:00:00', 5, 1, 27, '2018-03-23 12:00:47', '0000-00-00 00:00:00'),
(62, 'Fecho do Ano', '2018-03-23 09:00:00', '2018-03-23 13:00:00', 5, 1, 176, '2018-03-23 12:01:01', '0000-00-00 00:00:00'),
(63, 'Dashborad', '2018-03-23 14:00:00', '2018-03-23 18:00:00', 5, 1, 24, '2018-03-23 12:01:16', '0000-00-00 00:00:00'),
(64, 'Reunião FM/MV/DC/EC', '2018-03-23 09:00:00', '2018-03-23 12:00:00', 2, 27, 168, '2018-03-23 12:01:31', '0000-00-00 00:00:00'),
(65, 'Alterações ao portal das Nutricionistas (Pedido por FM)', '2018-03-23 12:00:00', '2018-03-23 18:00:00', 2, 16, 147, '2018-03-23 12:02:22', '0000-00-00 00:00:00'),
(66, 'Férias', '2018-03-26 09:00:00', '2018-03-26 18:00:00', 2, 27, 159, '2018-03-23 12:02:44', '0000-00-00 00:00:00'),
(67, 'Férias', '2018-03-27 09:00:00', '2018-03-27 18:00:00', 2, 27, 159, '2018-03-23 12:02:58', '0000-00-00 00:00:00'),
(68, 'Férias', '2018-03-28 09:00:00', '2018-03-28 18:00:00', 2, 27, 159, '2018-03-23 12:03:18', '0000-00-00 00:00:00'),
(69, 'Férias', '2018-03-29 09:00:00', '2018-03-29 18:00:00', 2, 27, 159, '2018-03-23 12:05:00', '0000-00-00 00:00:00'),
(70, 'Férias', '2018-03-30 09:00:00', '2018-03-30 18:00:00', 2, 27, 159, '2018-03-23 12:05:11', '0000-00-00 00:00:00'),
(71, 'Férias', '2018-04-02 09:00:00', '2018-04-02 18:00:00', 2, 27, 159, '2018-03-23 12:05:23', '0000-00-00 00:00:00'),
(72, 'Férias', '2018-04-03 09:00:00', '2018-04-03 18:00:00', 2, 27, 159, '2018-03-23 12:05:34', '0000-00-00 00:00:00'),
(73, 'Férias', '2018-04-04 09:00:00', '2018-04-04 18:00:00', 2, 27, 159, '2018-03-23 12:05:46', '0000-00-00 00:00:00'),
(176, 'Pedido teste elsa', '2018-04-23 09:00:00', '2018-04-23 11:30:00', 1, 3, 185, '2018-04-23 09:52:57', '2018-04-23 09:52:57'),
(177, 'Pedido teste elsa', '2018-04-23 12:00:00', '2018-04-23 15:00:00', 1, 3, 185, '2018-04-23 09:55:19', '2018-04-26 16:01:50'),
(178, 'Portal Absorvit - Integração Back End Wordpress', '2018-04-26 09:00:00', '2018-04-26 12:00:00', 1, 2, 15, '2018-04-23 09:55:32', '2018-05-04 08:47:58'),
(179, 'Teste 5', '2018-04-23 15:30:00', '2018-04-23 18:00:00', 1, 2, 181, '2018-04-23 09:56:50', '2018-04-23 09:56:50'),
(180, 'Teste ao pedido', '2018-04-24 09:00:00', '2018-04-24 11:30:00', 1, 2, 177, '2018-04-23 10:00:23', '2018-04-23 10:00:23'),
(181, 'Teste ao pedido', '2018-04-24 12:00:00', '2018-04-24 15:00:00', 1, 2, 177, '2018-04-23 10:01:09', '2018-04-23 10:01:09'),
(182, 'Pedido teste elsa', '2018-04-24 15:30:00', '2018-04-24 18:00:00', 1, 3, 185, '2018-04-23 10:02:24', '2018-04-23 10:02:24'),
(183, 'Teste 5', '2018-04-25 09:00:00', '2018-04-25 11:30:00', 1, 2, 181, '2018-04-23 10:02:38', '2018-04-24 12:27:04'),
(184, 'TEste ao erro', '2018-04-27 13:30:00', '2018-04-27 16:00:00', 1, 2, 187, '2018-04-23 10:05:22', '2018-04-27 10:19:01'),
(185, 'Melhorias na Plataforma (Paginação de pedidos, pesquisa, registo atividade)', '2018-04-26 12:00:00', '2018-04-26 14:30:00', 1, 4, 173, '2018-04-23 10:05:34', '2018-04-24 10:07:44'),
(186, 'Teste ao pedido', '2018-04-25 16:00:00', '2018-04-25 18:00:00', 1, 2, 177, '2018-04-23 10:40:09', '2018-04-23 15:55:53'),
(187, 'Pedido teste elsa', '2018-04-26 15:00:00', '2018-04-26 18:00:00', 1, 3, 185, '2018-04-23 12:14:00', '2018-04-26 15:11:08'),
(188, 'Portal Absorvit - Integração Back End Wordpress', '2018-04-25 14:00:00', '2018-04-25 15:30:00', 1, 2, 15, '2018-04-23 15:57:09', '2018-04-24 12:33:40'),
(189, 'Teste ao pedido', '2018-04-25 12:00:00', '2018-04-25 13:30:00', 1, 2, 177, '2018-04-24 10:39:56', '2018-04-24 10:40:20'),
(190, 'Teste 5', '2018-04-16 09:00:00', '2018-04-16 12:00:00', 1, 2, 181, '2018-04-24 10:50:59', '2018-04-24 14:55:22'),
(191, 'crm não está a funcionar. - dashboard farma', '2018-04-25 12:00:00', '2018-04-25 16:00:00', 2, 1, 128, '2018-04-24 10:52:42', '2018-04-24 10:53:02'),
(192, 'Portal Absorvit - Integração Back End Wordpress hbdashgsahg', '2018-04-27 09:00:00', '2018-04-27 11:30:00', 1, 2, 15, '2018-04-26 10:33:03', '2018-04-27 08:42:28'),
(193, 'Teste ao pedido', '2018-04-27 11:30:00', '2018-04-27 13:30:00', 1, 2, 177, '2018-04-27 09:24:43', '2018-04-27 09:51:26'),
(194, 'Portal Absorvit - Integração Back End Wordpress 2', '2018-04-27 16:00:00', '2018-04-27 18:00:00', 1, 2, 15, '2018-04-27 09:51:45', '2018-04-27 10:18:12'),
(195, 'Teste ao pedido', '2018-04-17 09:00:00', '2018-04-17 13:30:00', 1, 2, 177, '2018-04-27 10:14:01', '2018-04-27 10:14:01'),
(196, 'Portal Absorvit - Integração Back End Wordpress', '2018-04-16 12:30:00', '2018-04-16 16:30:00', 1, 2, 15, '2018-04-27 15:28:43', '2018-04-27 16:00:35'),
(197, 'Teste ao pedido (Primeira Fase)', '2018-05-02 09:00:00', '2018-05-02 14:00:00', 1, 2, 177, '2018-05-03 07:48:27', '2018-05-03 07:55:26'),
(198, 'Pedido teste elsa', '2018-05-03 10:00:00', '2018-05-03 13:30:00', 1, 3, 185, '2018-05-04 08:35:49', '2018-05-04 08:35:49'),
(199, 'Teste ao pedido epá mesmo fixe!!', '2018-05-08 09:30:00', '2018-05-08 13:00:00', 1, 2, 177, '2018-05-07 07:42:02', '2018-05-11 09:24:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `login_attempts_user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(5, '2016_06_01_000004_create_oauth_clients_table', 2),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(7, '2014_10_12_100000_create_users_table', 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('8fe2f9a5f5d4ff704b8e99aa8ef00b38c442d0aca39b1fd15b7d8aa02468178cc37aa1df0cf527d9', 38, 1, 'awdawawdaw@awdaw.com', '[]', 0, '2018-03-28 13:22:22', '2018-03-28 13:22:22', '2019-03-28 14:22:22'),
('4e04bf2a5077b19c29ddebf3bbd7f6524b04f25a8e088468860e226215e528ca497ffa6ba6c0488b', 39, 1, 'david@farmodietica.com', '[]', 0, '2018-03-28 13:23:25', '2018-03-28 13:23:25', '2019-03-28 14:23:25'),
('cbdc84779afc7e70a7f364581f6485d74614bee8988338746598dc43c4fd99d5aa81beb46ad82236', 40, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 13:53:49', '2018-03-28 13:53:49', '2019-03-28 14:53:49'),
('cda305b659969bffaf189494d4fa69ae29b184754093208017030b1eab851bf6c219d10e1a0e4877', 40, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 14:16:46', '2018-03-28 14:16:46', '2019-03-28 15:16:46'),
('cbd40ae9b2381de7fe364efbcd9537b94a15def2661ac729518f884ecbe3009d2673a5ddc30e6a1e', 40, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 14:19:40', '2018-03-28 14:19:40', '2019-03-28 15:19:40'),
('aa13beef00a199418c43c9be309393664a0777134f1ad7aa7da33cd3da3468cbba3c44bb6d6fb822', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 15:12:00', '2018-03-28 15:12:00', '2019-03-28 16:12:00'),
('9d6f88b0fc6ba87432e5e2a57c6e65c6a82c2ca979b7c2f42c7cb90a4cdf258ea8ec7b778f4d13fd', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 15:12:48', '2018-03-28 15:12:48', '2019-03-28 16:12:48'),
('e09e238856d06ddaffc469065bc686f618e7cfb90a62271f471c148cf3f1f8a000b82840c080b4f7', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 15:15:02', '2018-03-28 15:15:02', '2019-03-28 16:15:02'),
('4b40ceea6b560fab60b6e1cf990de8a49f41ca032e4b689c491f8215ddb22b2b571fc91d55ebcaed', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 15:22:33', '2018-03-28 15:22:33', '2019-03-28 16:22:33'),
('5ace10fb63924e5e71d204c18c4d8e8c29e63d158dba2197abd94ea4dcb5ae0e555bc079ffc7d244', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 15:23:30', '2018-03-28 15:23:30', '2019-03-28 16:23:30'),
('3693e1059a1fa6e252d5960eaec384d0f384ad1cc82ce4c171837a699b2d3e6c13ec3e90b0208c82', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 15:43:45', '2018-03-28 15:43:45', '2019-03-28 16:43:45'),
('d638adcbf01a26521613fb8b759693783346dfaec8a5297943bab93b14ffb8e88671987aed71b557', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 15:50:51', '2018-03-28 15:50:51', '2019-03-28 16:50:51'),
('4fdcd26d6ef54839e02c5fedaf149a13d13cd58a43f85eaa62be06b6e030e2853c9a195c96a81efd', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 15:51:22', '2018-03-28 15:51:22', '2019-03-28 16:51:22'),
('2dd9af6bd166ea731e9bfe3588cbb954645e14a05ec2e2131e42b3e6d9fa980ee7e2b0fbb5888956', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 15:55:29', '2018-03-28 15:55:29', '2019-03-28 16:55:29'),
('100d5406fbb476f5b1ba3e4100a599aa7cac044da6ad2b047a9714fbebd3d7dd48959b4e8c6b60c4', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 16:03:22', '2018-03-28 16:03:22', '2019-03-28 17:03:22'),
('5e22853578a244ae10b513d41e5596e31294088b4a6524c4295bff15d4903e68d107437d63d8cdc6', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 16:06:47', '2018-03-28 16:06:47', '2019-03-28 17:06:47'),
('0d8c16b3eb1bdd35f0972464392a5a5974938d9408d04343d07d812c8763407d9ebd9aa883b55936', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 16:08:44', '2018-03-28 16:08:44', '2019-03-28 17:08:44'),
('0fc2c296a6d81b1ed5d3c588fadb8fd2c32caca813d51b15cedcc9810a3d5c508189515b2f9913d0', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-28 16:11:54', '2018-03-28 16:11:54', '2019-03-28 17:11:54'),
('2e59cc5d3fddedddf8fce4e406a8b12519997697015cf4bb5a1cb53b77daf8e5de40bf7982129741', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-29 07:14:56', '2018-03-29 07:14:56', '2019-03-29 08:14:56'),
('d0b122076f1f72aba37449f3d1f8ddf111d020dc2d4f5c37201271d61c2087482b72fb65c6967153', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-29 09:16:29', '2018-03-29 09:16:29', '2019-03-29 10:16:29'),
('33865cc72ce636530195655474382a9497902888a456dc80d4f5668d58ae31568240cc22d09f6ad8', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-29 10:02:12', '2018-03-29 10:02:12', '2019-03-29 11:02:12'),
('8fbf0a4151fd4e39549f457d981811526e1efac902d217d6aa1e569c5f98943f70d77292aa629038', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-03-29 16:36:15', '2018-03-29 16:36:15', '2019-03-29 17:36:15'),
('183102f31451ae9da15e83d010581bce8f5764954551fa8079ee02ac08f654b00efd2f288b829948', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-04-02 09:59:20', '2018-04-02 09:59:20', '2019-04-02 10:59:20'),
('cf0fdaddb2b79a80a2159bc083a478c84607677772c53bb46affbc10e3f91371398b249dd74e23a1', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-04-02 11:00:04', '2018-04-02 11:00:04', '2019-04-02 12:00:04'),
('3b5939f0fe67eacceb3c551c25a1e891a37ec8dac953c94a2bfe2b9c39145fcdd624ac45a9e9b8a3', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-04-02 16:11:52', '2018-04-02 16:11:52', '2019-04-02 17:11:52'),
('81c2644fdd8906839c01ba85035d11591fb9a97c0f64272a530ea666f26551ff8465d0447fc7270f', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-04-02 16:13:35', '2018-04-02 16:13:35', '2019-04-02 17:13:35'),
('09cd91f29290834e914dfef67cffd50d741e662119dc5e23750b9108fb5dcec0dc37e1f6f1ad81e6', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-04-03 10:17:04', '2018-04-03 10:17:04', '2019-04-03 11:17:04'),
('35d768ff9713dc04e0f98319db663cb9598c27899bd2db0edbbef5e0ea41cf496b84bf159c86e618', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-03 13:24:38', '2018-04-03 13:24:38', '2019-04-03 14:24:38'),
('2c542fa8c01259831118e96d306e41a61bdd8a4deb830fe1ee45f36f6b31725fd683bca354afc262', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-03 14:57:59', '2018-04-03 14:57:59', '2019-04-03 15:57:59'),
('9a4421c8839beb059c3499b54d4a0eb1988c02b1db3d7e5c1998a393219b2897d60662042c9ef707', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-03 16:03:13', '2018-04-03 16:03:13', '2019-04-03 17:03:13'),
('ec0456fde6de58d5a600575046b8786185f22b7c905ff2984b64989ad2b3f169ec8aa985a4485c14', 41, 1, 'filipecosta11@farmodietica.com', '[]', 0, '2018-04-04 16:21:37', '2018-04-04 16:21:37', '2019-04-04 17:21:37'),
('1281e9330dcdbc89041eb61b09ccce31423e036b45157690daae918bafd874558b8eb777b46f2936', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-05 07:34:39', '2018-04-05 07:34:39', '2019-04-05 08:34:39'),
('b5d4be0c2d319cd1459ae73daf0b79221debfefa23ee12f16939f9c821fbf11850f428766026e3d5', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-05 15:44:19', '2018-04-05 15:44:19', '2019-04-05 16:44:19'),
('837ac75f5c43cae89fb59848d589083acfaa5be04a0502c5a2926c726a7455906d29c410505c698c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-05 15:48:19', '2018-04-05 15:48:19', '2019-04-05 16:48:19'),
('42c958725fbf3b5674e3818dca0fd4a567f0c5e4fb26c90c4718f9310f9274da36239d2addba018a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-05 15:48:57', '2018-04-05 15:48:57', '2019-04-05 16:48:57'),
('7f8ee7ccd55f31df343a32766787b960bfdd8faae1bf465c2fb070271d34363f2e71a3aeff366728', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-05 15:52:16', '2018-04-05 15:52:16', '2019-04-05 16:52:16'),
('b8c250b646bb6e3bb9a5ec7652d43244cd528ace4de48e85bc9580892550353727f88e552393b741', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-06 12:46:19', '2018-04-06 12:46:19', '2019-04-06 13:46:19'),
('f6dfe1a3e21dd8d005bb0ec6829b1789ce7339625352a586266802e547df2f14835066b343c5afd8', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-06 13:31:48', '2018-04-06 13:31:48', '2019-04-06 14:31:48'),
('9af32dab825000b272eaf886f72ad56794d733f736c4de12009197d6061e922dbb6bbf5b544489af', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-06 14:51:23', '2018-04-06 14:51:23', '2019-04-06 15:51:23'),
('a461367d29e2153f027e7a13f3f768e7ac9724ffecea722cc6ecf7056a5361ea3061efd0414712da', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-06 14:52:21', '2018-04-06 14:52:21', '2019-04-06 15:52:21'),
('d06d24cd5348f7e90d28ebde50644dcdf9deea731071353332875a5a2b1d3040f57f94e0ae5c23a9', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-06 15:29:36', '2018-04-06 15:29:36', '2019-04-06 16:29:36'),
('2463a93f4d2eee825375f889e500a31269675d2c0ec888c6e4dd4af1b3b2456d44fbbc07ee4101b1', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-18 15:15:48', '2018-04-18 15:15:48', '2019-04-18 16:15:48'),
('abdd98e696848ff103cee6ad084e091f56737f7e53b9cfe860cb1a5aff1970e8be6dc97729cac42d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-18 15:16:25', '2018-04-18 15:16:25', '2019-04-18 16:16:25'),
('9cdb112f983831ae3da9467804e4123d4d0e1006c69d4df5016deab7a6c21910e0de7f6a1bd67943', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-23 11:01:57', '2018-04-23 11:01:57', '2019-04-23 12:01:57'),
('ba91f23c6525b625cdfbf0ac278a2afa45c8e06572e331c77ede13f66e69461974c63f6acdbdae3a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-23 12:10:14', '2018-04-23 12:10:14', '2019-04-23 13:10:14'),
('24ad6b9063be1841d7c27387d4080644c4468c521a9dc47bbdc1c54ab65bccca09e61d8f0f5d28ac', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-23 12:12:42', '2018-04-23 12:12:42', '2019-04-23 13:12:42'),
('a555b58e5ff306018fc6cfe725ddfa3e5289e0a662e5270f09b35464f193e4ef9a2908e4230de466', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-23 12:44:46', '2018-04-23 12:44:46', '2019-04-23 13:44:46'),
('3c8e439a73a13b17b41a5dd89105730820f713a5f9d04ba99993260fb950d7d73228e6cc611af3cd', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-23 12:57:17', '2018-04-23 12:57:17', '2019-04-23 13:57:17'),
('6b89b56aa336b4bf5656eb0c01318ebb02bfc08d6b49c19d7c2682929bccc02fd77520a7af8b37fd', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-23 13:00:57', '2018-04-23 13:00:57', '2019-04-23 14:00:57'),
('d9b774ac4952c6c57626b3b4fe0fd924cac496745dc614b756ef70de1b98542c8cc6ad683e840aa8', 2, 1, 'nunolopes@farmodietica.com', '[]', 0, '2018-04-24 10:52:27', '2018-04-24 10:52:27', '2019-04-24 11:52:27'),
('b723da9ba28af4c2a70f65e5195a021255b78ced895769109b76b2952cd243e9c03484f885f3dfdc', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 10:53:16', '2018-04-24 10:53:16', '2019-04-24 11:53:16'),
('d89722bb608341b49a74608d93610abb1b4f6e6cc96b3bc44ea72178080c072593d037cb713b6dbc', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 13:24:02', '2018-04-24 13:24:02', '2019-04-24 14:24:02'),
('50b8b56f9221e408235c64e76aa9a4ba1851d5603de1448223fce47a6372de5d3d85a0deda5aa50b', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:08:42', '2018-04-24 15:08:42', '2019-04-24 16:08:42'),
('d1c02eb11afecf0d5b2e903ef4a0f65fe8725d6d1a537d4a5e974cf5f73c7ed662aa62b2814490d7', 2, 1, 'nunolopes@farmodietica.com', '[]', 0, '2018-04-24 15:15:26', '2018-04-24 15:15:26', '2019-04-24 16:15:26'),
('3aba7442c22b5f7d205e62d5ed39754ffb9d5041f403978835074f604fdcdaa1e1fe15111862ca3c', 2, 1, 'nunolopes@farmodietica.com', '[]', 0, '2018-04-24 15:16:10', '2018-04-24 15:16:10', '2019-04-24 16:16:10'),
('218619c9084bfe375bd026f5da76dfa31d049332fc0989fa36d84a154b415832af7faec887ec4444', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:16:28', '2018-04-24 15:16:28', '2019-04-24 16:16:28'),
('19c61ae4ac7bc89f8a550a25cd6a8cee48b98b427cff1cf41ad0099c0a37ebb50539768e5e937926', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:37:40', '2018-04-24 15:37:40', '2019-04-24 16:37:40'),
('45353f81350509cad5ab6728ca7cd2fd231f18caf44d76f12b40304377174abc2e7832a127742598', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:38:38', '2018-04-24 15:38:38', '2019-04-24 16:38:38'),
('8a1a9646ae49ea38929d73afa2f3ea38e86d5e5486cdb7f1d4b27ba64acfd6d831fd11a0b529e103', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:43:16', '2018-04-24 15:43:16', '2019-04-24 16:43:16'),
('03f4a8c1bc3273ef642e9fd14180d45fbd42de74b465c8baa5f8c37dda5e248585ad5c2c37d7f216', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:45:40', '2018-04-24 15:45:40', '2019-04-24 16:45:40'),
('170d0efc0fd943d08cec56345912d9d56a67efd87c06a1e34bf23856f51f894ff5b62535cbb6eabe', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:45:49', '2018-04-24 15:45:49', '2019-04-24 16:45:49'),
('27f933634547c8bd2fe0d0eb2205b7b6ec26df5e1ac1a39a1e8585cbc905243770c59d4a7015e73a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:46:03', '2018-04-24 15:46:03', '2019-04-24 16:46:03'),
('9633298f5c228ebc2bcb5a588f03200ebf07b8167c9a82e2c9bc95a9be837a461a519226398c5c0a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:50:22', '2018-04-24 15:50:22', '2019-04-24 16:50:22'),
('d2e1cf18714723a20f2dbb6fab5c45195e07215ab8034811b416c590eac1f11c63a627bce5354732', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:51:54', '2018-04-24 15:51:54', '2019-04-24 16:51:54'),
('5cd26d8e8a40ecb810d693dedd5c5ffd0a8c2e35bfa859fd3441e1660fb37fd04c4feafb8b60a135', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:52:15', '2018-04-24 15:52:15', '2019-04-24 16:52:15'),
('ab4d49ea582a82e6356f08ee87d9b8aea2d01500c4f4e0d49106a4b1b92d27af409aa62f6a1ecf3f', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:53:18', '2018-04-24 15:53:18', '2019-04-24 16:53:18'),
('0f29ef745931555a9948a71d94aa2ea0b0b11c67d55397ebf52348356d2e378b9ecdf68f23027f13', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:56:44', '2018-04-24 15:56:44', '2019-04-24 16:56:44'),
('9776cea05facfae5aed4b3f3f87a7361cc35f77743f0b9721eeafab3190b142eb76072a8546ab7bd', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:56:48', '2018-04-24 15:56:48', '2019-04-24 16:56:48'),
('f4b577a90a59d3fd8a2c484fed26d95ef889ae1496e39564bdade47c4d612e056c30bf8935c7e750', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:56:55', '2018-04-24 15:56:55', '2019-04-24 16:56:55'),
('376f649de2300b3fa2d395a6ea9fc3890c434fea605936f57ee5590c551a3574e2abf986a3a92f5f', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:57:03', '2018-04-24 15:57:03', '2019-04-24 16:57:03'),
('b585eaf1756a4906a3fe63b0c60ab1846f825de75337c67226914c9f346562c8093b510e263cbee2', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:57:50', '2018-04-24 15:57:50', '2019-04-24 16:57:50'),
('ddc6742e7c5a9ca427deec4152bbee9a30c3c72d3ec40b78e1aa9df84f5c5574e2184621c06c4385', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:58:13', '2018-04-24 15:58:13', '2019-04-24 16:58:13'),
('3ede4a1d38f1a5b891ea6202f408c62cba5db823674e7a6d8343b4863fe209f55a1babf2ac2abd46', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 15:59:27', '2018-04-24 15:59:27', '2019-04-24 16:59:27'),
('e013658564731cb861ff48d11aae3e5f19170fbbda479d8d7e992eafc073da728ab29bca8fa5f980', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:00:09', '2018-04-24 16:00:09', '2019-04-24 17:00:09'),
('c52d23fae29781c7d54c0d6bfcfae5c95f88ca98c16c690db814d4edf8c328d26435c4b9239cf44d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:00:22', '2018-04-24 16:00:22', '2019-04-24 17:00:22'),
('4244526b2faa7cceba050a1f642aa0c7b4d2217a0f0c3cf38dc8ce00bbea9723043894883c35d79b', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:00:31', '2018-04-24 16:00:31', '2019-04-24 17:00:31'),
('1acfe42265e2e1f5df56f015f55dd66e962c49ca8abe0b10242f2ec6cd3c83860aada5e4064af06c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:03:11', '2018-04-24 16:03:11', '2019-04-24 17:03:11'),
('8c081368efbc31f23cc5ff7544303214f6041b43753c110218abcb982d380554d682622b250063ec', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:04:15', '2018-04-24 16:04:15', '2019-04-24 17:04:15'),
('add82ce4a09c862dbb50ec5e463af65cec34998fd13def1cad1c84e5860def40dbe9ae4f61b830ae', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:04:20', '2018-04-24 16:04:20', '2019-04-24 17:04:20'),
('fb7775b4afe12641c5cb86cec05d1442fadd87e490d1842feb1827dd811c4a63888cdfb2f6bfb6db', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:04:27', '2018-04-24 16:04:27', '2019-04-24 17:04:27'),
('bbe60087b6040f83c24cae8bbeb4d3d01c018a7194767a9c8415e60e5229ee4e033ca792ce4038a5', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:04:35', '2018-04-24 16:04:35', '2019-04-24 17:04:35'),
('2f07380478b81f8d209f6d5678296e00d0a78b380cefb491445d53ae0bce9fa1bfc45bf8c7d96dc7', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:05:17', '2018-04-24 16:05:17', '2019-04-24 17:05:17'),
('b65847f1eb95b70e82f712bfd54b26f73762a1ed2dc54bf01245db111e7b9c464a5c32df029e4c4b', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:05:25', '2018-04-24 16:05:25', '2019-04-24 17:05:25'),
('efd74e37e149f3067a34b6cdac38d9dfab5b8797e65644b6afe3865da124f5090a2a98ec6e44eaa2', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:06:01', '2018-04-24 16:06:01', '2019-04-24 17:06:01'),
('6f5fdff901b550b725b76ff81d25d527dcfc73bda9413e443434b3ca4ec79fa5e160ef2d3dd0b1ca', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:06:11', '2018-04-24 16:06:11', '2019-04-24 17:06:11'),
('31214022f32d204cda721dc6e7f4d56dfddd765526d5c5602b25a8db2406cdc6e9f9f2ff64ca9c43', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:06:19', '2018-04-24 16:06:19', '2019-04-24 17:06:19'),
('105ac1bb53c80a514cdbfea346f826620eaeaa9f7ab2ddef09a70c5499609d99a64d053cd54ed0a1', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:06:30', '2018-04-24 16:06:30', '2019-04-24 17:06:30'),
('8dfea2575b50eb3f7c6cdb081df538358bcfd18469d8d56edb046f7079388bf20e2b67e545b39225', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:07:00', '2018-04-24 16:07:00', '2019-04-24 17:07:00'),
('6c99dbb6725b89bb2691fafc7b482066760be630371b39c7901a1e54ab3e36a1179e94a6a8bbe74c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:07:06', '2018-04-24 16:07:06', '2019-04-24 17:07:06'),
('25ca6fd943c330b3725edf77183224ee55f23bd277ac0e7cf730cccaf3784960b36b7a84a7661bd7', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:07:11', '2018-04-24 16:07:11', '2019-04-24 17:07:11'),
('d89fda60c5c8562c39063083951fbeefcffbf5d533bebcf0bbc7906e9b192137e2a9f9e86da7a9ca', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-24 16:11:20', '2018-04-24 16:11:20', '2019-04-24 17:11:20'),
('dfd4d10140eddf47428939aa6862fc5cf880c8ce038bd9694ccab6ed27b280a88131c13c6ffb411e', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-26 09:52:14', '2018-04-26 09:52:14', '2019-04-26 10:52:14'),
('728eb82af2a00fe70f5a7e5ae21c91e7b9b1baa1ef73864b76c93ce6e64ca8e3427e7d59dc4bf61b', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-26 10:38:53', '2018-04-26 10:38:53', '2019-04-26 11:38:53'),
('8a89cc236260a82a624f189d6d63675a7c2c1c5d3cc0559fcba4aa4554dcd6746fb2334790576687', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-26 10:39:05', '2018-04-26 10:39:05', '2019-04-26 11:39:05'),
('17903ca1689ffff584e6876cbfed2644f627c47ae585d7c079f87d9240823dd2f67109e4dc293fd0', 2, 1, 'nunolopes@farmodietica.com', '[]', 0, '2018-04-26 10:39:09', '2018-04-26 10:39:09', '2019-04-26 11:39:09'),
('44bb7376cb5b0c95d5f7a2d2d7aba54571d74538276b28991234e21d50c3080ffaaf79eac37b76ee', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-26 10:54:17', '2018-04-26 10:54:17', '2019-04-26 11:54:17'),
('369b1573784f5f19ff22773a6738e483c964a1d40df6f51210f71cbee01a2f870505fcb117133a27', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-26 10:54:21', '2018-04-26 10:54:21', '2019-04-26 11:54:21'),
('7e06204ed4ccb6e930ac21f26dcf86c3357faa700b408b9df51c218b9266c85a03aee04d3a22ff69', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-26 14:21:57', '2018-04-26 14:21:57', '2019-04-26 15:21:57'),
('b07d37de8662c17c21a6c757b51e7c7fc3bd575501b7de21cd4f1d0580b7aa4390e13d567f474769', 7, 1, 'ricardonunes@farmodietica.com', '[]', 0, '2018-04-26 14:49:22', '2018-04-26 14:49:22', '2019-04-26 15:49:22'),
('e0cfe06420f9d513cfc3461df611d8f6103cc0d85b1aa331eb0fe00058ad009d11031f6bc9593fba', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-27 07:56:36', '2018-04-27 07:56:36', '2019-04-27 08:56:36'),
('9c88fbc46396201e1a5f5c55f9c9060d8964a27caa8ce67ec77c55d74466535774e373f54c010089', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-27 08:04:41', '2018-04-27 08:04:41', '2019-04-27 09:04:41'),
('5d4bc211dc7b6b25221191f7fc2e7df6f079a6d61ff207a1e8c9a133084f542a1f71559ebc6e6fcd', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-27 08:36:35', '2018-04-27 08:36:35', '2019-04-27 09:36:35'),
('7a887df5a339c9134cb4e7da7cdad9266845501b22cbce4a1a72f186b8209c044663200de4296639', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-27 08:41:32', '2018-04-27 08:41:32', '2019-04-27 09:41:32'),
('bfcaca5161a7d034535105166b2d4c9c8f0504a902b45a8d772c9b75b229b2025da5a49c4e494105', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-27 08:41:42', '2018-04-27 08:41:42', '2019-04-27 09:41:42'),
('8f63e2643d6f6804adce6049df2d836b99fc33b9d291b0d7d28f1a86279fca3bb80512937f4aa3f0', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-27 08:42:13', '2018-04-27 08:42:13', '2019-04-27 09:42:13'),
('e75e15fddb27ed55b360468aa2f0d6c35a33c0c1d47d29f9bcd692f4cf2c4f82827f002793cbb534', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-27 09:21:03', '2018-04-27 09:21:03', '2019-04-27 10:21:03'),
('03a4fb4bb87e4ec15f57817797b3e3ba0539293453debb53fedd011338d0b62c35799829c79d35a8', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-27 09:21:13', '2018-04-27 09:21:13', '2019-04-27 10:21:13'),
('82d770356f6c7a78c17c475e5bf6f214f8a2def90384c3e855c3095d1741a7292f0692f7040325a9', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-27 10:01:28', '2018-04-27 10:01:28', '2019-04-27 11:01:28'),
('98403ced150859b79399993653bc5f617e973b60fb9d8be1b54c870ea900b28f8d0640dab3e9a2ce', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-27 10:10:17', '2018-04-27 10:10:17', '2019-04-27 11:10:17'),
('9f802943f0409fcd50b6864a11d52bc8bc911d8a586f82933eb88eabee61665b8f12adceaf084ce5', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-04-27 10:10:23', '2018-04-27 10:10:23', '2019-04-27 11:10:23'),
('36ab2d90a7633a6d1860b5ee132cf8fda6176ef9da33dc4db92333e145de2349ab93ab7a19b61395', 2, 1, 'nunolopes@farmodietica.com', '[]', 0, '2018-04-27 16:00:03', '2018-04-27 16:00:03', '2019-04-27 17:00:03'),
('84017e2ca964ff56c37596fa62c97e19f87270ef6e853ef217487d10eefddef4d217d85376e406b4', 3, 1, 'elsacosta@farmodietica.com', '[]', 0, '2018-04-27 16:00:19', '2018-04-27 16:00:19', '2019-04-27 17:00:19'),
('5cd6b7330d8ea3b5fff8207fea325b416c497d1868cb32956c77d23535f9075a8d1f14eecea2a249', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-03 07:56:42', '2018-05-03 07:56:42', '2019-05-03 08:56:42'),
('4babe5bf0c8ed9e7086198aa63c1b93e7525fc99d09e231df1cccd3dd03525ddc45b2f210d0b590d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-03 12:44:28', '2018-05-03 12:44:28', '2019-05-03 13:44:28'),
('b4dce097971478fe279b3bdee35e2199e748efe5c658a5969241f7b10feca452f37cac9b3e667285', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-04 07:59:54', '2018-05-04 07:59:54', '2019-05-04 08:59:54'),
('bbecfa4b5203482038d215710f2773ea37298d4ee96aff3d1969aa1b87653874f035c450a0f2a75a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-07 07:43:39', '2018-05-07 07:43:39', '2019-05-07 08:43:39'),
('ad3cf8f3b8801e3c41e61891d9247476cce39e189f0d904be21c44fad5b42a46858839d7ea64655d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-07 09:24:31', '2018-05-07 09:24:31', '2019-05-07 10:24:31'),
('d8afb2a59ae4501debef7ea9190aeb31cd92dc96c476815349a07e2b4d9bd265eacc42430fe0fd9f', 2, 1, 'nunolopes@farmodietica.com', '[]', 0, '2018-05-08 10:43:08', '2018-05-08 10:43:08', '2019-05-08 11:43:08'),
('517c5fb15249f03897b03d5162bf8b4551def5ab318e016f32f01ac0823e4a3f165cbbf6dc0a7207', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 10:43:30', '2018-05-08 10:43:30', '2019-05-08 11:43:30'),
('adbe9dac61d547053c9a524b07026e3c7522e0af593cb452d335522effdf79b9ff90873ffb693f52', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 10:50:01', '2018-05-08 10:50:01', '2019-05-08 11:50:01'),
('dea9d4eeef7e62ca11f6c4f7491ef8921c011af4eb2a447fbda08863a5e75854667c038d65ea5290', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:10:24', '2018-05-08 14:10:24', '2019-05-08 15:10:24'),
('700dea0e76c06cd5194a68fd52351c3da37b28382ab00be15ba39e0f40bbcd3fff8b5bdb516d98ac', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:12:41', '2018-05-08 14:12:41', '2019-05-08 15:12:41'),
('022bdb586709e2bb7e139ab75924b2474f7a3d5645a58c3eb035e3ec1747057c080760e8e4899aad', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:44:15', '2018-05-08 14:44:15', '2019-05-08 15:44:15'),
('b00ea5a0b72fe31db01e25890c619509c42f75c651d95423a8a9a47e9ca23fc1f57d45527dd9c628', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:45:30', '2018-05-08 14:45:30', '2019-05-08 15:45:30'),
('818a8d7ee02316e58074b1c3326e88a41d1f04fa7762b5161ddcc786ff5ac5193e9243794691972c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:52:49', '2018-05-08 14:52:49', '2019-05-08 15:52:49'),
('a5b2bd780610ef0836645f088b1e0bdb70abf4502de77f2c8552ccc9b6e4a98af810a0f993a46b4d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:53:56', '2018-05-08 14:53:56', '2019-05-08 15:53:56'),
('d417e60bef462999ac38f2d0ef9e1e4cb4e084fe709803da50ae0ffb049975cbec5be1c9ed51d0fb', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:54:11', '2018-05-08 14:54:11', '2019-05-08 15:54:11'),
('bce36cfa28efdb95cc865b5d204aa3835a655064300d6bfe8f87242af96b23c3d6bd0a98da3739ea', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:54:19', '2018-05-08 14:54:19', '2019-05-08 15:54:19'),
('4f80258af4aac11cc9a59351ab1cabdf7aa83872c20e515e277539c190a9cd7fdc9f5332f5d8f867', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:54:56', '2018-05-08 14:54:56', '2019-05-08 15:54:56'),
('4abea82cfe444317474243b124a96d455d819a8401de85374d9d7dc33e07fc29f7cde6f5425a18e2', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:55:32', '2018-05-08 14:55:32', '2019-05-08 15:55:32'),
('d53fe2dbd92f809e5b31aa7528c6becf1623f819de4b556ec1bf974ed56b720d009a704e4db48e20', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:55:40', '2018-05-08 14:55:40', '2019-05-08 15:55:40'),
('53ce8031aab4ff40bce65a6faf3b894906d5063a49b5ac21218c6dc5d43a260de2f7ee3725742a27', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:57:15', '2018-05-08 14:57:15', '2019-05-08 15:57:15'),
('6d8dad329011b9237a6e44cf48f65113f930564f60335fac997cc2f6448dcac47d8d77c858fef126', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:57:36', '2018-05-08 14:57:36', '2019-05-08 15:57:36'),
('b290019c06564c9b6aca9b42582b4b869a5791f4d64ddd30cb1a32d760b25eb5e492ced12ff02378', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 14:57:41', '2018-05-08 14:57:41', '2019-05-08 15:57:41'),
('9d36221a737b3cc900ba0773dbe667fc4dbbd946dd949867eecf68339dadedd3dce3e5304d51a0e6', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:00:13', '2018-05-08 15:00:13', '2019-05-08 16:00:13'),
('1ea61e9f4ba4b33f0d8ae7f47dc0905a3db1c70c1f13b77a3a455e5d29424eec76d7b99aa1b49eb8', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:00:18', '2018-05-08 15:00:18', '2019-05-08 16:00:18'),
('ef42337007e017130febc2350f83211f460f51a5ecc6435bfad6eaa139633c81d43baf08df6f8b42', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:01:17', '2018-05-08 15:01:17', '2019-05-08 16:01:17'),
('fe568a72712a1e010b41dba57cbbf33a95e61c2bf8d0096ac1e70ca8e687f4a3e2f6d9b1a227cd32', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:01:20', '2018-05-08 15:01:20', '2019-05-08 16:01:20'),
('fa7e716eef24e639ecd0956f2684c9bd3e7fa3285c058194c0f45502c052630626125dbf11796c10', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:01:30', '2018-05-08 15:01:30', '2019-05-08 16:01:30'),
('ceec2721f719fa6c4abdfb33a739e06f4e001003960f136cdc875e71f35745c92d4fde34cd4d0558', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:02:08', '2018-05-08 15:02:08', '2019-05-08 16:02:08'),
('e39162ec048f397065b907a5f31349896629740981daed576717ff0c79ea0f51cc8f7306a4fa2e62', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:02:47', '2018-05-08 15:02:47', '2019-05-08 16:02:47'),
('d6b7e48512a302db1b7d49deecc8cc33f76fc4807f298c1a2f28b8fb4a2350df3d8ce631fbfb137b', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:05:38', '2018-05-08 15:05:38', '2019-05-08 16:05:38'),
('c842319afb025cd3049b39c192fce996eae02f157233fd145949bb6c85a99d8440cc20477c390cae', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:05:43', '2018-05-08 15:05:43', '2019-05-08 16:05:43'),
('0feedf30a352376ba91b0c77cb13393f8be655baee2ba85ae6e11fdc6378f274f276e5bdcdbff44b', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:06:31', '2018-05-08 15:06:31', '2019-05-08 16:06:31'),
('c2561e5be3028a1853db3a8f06b4e202b03d8b8f1a61fb5e1736d5817d580a05171e831f145c7c84', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:06:35', '2018-05-08 15:06:35', '2019-05-08 16:06:35'),
('f467d350ad9f47dca278522526423bb2be862fe0e8ce8ea14ea34821f43bf5d133c8f885ee4bafd8', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:07:08', '2018-05-08 15:07:08', '2019-05-08 16:07:08'),
('b40fddd85c998364096e6d7c1af03215d76fb4191f0f6e482cc70ed6cdc39ff62825e2d9608bc98d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:07:11', '2018-05-08 15:07:11', '2019-05-08 16:07:11'),
('88d96fe325cef2c9107e36a71eedd72abbb22693424816fcfdde9ac66ac3dae93b2d487823e7f91f', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:07:33', '2018-05-08 15:07:33', '2019-05-08 16:07:33'),
('fe3e805cad48ebc9e23bfa6d8a9334b2d0701cb2f9d126a0ce70157b6cac3ccc9b93fbbf284dbccd', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:07:38', '2018-05-08 15:07:38', '2019-05-08 16:07:38'),
('a824247b06a01ded408edefcae969d0f3e4d109ea7eabdab7620672a5ffec12147dd6e688f39cafe', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:08:03', '2018-05-08 15:08:03', '2019-05-08 16:08:03'),
('d413605a4d333decc61bb37d87712140d8ff219534445d7226da729bdf287d6bda3bf07c302f4b22', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:08:34', '2018-05-08 15:08:34', '2019-05-08 16:08:34'),
('410cb3ad536a7b1ef7100c782a4455292b4b2ac0eaf31ffad6bdfe8707b73bc026574264805fe386', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:08:47', '2018-05-08 15:08:47', '2019-05-08 16:08:47'),
('536f7ca89260f37c5ab815588f5783f898e375c0792052eeb03529a92b0a68fbe3a57fc2d3e2120b', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:08:51', '2018-05-08 15:08:51', '2019-05-08 16:08:51'),
('163ecc96d2ffe248becafdda2b9d35c657396a1070cf034d2a0f1b57441f6fdba93f5f22ca826f49', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:09:34', '2018-05-08 15:09:34', '2019-05-08 16:09:34'),
('d0d80a7f45a81d0b70b9f581f4bf753c98554dcc41976a4ab7ac7203fb53b94aebb2845a7d67931c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:09:49', '2018-05-08 15:09:49', '2019-05-08 16:09:49'),
('466e7d15a6815f11dd8971b1746b4c43f131dfa7292d74acfc896ed1a3b3c431e36399d4231e4e33', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:22:27', '2018-05-08 15:22:27', '2019-05-08 16:22:27'),
('3a36f857a1391e341fe916d1530fba5619a709763b1f10702e74e82f9140b554a1ab4c65e39579d0', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:24:21', '2018-05-08 15:24:21', '2019-05-08 16:24:21'),
('f4a31eae8ab15e76313c098398f22f18a49c43e2a2c50e13e5061b7d6cc043b4b361ddc231887318', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:24:29', '2018-05-08 15:24:29', '2019-05-08 16:24:29'),
('980315d34064cfd44c7f0dd7c292f4187c07fef5904bb9be0016b9019c5bfc241425e7801255446c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:24:51', '2018-05-08 15:24:51', '2019-05-08 16:24:51'),
('13688d4dc9ffd4a95d1fc86c8dcbdb14ccfdadb629aef5853bc86b40484bc1c362bbd3e3ec7075df', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:26:13', '2018-05-08 15:26:13', '2019-05-08 16:26:13'),
('f082c789cd9f4f25bd789665abdc607dfd614d733fe1db0d16f307f5f9e7a40eac0f1a17c591097f', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 15:53:54', '2018-05-08 15:53:54', '2019-05-08 16:53:54'),
('4a1328dd9483ea117c7febf13c805e1bde2f26c8c9d338033d50d7b48bd835629e1807f63675c04f', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 16:11:48', '2018-05-08 16:11:48', '2019-05-08 17:11:48'),
('80a0c21c9b5dc8ba3fc22b58e743ee2711d9d8cfc821013abf8b6efe5540d6ddb572fbd2ae8663b3', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 16:12:00', '2018-05-08 16:12:00', '2019-05-08 17:12:00'),
('bef9769a7b2404b20348aa05ce39c030b32359ef12bbd26d34f3e6c0bdafd7c6d158c1e7ffd813c7', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 16:12:45', '2018-05-08 16:12:45', '2019-05-08 17:12:45'),
('6177f8b519eca05cb70c76b25fff74274042dd59836c276c0e3b91d34107dc0816de0bb3c4dda3b7', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 16:13:37', '2018-05-08 16:13:37', '2019-05-08 17:13:37'),
('f521f9c9542aade68dee5f6a9410dfe932ba15eb38ec68c51a54124d8221c289d7a53bc1e9102e90', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 16:15:07', '2018-05-08 16:15:07', '2019-05-08 17:15:07'),
('c117ed48ed3f0560e705c5f974a528adeb99e18f79bf6b878919b9217d4acda9e12629e67041fbc8', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 16:15:25', '2018-05-08 16:15:25', '2019-05-08 17:15:25'),
('d84581361ddc500ef15858ce948c769291f5115b98bae3825773b8178b752553be9e24a99eeb7086', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-08 16:20:39', '2018-05-08 16:20:39', '2019-05-08 17:20:39'),
('f3d85969a915b03e590d60d9bfcc3cffd273387f49fcb639703c9c60a9bb4a2a98465f768132e43c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 07:41:41', '2018-05-09 07:41:41', '2019-05-09 08:41:41'),
('bcf581a1b0f3c61455f98a1ec871fac2eda14b36ff135133f4126dad72623115dd62afcb22e04f5f', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 07:57:27', '2018-05-09 07:57:27', '2019-05-09 08:57:27'),
('4e698445679c233274f2f4d0910d126fe3e72c594914687e0a9bc66a6d1d1c738085c51cce328696', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 07:59:00', '2018-05-09 07:59:00', '2019-05-09 08:59:00'),
('f64c390f182c187c73ee6532781081340a1a9e8f5503c7dce308bcadd40b8e12da2f8ff73bad60af', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 07:59:10', '2018-05-09 07:59:10', '2019-05-09 08:59:10'),
('7e8c04a209d0ea002e8790f5aa86ca04be1a35f4dceab6d0b6cba1b0c91fdcaa820ddd98992fdc10', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 07:59:18', '2018-05-09 07:59:18', '2019-05-09 08:59:18'),
('107d974dd29be0ebc60383c406717eab0251c7ea515a919bef1fcf820d6634885717d2590f541dc4', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:27:18', '2018-05-09 08:27:18', '2019-05-09 09:27:18'),
('f7d27c48c58dd13187bf1391d808364340128a6abc0dc638b98c4864ca757a128610b18131f0fe3a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:27:47', '2018-05-09 08:27:47', '2019-05-09 09:27:47'),
('c21fd6ddc604cd997ec0a48ba77081b6fab33ec1ab4ac0f651cd50d9bc8ff4ee448aaaaa2f416c94', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:29:16', '2018-05-09 08:29:16', '2019-05-09 09:29:16'),
('d1a68cfb3095c866543eba6339127322ba7e046e9db5c94fa6383825016536e34da1352d054dcd77', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:29:31', '2018-05-09 08:29:31', '2019-05-09 09:29:31'),
('8559cf9008b3cc63eb60d068de12a4b292a86e3e47c210b8a81d2333d055ac4cee282d19cdd18292', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:32:17', '2018-05-09 08:32:17', '2019-05-09 09:32:17'),
('f7c3eafba3b205d431eee190b1aa87fced0a60e09319ec53305205617207b979c9a2c87fdf917df2', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:32:33', '2018-05-09 08:32:33', '2019-05-09 09:32:33'),
('2620099ab46b6760b03acc50b9b5bc054020235fc083325b90fb3157043b8df7e441cc6de84ac30e', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:34:27', '2018-05-09 08:34:27', '2019-05-09 09:34:27'),
('4139c0cdef3dfecb5fa82b5993c03a2a241d855e91f4e854f45ab323078bde22b506ca6bb3e7c6ba', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:34:41', '2018-05-09 08:34:41', '2019-05-09 09:34:41'),
('45b39b2606a67e6ac37fdecd38039f899aca005e8690d4f3c214976b429c0703c1ecde575cdf27ee', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:36:28', '2018-05-09 08:36:28', '2019-05-09 09:36:28'),
('705d18760f86996231fed2ac0d262ef5637f5d40bec251b4f2550aef5bdd95cf2efce8b56e98ce42', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:39:29', '2018-05-09 08:39:29', '2019-05-09 09:39:29'),
('3853740e85979f0975c08e5f778fed8b3923f96cf80c131e23def4a0321cf08bf769397804c14188', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:40:15', '2018-05-09 08:40:15', '2019-05-09 09:40:15'),
('5b02673f081fedd3b77d7747402889c5898ecabb4c90a27a2ef8d0a6578fc64f4e1b0bd19a27285c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:40:32', '2018-05-09 08:40:32', '2019-05-09 09:40:32'),
('eebddd920a5c7dcbdd1455e9ac080138ae8294ccdfb1caedee2cc689c8527aeb579c04957a53cc38', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:40:49', '2018-05-09 08:40:49', '2019-05-09 09:40:49'),
('e17cd4bc8e11f6fa24d3f64aa483d6465b80b05bf76c56ff3bf83b3da87070e44adedc99b36fcc93', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:42:27', '2018-05-09 08:42:27', '2019-05-09 09:42:27'),
('4f851f8346da60b92fc39c0d0b35f3ec67506fcd4459cf33c18ab25f8756ab0e18ef361d752e8b5a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:42:56', '2018-05-09 08:42:56', '2019-05-09 09:42:56'),
('aa52e5fd633ca25c923905eeae8e6d6663c9fc0be65a9f4654817fa5ac80ed025b57c979d0b7466d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:43:49', '2018-05-09 08:43:49', '2019-05-09 09:43:49'),
('9c38798f9c831bca9966221af79d8e942ffbb76c4f7569f294cb10cbcfcf0cd48ba1a35bc5f83543', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:44:18', '2018-05-09 08:44:18', '2019-05-09 09:44:18'),
('5ab2f4f363400eadf537514ff7dec4e83acd99e88ab9a41366d094aafcd7bb629ac635db6737accf', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:44:38', '2018-05-09 08:44:38', '2019-05-09 09:44:38'),
('8d86998304f9f9005e160ab6bcd096558f3bd4e27ecb801dddb67b64ca19e05191ab83487aec6ccf', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:45:10', '2018-05-09 08:45:10', '2019-05-09 09:45:10'),
('d1cf06d3303692f7852860ff418600953a01f0227314e461b1fb3c76268f70335e63131286c7d916', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:45:38', '2018-05-09 08:45:38', '2019-05-09 09:45:38'),
('baa86a3e726cb0d366fd42f1ddb8b8edde2732f3a9c91b3a65136bb290265ef56430fd8cd933a786', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:47:01', '2018-05-09 08:47:01', '2019-05-09 09:47:01'),
('a5a6ad885f1df5461a2674747e6051bbad35639895cd9f3b45fa0213c04b50d76409134050891d04', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:51:15', '2018-05-09 08:51:15', '2019-05-09 09:51:15'),
('ec17545acda9542707e6eebc7deebf650c3c7beae83137978b20a9c76a5f806b1ddf94a413a2d252', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 08:51:39', '2018-05-09 08:51:39', '2019-05-09 09:51:39'),
('aaeb40dfd1b1311bd3a7ba302a5f0ebe58efad8c501940f29ce2468296347b4746c19a7dbc31c193', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:15:47', '2018-05-09 09:15:47', '2019-05-09 10:15:47'),
('95fa5970d8754632df4f4f5bf1f19acc821faafa15707ab46f8333266d5ea2a2274072f4e7a5c129', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:15:56', '2018-05-09 09:15:56', '2019-05-09 10:15:56'),
('cb76d8b44103a5aa2f7fb5332ae2fbfe95d04e897804886f0ed30eece0513fc32fef39801c23da36', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:16:53', '2018-05-09 09:16:53', '2019-05-09 10:16:53'),
('df60f5d7876998f789bd5270612ac5925f2a0da876f68f77966add5a23e64bd7e07b06e994bc0398', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:22:42', '2018-05-09 09:22:42', '2019-05-09 10:22:42'),
('d28d32abfc5e3295dc05683a77e49cc053a9c70b7867d9101db9c96fbc65a3142bb8366c484a3bf5', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:23:43', '2018-05-09 09:23:43', '2019-05-09 10:23:43'),
('7d7cb18f03485f947497ef2648e155aed4ca75b0fb146b8fe736318c0295f2a0037e084aa6ae4362', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:24:09', '2018-05-09 09:24:09', '2019-05-09 10:24:09'),
('548ee258ff1d7632c099a5587c863dd2d1e7a29888f5fe171831fead201836769e102d576d362b69', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:24:35', '2018-05-09 09:24:35', '2019-05-09 10:24:35'),
('91082fee6f5fe0141265a6cee19b55e12853b7d437338572ddef0ee0e51cbf4392dd1e70ac1b7fdc', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:33:46', '2018-05-09 09:33:46', '2019-05-09 10:33:46'),
('def9b0bfd26b64cd90ae824ef2b64c4b73872642f7c69c8343e5556cb24166a86028d3b68603bdf1', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:35:19', '2018-05-09 09:35:19', '2019-05-09 10:35:19'),
('9afa693f0f989d6c2ec3a0c7aec502b2b84859c20878beb1db6e2595bf77b9716d2bc9d982fe8f9a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:39:38', '2018-05-09 09:39:38', '2019-05-09 10:39:38'),
('58a79ee1f31f9fbc3f2bedeabfcfccfde6a41c21e0614dc7f662c173260589e7156ab147df80eff8', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:39:43', '2018-05-09 09:39:43', '2019-05-09 10:39:43'),
('daebdfedbae96d567d807efa9a57b2e98322a2142779cdfa39ead9a922ed600927166d5ab6e5b43e', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:39:51', '2018-05-09 09:39:51', '2019-05-09 10:39:51'),
('8483627d6fc2a710bad973ca3f8a0912b11402712f0d992263b7187ab14847db296e50e4b3fab558', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:41:11', '2018-05-09 09:41:11', '2019-05-09 10:41:11'),
('059a3a4bf02429cd14a4d2fea344de1d1dc23345743f0de6b87d108f42ef76d5cf52cde1feefe177', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:41:27', '2018-05-09 09:41:27', '2019-05-09 10:41:27'),
('b05aca1ec3dfeb7e3f59c36734aca6f7bda2f1d025c3a2a5057d76be462231941eea1603b7f25dcf', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:41:38', '2018-05-09 09:41:38', '2019-05-09 10:41:38'),
('dcd64d1557440dde5784829f661307369b96125682bcb8f6fe17a54048111446b9907014860a0e13', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:41:47', '2018-05-09 09:41:47', '2019-05-09 10:41:47'),
('769c183d94aaf259749f9ee600c7444cfff826b03ba20438aeaadc3417c9e679c22f4d23681f6d06', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:44:10', '2018-05-09 09:44:10', '2019-05-09 10:44:10'),
('6348db04af450e8949b569950ce42aed7459e246c5dc93250974cd3847818b542343f4b155fce4c3', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:44:40', '2018-05-09 09:44:40', '2019-05-09 10:44:40'),
('814e01335e0a2a8a85e6e1054c4585054cf9ed2822b4640aea3ba73ef49c695ddbe07c1e76a18b60', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:47:49', '2018-05-09 09:47:49', '2019-05-09 10:47:49'),
('b8dc3535ff39069e2764fe85828a8a3ffad21bbc27e2b4a51bceb5974cdc71186ccd0b3b9232351d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:47:52', '2018-05-09 09:47:52', '2019-05-09 10:47:52'),
('7ac07737bcf6867b1556f994c3cd14d9f1b704eac8b9ebdf3b5e81c4df6b636427c54c64aed12a82', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:50:11', '2018-05-09 09:50:11', '2019-05-09 10:50:11'),
('77def12daccf6e614d8e8ca3f3d3da0f086dc3f3e82dac3f25bd635a9c66eb9a25702d74f0902d58', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:52:09', '2018-05-09 09:52:09', '2019-05-09 10:52:09'),
('8a5aa63dda712a922c3852913a6c5df246524531fb35d743f504379f025289eb9f38f7be71cf0153', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:54:09', '2018-05-09 09:54:09', '2019-05-09 10:54:09'),
('7f8eafc5b810cc041435f4566b63901b0e0a2dd7f55f7afb00f14788b4e1b5c59a7e6de4a184dad1', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:54:13', '2018-05-09 09:54:13', '2019-05-09 10:54:13'),
('65f4636d5fac37effde789da59372ff35890088792acc69c54748ef431748046e1bd963915e9dd97', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:55:01', '2018-05-09 09:55:01', '2019-05-09 10:55:01'),
('351e313bf456a384f0cdf91ee2645968a2809b87369f073e2e73d1ad06a54758b916e0f32bf15ffb', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:55:48', '2018-05-09 09:55:48', '2019-05-09 10:55:48'),
('0ebe91e158718f70974ea63ed5938ab0708a5243f720ce5e7cc6769b7fa716cd7dccb9c0e30dc547', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:56:53', '2018-05-09 09:56:53', '2019-05-09 10:56:53'),
('02cf2594eb267f6375c3825b8c8d1c4fb000c67a9b72890b65b90fffb2c148b8dc94e57c358be8fe', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:57:10', '2018-05-09 09:57:10', '2019-05-09 10:57:10'),
('f960a4ae7e3ef2be51bd843a01a2d38ce87b180795d1c891f9cf30d3296fff0bada4ddf791af15e6', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:57:21', '2018-05-09 09:57:21', '2019-05-09 10:57:21'),
('5a8079f24625788e33853d12853dee26604fb63b37b05bfffd06cb7e5284ac6f6e90ccaf5d9c2792', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:57:35', '2018-05-09 09:57:35', '2019-05-09 10:57:35'),
('b7c93f9b70a99a0318610feed16c2203cb5622cc8373d5b85e2fd81f6aca8e12a27b7e4452eb6412', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:58:51', '2018-05-09 09:58:51', '2019-05-09 10:58:51'),
('a0b9093dcc705f551e2a60e7c15dd40ed6a5c881b74aee0f7511251a33f662cde45262c8efefd7f3', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:59:09', '2018-05-09 09:59:09', '2019-05-09 10:59:09'),
('579202f90b1ae3a65533174410bb436365c0462e5a6179427bfca2b53414989d605fba8c3212fd01', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 09:59:16', '2018-05-09 09:59:16', '2019-05-09 10:59:16'),
('d947981aa7b932b7b9740ed3451015235588c34a57aacf0e627319ef074def6c4f8536a193ebf6ac', 2, 1, 'nunolopes@farmodietica.com', '[]', 0, '2018-05-09 10:01:00', '2018-05-09 10:01:00', '2019-05-09 11:01:00'),
('fe7a2c18a9d81eb40d9edebe532b00aca58e4479b1976b2c075555ead6890b38791d7e60534db33c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:01:05', '2018-05-09 10:01:05', '2019-05-09 11:01:05'),
('28b6174280fd8fcae3af9925100f5b331d1f8d0b67f722abae480105b49b677e579b7c65d3190b58', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:01:19', '2018-05-09 10:01:19', '2019-05-09 11:01:19'),
('24ddaa645cfa99a19860ea5746e85d82c3103ceee069af1df6beb04f533e74ffa8d14dc6318f1a0a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:01:30', '2018-05-09 10:01:30', '2019-05-09 11:01:30'),
('fe237a2bdb06666080ce550b1f7ee2d64145de70102e3fe2bb357c75e85e68ac6ee54637b63a525c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:01:36', '2018-05-09 10:01:36', '2019-05-09 11:01:36'),
('2acd54c616cefc1f2a1cdbd83d7bdecf65c310d628a6e9870c57d88ff633858c70db79090f8e9f87', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:04:20', '2018-05-09 10:04:20', '2019-05-09 11:04:20'),
('215447f36faa97114a2a99b294ce5373d6fc10af14a5a9bae576542c49bea0b0b9fa6f03f3539cdd', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:05:06', '2018-05-09 10:05:06', '2019-05-09 11:05:06'),
('80a1b0e757d877a5aa5235a1f32ee3ab5be9920cbd8665f269474b5d12bd9a9a2a665cbba8e50263', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:05:23', '2018-05-09 10:05:23', '2019-05-09 11:05:23'),
('ae12315b84b12a1dcdff0c908001cb4e0985b8cbb4cdb83d2794758dc90b901a286861e7d942481a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:05:41', '2018-05-09 10:05:41', '2019-05-09 11:05:41'),
('97ebda852afb97b48ec0d21cf4f1b1b71d91d95807b94ecb07b400584f611ee2d77ef3fa88726c92', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:05:57', '2018-05-09 10:05:57', '2019-05-09 11:05:57'),
('656e3beb92b3160e452c53ec72b37102d5633a34b297bbd35b731aad5cd527f8c761e069d92bab71', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:06:25', '2018-05-09 10:06:25', '2019-05-09 11:06:25'),
('0d67c801aeefd025078707439b0fe59976f608c7961552522b88594ad2848ff9fd9b346cf3b0c79b', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:07:17', '2018-05-09 10:07:17', '2019-05-09 11:07:17'),
('3cc2ed9570c902cb3ab526912e849748b4dc9b2e48a40542c3d2d50fcd508b0d5e120f7cbc2e6f44', 2, 1, 'nunolopes@farmodietica.com', '[]', 0, '2018-05-09 10:12:28', '2018-05-09 10:12:28', '2019-05-09 11:12:28'),
('f1281daa92c3a4371ecb5cae9e6a07b198c26c3672131e8699b0bd17cd88c94273c258f7eb8e579b', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:20:19', '2018-05-09 10:20:19', '2019-05-09 11:20:19'),
('d6aa59f52bd4314da2904dc56fd96a5edc795fdd38641a96a5cf8b09f8a99b39eed048a65f0b0193', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:20:25', '2018-05-09 10:20:25', '2019-05-09 11:20:25'),
('8a1eb9315cb38a44995278ee57e155db55175b471eab5d34f789df0a7cc91ee3b8908b03e3ce751a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:20:31', '2018-05-09 10:20:31', '2019-05-09 11:20:31'),
('60eb76dd1e5b474c53dac3cae0eb7c68f839284bb00857dc877c6dc3ebf645edd9cc2c18f7a26e61', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:20:39', '2018-05-09 10:20:39', '2019-05-09 11:20:39'),
('817c7f42ac7a6e2a69574a5b6a0e0d0b2000e7257d876cd98e223399cfdfcea4ad702f382cf3800e', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:21:37', '2018-05-09 10:21:37', '2019-05-09 11:21:37'),
('5f6786c253147dea00bf36c3556dad968d367fb2d0c181accc89eb84cdec6e3913c79f77fdd199d2', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:21:57', '2018-05-09 10:21:57', '2019-05-09 11:21:57'),
('13b7257a0c7bb96e29337c173819cfd58022c67ae609d8c3df58a20ebb82ed45603dd3be97abc48e', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:22:27', '2018-05-09 10:22:27', '2019-05-09 11:22:27');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('22d0d97e550e3e9da5702c5e859cd274c7fa9316cdea72fdc7a1d8735c86c1132247830ea774b7e9', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:22:34', '2018-05-09 10:22:34', '2019-05-09 11:22:34'),
('29eff6788f65b919adef58f2595a0e42a99375e1f3b0068206af04e0cfab54cf695a7b576c3fd01a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:23:22', '2018-05-09 10:23:22', '2019-05-09 11:23:22'),
('59ebdcabb2c4f4557b74d8e73833a0fa74be8f31a6bb26fe577b05b8f3d7881cbf97a7ef0917a6e0', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:23:45', '2018-05-09 10:23:45', '2019-05-09 11:23:45'),
('4942de4ba075cc6edd36cc6fbfd48e3db20b179448e79596ab9d143cfd15c6d7f9b4d08cdbe09629', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:24:34', '2018-05-09 10:24:34', '2019-05-09 11:24:34'),
('42d40e961469bd23c75d532d83d33cef49d4744c91dac054fe181945e52da85adb5021b27a843936', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:24:53', '2018-05-09 10:24:53', '2019-05-09 11:24:53'),
('b03bda03ddc6d8a1b78daba815095abfef2b6c0e4b17225cb370714b4cefcb03ffc151d59ff319a5', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:26:36', '2018-05-09 10:26:36', '2019-05-09 11:26:36'),
('dc0ed58b560c392001bdeec4139e32e1a92ca95c027680756d9f08805288e6e3b7757b4d409ec00b', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:27:12', '2018-05-09 10:27:12', '2019-05-09 11:27:12'),
('1b806ac22601ae75e704e47798e986bf57ab1794e68770eea1c07daa0dfec014b32abd8c80df899e', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:27:47', '2018-05-09 10:27:47', '2019-05-09 11:27:47'),
('44035bf2231f58e0f9118be39e39a2207fa2d314a0fddf8adcf3e49e53ffe74ce924696c37209aaf', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:28:01', '2018-05-09 10:28:01', '2019-05-09 11:28:01'),
('41c621799c6decc39f0114ce183893d630da27116eb6284b1df1e7b88287ba24a3405f44aa34313c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:28:08', '2018-05-09 10:28:08', '2019-05-09 11:28:08'),
('f5f6062b1b36891aaad03544a7518d9f78ad9eafe3d97d34c9b1e82d028fb306a49021f97a55cce9', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:29:27', '2018-05-09 10:29:27', '2019-05-09 11:29:27'),
('a636a91a82d2ca2c49efc7790e3dbedc34fafd540f3d51c188a2cc8140571950e29e9d3cba846040', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:29:35', '2018-05-09 10:29:35', '2019-05-09 11:29:35'),
('12dda38fe262357414a73964f18adf3ab9270c68bae9a54cf87a9cda1f592033062a1a2abc25ed36', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:29:46', '2018-05-09 10:29:46', '2019-05-09 11:29:46'),
('956ee44f842f8c28cb9bd3604f1237884ad7bfd99f8d76262c31ee87af3e45a20e1f20563b6d51b0', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:30:40', '2018-05-09 10:30:40', '2019-05-09 11:30:40'),
('dbc015b8f9cf82754aa7d9cf33386cc08a99da2e11101b55533eb677c3079e5755893fe206170714', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:30:44', '2018-05-09 10:30:44', '2019-05-09 11:30:44'),
('fd6326d1fe70ff15bb94486058cea1b47da42ef1be0fee9b0077465e261821a8d281eff70b7d6679', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:30:59', '2018-05-09 10:30:59', '2019-05-09 11:30:59'),
('ef73965dbcf517c58a4a8f6ef066fe8ef1924ed4b77be3a1005aff2b9a3fbea8a0a38c5ad77fe698', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:32:16', '2018-05-09 10:32:16', '2019-05-09 11:32:16'),
('e1467f8bd0b12db692451aa34a5653da302e563ac029e16d5eeed53fbfce3cbaa6e12731d1734728', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:32:29', '2018-05-09 10:32:29', '2019-05-09 11:32:29'),
('031ed916872534b6bb510a433b266fbf749ed2a920c8cda1ad951323703d7183bb189f5c007e6e86', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:33:00', '2018-05-09 10:33:00', '2019-05-09 11:33:00'),
('229033ef88fb9b9a35bbe4b585b92888450e320e2e6d19a99ada4e7e13f01595f621f9598d2e922e', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:33:15', '2018-05-09 10:33:15', '2019-05-09 11:33:15'),
('3f358cca4e2679eb830ea7f789b4da2fa0098259d0c96cd29c9116e6b812e6bfc455acd136a58fd6', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:33:51', '2018-05-09 10:33:51', '2019-05-09 11:33:51'),
('342d1827d7befe8b4eada12a5e31c1f2b6515a6123fc96a012febe011eabfef5ed56b8492371248a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:34:33', '2018-05-09 10:34:33', '2019-05-09 11:34:33'),
('7e293d271a758121c003a08ceb5347447ecf6d62b0a048db66f360da8ea69c3b40ff463bddb258b5', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:34:58', '2018-05-09 10:34:58', '2019-05-09 11:34:58'),
('4c18834a5b693e2515d1edad0694d59b6ce0989eecee05a21e9e3922498339f5c30aaa7a142df010', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:35:14', '2018-05-09 10:35:14', '2019-05-09 11:35:14'),
('656aa8ce91e2aa10e966029d9090d209777f6c036c50c7ce4d9bed905cf2b64f0c0f4186ea60e83d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:35:29', '2018-05-09 10:35:29', '2019-05-09 11:35:29'),
('d5ed3b9aac39ffb505710f80234cd1bd37a67c3d492641c5772a4e08b9da39e42ac187238cc5a723', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:35:33', '2018-05-09 10:35:33', '2019-05-09 11:35:33'),
('4333763a05f93379013559143b0ce4a7b681a2a95210301e33bf289c6784a92ca9224b0303f18c30', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:35:35', '2018-05-09 10:35:35', '2019-05-09 11:35:35'),
('c8311b2a564baa2b1ea7d6faf75a4b33bf9f9bb1970b7238c120c73ce98712b91aa33d8cf88cbb67', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:36:49', '2018-05-09 10:36:49', '2019-05-09 11:36:49'),
('cd60f2f04f11b6855561a6cc2646859e79c7481cc1b1cdc348fad66d5c6635cceb828a9ee573eff0', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:37:04', '2018-05-09 10:37:04', '2019-05-09 11:37:04'),
('7a16b43957204d7385a49aba6f3dfa997ddd807741d2768bdd1f02cd8a2f3b2e564599b68764b36f', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:37:13', '2018-05-09 10:37:13', '2019-05-09 11:37:13'),
('82c63e547a06ed2775541d46a542874bab049a762889a8391db6fad738af3fe929280a89221fddfb', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:37:32', '2018-05-09 10:37:32', '2019-05-09 11:37:32'),
('11b41b4324b50955533a9413c4250a1ff7e505c2cdef44d62b3cd5fb967c68569d660b989e5432cf', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:37:53', '2018-05-09 10:37:53', '2019-05-09 11:37:53'),
('cad9f88ef2fd6b75d91f427488287e750520fae5ebd93aa8b40518ca714a453f025b1ae57c6eab9c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:38:02', '2018-05-09 10:38:02', '2019-05-09 11:38:02'),
('c97c97f8825f75fcc6acd6d73692665dff0f74dc6b256b031a03786548c9bda8881df11347a99d46', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:38:55', '2018-05-09 10:38:55', '2019-05-09 11:38:55'),
('eb83d8073dbcb5cd4a9c813661d61c205df68b66ba99f1863346f7d99ad55857d81e8cacd4f27e58', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:39:03', '2018-05-09 10:39:03', '2019-05-09 11:39:03'),
('ba6e72afc694c77c956c9ad38fe4f96cefc97e92217c70a8ca68a06740f82dae45f5bd036d5c596d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:39:11', '2018-05-09 10:39:11', '2019-05-09 11:39:11'),
('e5091c54e2d9f88482da4d6be9abf33da9d632f9cdd7fd97414db86a71a7af3371a4d9c631ec928f', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:39:18', '2018-05-09 10:39:18', '2019-05-09 11:39:18'),
('67e0e83313ecdfaa090cd6b44bc47f56e99ee12edf32de4cab159f9925284e88bae88d84e8119d19', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:39:49', '2018-05-09 10:39:49', '2019-05-09 11:39:49'),
('496a036e2f53286a890c5bbf78c5e87373d21a21d290ad6070a55a40e02dfb6e3e8c700b69bcffdb', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:40:41', '2018-05-09 10:40:41', '2019-05-09 11:40:41'),
('794492e59f866088d51fdd02f6df306486663f1a3991c80034c08f4e2cec118f5a57ffb704b8609f', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:40:51', '2018-05-09 10:40:51', '2019-05-09 11:40:51'),
('c5904f8f7f4e2d6077c7d64808ae6f52c0d68804a86a18b9af6d66632640237d8df56abac2de2fe1', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:41:07', '2018-05-09 10:41:07', '2019-05-09 11:41:07'),
('8a76d31941914234b3447a4e749ef60b45e5e9dede470ec8d1ebe1dee5ac3aabd31791a71f029c7c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:41:10', '2018-05-09 10:41:10', '2019-05-09 11:41:10'),
('9d558fa74309df0b1fa20c6408cefe219fdb7c8be0b4a607e3788a1375ed898d29b3799d24ebae76', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:41:21', '2018-05-09 10:41:21', '2019-05-09 11:41:21'),
('66c54231eb1794ae51ce97d4537f3af527b9e548e136504f3d0cc6264174c671fbe536888ca6ea46', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:41:29', '2018-05-09 10:41:29', '2019-05-09 11:41:29'),
('0b793a0ab7e5ce4c386d38c097c11a48205b1463c172f5e3969eec2be56c76cb35d3e933c5321661', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:41:43', '2018-05-09 10:41:43', '2019-05-09 11:41:43'),
('35ee742ad6b3724468fbe706b561221e1edda843425e04af9b58ba63fe1db70374eb13d6f885d6a0', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:42:37', '2018-05-09 10:42:37', '2019-05-09 11:42:37'),
('13cf80d27fe0b86072a6bdd24563d6c13da193318b87e6681a5913cf90ff2e055fc1674656c2155a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:42:53', '2018-05-09 10:42:53', '2019-05-09 11:42:53'),
('3b0be11cd83006e54ea1cf3a4df7c710441ee9f30814ae4288a116f9a6b8e680ced57478c68e2ec9', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:45:54', '2018-05-09 10:45:54', '2019-05-09 11:45:54'),
('e791b04b0cd711da4edfa60aac34880d463bb031eb0cdf2f49805192c156ee82837cf4480fa3c845', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:46:05', '2018-05-09 10:46:05', '2019-05-09 11:46:05'),
('424b07b78d03c2679832da461359f92d95bbb2dff5631af725caabac3be7ef5adf23cdb552e9440e', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:47:49', '2018-05-09 10:47:49', '2019-05-09 11:47:49'),
('6d64e273c2f4280cf3074c879b9fa8359713e3de4e97c134ce1efa31e35f032ebde329df637a1eff', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:47:55', '2018-05-09 10:47:55', '2019-05-09 11:47:55'),
('d0298512f8a966ac692e18f6fb23bed99dbf26c24ed7aab1b446d75e5d3322d353fc9eda8b295576', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:48:02', '2018-05-09 10:48:02', '2019-05-09 11:48:02'),
('9e095bac1061f8f3fa0bcce92a22168c2da885715cfea9797cef8fd108e11641731533267c9b270b', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:48:09', '2018-05-09 10:48:09', '2019-05-09 11:48:09'),
('28b60ad9f0998c3c3d75d1f689c32fa52516935f28ae451301f33845209e84b73a76f870c1220011', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:50:40', '2018-05-09 10:50:40', '2019-05-09 11:50:40'),
('c0720cdfcf07047ce57c9040bb88b447ce40a37aa0b9b7f036a4739e27eadedd633e560dcee94d20', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:51:05', '2018-05-09 10:51:05', '2019-05-09 11:51:05'),
('80dbc76c78e4412da9f182684156dc4979b8e5c3ccf5fed12b12e53e5562ba99393a10ee21c10526', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:51:13', '2018-05-09 10:51:13', '2019-05-09 11:51:13'),
('c390b643bc779b772011e27c59d3da685ff5cb123d9bc72924f02b25f418d9c36fd3339c485f8eff', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:53:11', '2018-05-09 10:53:11', '2019-05-09 11:53:11'),
('1b5dddf5b5538b40d5686eac3ceadb14fb5c7faa34cf294ac4c18378b52f7de10c3ac55d3fccbe98', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:54:21', '2018-05-09 10:54:21', '2019-05-09 11:54:21'),
('44c8aecb09b27b4257f43a6276d896141b2547cf3a942ec017e3479c09ffb1bbe2fa1d3f2ff2a43d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:54:31', '2018-05-09 10:54:31', '2019-05-09 11:54:31'),
('642d05b344c32bae54e239c9553475ed8540d0e60fb54521b4058f57f9ccf5babf5336f2e1f19de8', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:54:43', '2018-05-09 10:54:43', '2019-05-09 11:54:43'),
('f2724ecdcc5585c72753af9c5f67a27ac2ad60dc2584ad5ac152dc60e7699aaa7ad4b6d6e6a727fb', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:54:55', '2018-05-09 10:54:55', '2019-05-09 11:54:55'),
('5344756013f00f628f0813306c00885469832bee3baf3b2ab1182e2b929f833786118b6ec02caad4', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:55:01', '2018-05-09 10:55:01', '2019-05-09 11:55:01'),
('3ecb6808ca234a57da5462c45d531a87e04ecc6143ab6bdac0c7c64f6968cbe6b1da2148728a92ad', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:55:43', '2018-05-09 10:55:43', '2019-05-09 11:55:43'),
('59ac98282fe70789a54efb2a93d51ce78990aabba6b8fa50e4c6bf59148e7982ea4b3d6faa925a5b', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:56:01', '2018-05-09 10:56:01', '2019-05-09 11:56:01'),
('e176da662fba52e66f4f2b6f989a62640e64442eed15a292d1dda73443482846934cd6a620d95edf', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:56:14', '2018-05-09 10:56:14', '2019-05-09 11:56:14'),
('82e2d209a3fd0f53ac63c9699c053e796829f7bb9ac24a7414bf31f2245653f4798a768047bcdd98', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:56:43', '2018-05-09 10:56:43', '2019-05-09 11:56:43'),
('c24be00e367881703e92fef50981322ed93f4217c755be50d66351cf7d2322ac6a4f7051289046b6', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:57:09', '2018-05-09 10:57:09', '2019-05-09 11:57:09'),
('8f30dcb1c20f1db78b55ec2ca71e9f6d4013792a8d6a62d47351f6a1df0b09750afe85a03935f704', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:57:15', '2018-05-09 10:57:15', '2019-05-09 11:57:15'),
('6fd7f8bc7283e7151983e99b54e5a0cc9b1e6bb453b036140f4ecd327925290fc0de62665fc64624', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:57:27', '2018-05-09 10:57:27', '2019-05-09 11:57:27'),
('07ac29d5dd3e410adebb189b6cc37f7b2b92479bfe38618f5aeb40fecbe38980bb8623a566c465e1', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:57:31', '2018-05-09 10:57:31', '2019-05-09 11:57:31'),
('955f65d0ff6337d9aee7496b3c41597fa8fbef77a6bf82c88fea0aa614f00dd6db949a2bfe176b49', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:57:40', '2018-05-09 10:57:40', '2019-05-09 11:57:40'),
('5922bd251c7a82cc42a79c372ce2c06d760601898dd165ae5d20d853a764edadb9545a68f6d2a1b5', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:57:49', '2018-05-09 10:57:49', '2019-05-09 11:57:49'),
('180b5fae580bfa1fa15ddcb2a55f38fdb3c35a8fd7daa1be573cbbf588040775d3b874541d3d27a7', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:58:07', '2018-05-09 10:58:07', '2019-05-09 11:58:07'),
('2b399565f079cee023842ce4a680a7b6856f2cf9e175ab7dd0e247e1a3ec037dc6ff400f3f4184af', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:58:16', '2018-05-09 10:58:16', '2019-05-09 11:58:16'),
('95404730a69255aa660ab2975051ef38898d10fd65b8b5345274a84a059b22a2965ad3391984e5a7', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 10:58:32', '2018-05-09 10:58:32', '2019-05-09 11:58:32'),
('c375f3b9aaf04679c37b82d0183f85d9bf9a0e1be338a153be89866b0610795e68c75d40ed8332d5', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 11:01:46', '2018-05-09 11:01:46', '2019-05-09 12:01:46'),
('6bc0144b153ad11b755e8674460af77ec0ccb363144ebda06a775429d3a26645eb5b3e05ac235bc4', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 11:02:41', '2018-05-09 11:02:41', '2019-05-09 12:02:41'),
('ec43564d99cb8f516f4395122cdf02eb5586419325e8b8de966df7965bdde2b23b21e7a86dcc22b8', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 11:03:09', '2018-05-09 11:03:09', '2019-05-09 12:03:09'),
('7869a3e5aafba921f267773412656a99cf37133e0861cdd7642475c797673b9a48f200cb01003d67', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 11:03:23', '2018-05-09 11:03:23', '2019-05-09 12:03:23'),
('b6133cd339529f8a1f010df657c19d4d465ba176a16aaa1f8f209052102001ec8058afd48bcd0ece', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 11:03:56', '2018-05-09 11:03:56', '2019-05-09 12:03:56'),
('b41a5cb0f641c896ea8314064c9399c06039a8b52faf7177e02bddf1c03b6bb7e1bda071bc434734', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 12:13:57', '2018-05-09 12:13:57', '2019-05-09 13:13:57'),
('406fbbd1640414b5970433b9e52458d1c53ed2330cd2b59a9db3dcc31d48ba60a589aa94c4d9391c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 12:14:08', '2018-05-09 12:14:08', '2019-05-09 13:14:08'),
('14ff696fe8aeff1ff831bdb8fccbf094bc9e023965ebd27c2d8b9ac87cf6800d26a3ea3e06957568', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 12:14:22', '2018-05-09 12:14:22', '2019-05-09 13:14:22'),
('279bf64c1542f26ea89de8baa604e4603a4c2b26f5ecd24d1d8487d61be32cc8c8294dfbe662f323', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 12:14:26', '2018-05-09 12:14:26', '2019-05-09 13:14:26'),
('081a4f642cb454e8e3f2059938309043184bc1a640a58c320ec8446feffb435fe85a02e27a7e5b85', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 12:15:01', '2018-05-09 12:15:01', '2019-05-09 13:15:01'),
('885f2d283d974446ee8fddbfffebc377da0b7e14558d71175a5382205f30c3cba699557d12387252', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 12:20:26', '2018-05-09 12:20:26', '2019-05-09 13:20:26'),
('e103bd1acb71d3e0107a868643e19b33ae128b8f515831bfcae7b18bca97efc03e5504ec746b30c5', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 12:20:41', '2018-05-09 12:20:41', '2019-05-09 13:20:41'),
('00389d37df38e7488e6ee58998aaf730e39b785558d4c45494b8b7a6f5c09d9af9b848b000044fbe', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-09 12:22:46', '2018-05-09 12:22:46', '2019-05-09 13:22:46'),
('55f1234f94edffbfec8511f75191774de5d1177a950b53a551d1fbdc14b21a720bb42a7c8d1d9590', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-09 12:23:01', '2018-05-09 12:23:01', '2019-05-09 13:23:01'),
('4464fbbb25ec5429eb28a58eda629698186881653831bf71c86f7682e236fb862a5e08c4f47c53ff', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 12:31:05', '2018-05-09 12:31:05', '2019-05-09 13:31:05'),
('b8544f2b709cea1e24718b8bf4f50a5ec805a0ec258c0a0ee8466f3fb617fbd00ad748b06250d6c6', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-09 12:31:40', '2018-05-09 12:31:40', '2019-05-09 13:31:40'),
('6734f5c0e16572612aeead9045c4f53844e492e499971a713e09e2ac6505fa94f3c6733beefa4d9f', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 13:21:47', '2018-05-09 13:21:47', '2019-05-09 14:21:47'),
('af60546752d2223533f2ac670b7efe067fdfe22f7809150d5a687591308e7a4dc77f95cccf9047dd', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-09 13:24:20', '2018-05-09 13:24:20', '2019-05-09 14:24:20'),
('3ccfb5f96b244cdfc78aa447a287e75117bf60961ae9a026b711eee2d6ea249ed85709a87aaed4db', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-09 13:24:31', '2018-05-09 13:24:31', '2019-05-09 14:24:31'),
('8cb9cfcc7e54026b50efbcb16d7a0e73b9156b8a0fa014a16cdd6e5bebd5dc4579bf3374862d61fa', 2, 1, 'nunolopes@farmodietica.com', '[]', 0, '2018-05-09 13:24:36', '2018-05-09 13:24:36', '2019-05-09 14:24:36'),
('51eae2167d01767a3b0821a32b247ae0b9fee11145a6588f46a6df2a7aa15d12b3a7e8b1c049dc08', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-09 13:28:05', '2018-05-09 13:28:05', '2019-05-09 14:28:05'),
('cfd1afa9712924670ef0b5a04b52dabbcde689cede0f1b42e5cdb447cd246ecdba0271c259590491', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-09 13:29:11', '2018-05-09 13:29:11', '2019-05-09 14:29:11'),
('17836bec417b8a2d4c8b920494ecc4899b6b115d4028d7b0461544f936d99620d321a2de702bf363', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-09 13:30:31', '2018-05-09 13:30:31', '2019-05-09 14:30:31'),
('2906e5b906b0fe17bc9d18ac2f2bddf6d568851debfb00d81b0d02615bc87a611bcf94e542c0a5b1', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-10 07:45:54', '2018-05-10 07:45:54', '2019-05-10 08:45:54'),
('d2a09ac53766195ee95c8cf888fab406ba5636dad26ac145dc5387941d955a4c30609d1d365adf3d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-10 08:35:27', '2018-05-10 08:35:27', '2019-05-10 09:35:27'),
('f42e5574fa4b59f4e88932b70d00b73b8e7f3161f3e795a62d2caa342df16abfc059363a3efc628c', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-10 08:36:01', '2018-05-10 08:36:01', '2019-05-10 09:36:01'),
('cea4f76625ad8e0a3c88bd0ba3a65289b0241f1db50d43f5e74da9cf62dcc4315bb6452f30a42d7a', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-10 08:42:34', '2018-05-10 08:42:34', '2019-05-10 09:42:34'),
('a42938fe1c945f2f96892c7e78ba46b3bf89e4ae7f3bbedb468f68da3b2eea7c1419bf83e444bdd4', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-10 09:05:39', '2018-05-10 09:05:39', '2019-05-10 10:05:39'),
('33fb13cb912275ada9c3d8d8df7a3716b39876b0eaaeea3e958df13d6fe1a64c8c985f42075e963c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-10 09:05:53', '2018-05-10 09:05:53', '2019-05-10 10:05:53'),
('7f0516c01875cd287042f2c87cbb86552cf44609e1018a196b379172be412bb1dae081ae2330e6b2', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-10 09:25:02', '2018-05-10 09:25:02', '2019-05-10 10:25:02'),
('132863038bc543ac7c2d8e11ed8cd04166111e4b4b11585f4027aea2fc764ab2b128021846762aa6', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-10 13:54:29', '2018-05-10 13:54:29', '2019-05-10 14:54:29'),
('c598a06e51d158add6207b13e2f4f1c71b788635b210fc5aad8bd76d6b5be935628d2b4c5f819e6c', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-10 15:54:29', '2018-05-10 15:54:29', '2019-05-10 16:54:29'),
('4389c14306f5da325da0bee88ccaf0e3e9113a9e8d774a74eefab6cb6363608d142e9cebbf38a20e', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-10 15:55:08', '2018-05-10 15:55:08', '2019-05-10 16:55:08'),
('fe275f2b74da5743b667a2dd3a2965c1df94b10e08f53f938747292d6fe3aa5ce649e29f2e33827d', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-10 16:03:37', '2018-05-10 16:03:37', '2019-05-10 17:03:37'),
('61fd01db2109f79bd9b905df7d36aefb38508ab51ec78ebc89b811d65ca02247bd3bb51a3da6314f', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-10 16:04:37', '2018-05-10 16:04:37', '2019-05-10 17:04:37'),
('2946aee259a0dc4e5b3d9ec431f91366570350e10f6c3de078a50622690f0222bfa5f9d4d3a23d71', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-11 09:15:49', '2018-05-11 09:15:49', '2019-05-11 10:15:49'),
('6756ffac36aad10faec0e5b20538b9a5ec68cecf3089102718298229b8740880d39a148f5e470163', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-11 09:24:56', '2018-05-11 09:24:56', '2019-05-11 10:24:56'),
('5a6f58fd54f8f53af48007728cb564b79f005fc2fda31cd9d79dc11f7c4cfd5f58dd023b0715d74d', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-11 09:45:12', '2018-05-11 09:45:12', '2019-05-11 10:45:12'),
('0bca327cc2d8e11390a2085d66a622fc6b34e175d29212677b9d75e20c2b16b87ebf1cc44b3ebb8f', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-11 09:45:16', '2018-05-11 09:45:16', '2019-05-11 10:45:16'),
('99b8951546ad6ba13ff6e00cba148bb2360226e6d44d15c46a2a08b37f588a2e7f499110bc768c1a', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-11 10:08:05', '2018-05-11 10:08:05', '2019-05-11 11:08:05'),
('2015e740d232fbfcc014b8277393c5379b3d0771d6a7cba2deffe14d190729a11dd992389208c5f5', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-11 12:45:49', '2018-05-11 12:45:49', '2019-05-11 13:45:49'),
('9a7b08fc3fc0292deddbc85835c7ddb41359ab4618d786e8de1845c60de2051c1d42fe35d001f2fd', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-11 12:46:54', '2018-05-11 12:46:54', '2019-05-11 13:46:54'),
('3aff6891e6a2ccaf44171750458656717cbadcb545d228cbc094712c3e80e76a69c393124476728c', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-11 14:01:44', '2018-05-11 14:01:44', '2019-05-11 15:01:44'),
('04b475dd80ecd974cf639a93edb683a923f44b73b55f331e1f53271dfae9413e673c223ef45d7f63', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-11 14:31:52', '2018-05-11 14:31:52', '2019-05-11 15:31:52'),
('6a488bb922456f3f8f9d251f7d6220ffeb51ad434ebbfa9224a5fb2af39d6bcc67b492798574f862', 1, 1, 'filipecovas@farmodietica.com', '[]', 0, '2018-05-11 15:22:08', '2018-05-11 15:22:08', '2019-05-11 16:22:08'),
('4b34b4913c386596043f92327392a90d21cb36e383309c29b756efeb919792e94284b9d2f1fceceb', 8, 1, 'franciscamenezes@farmodietica.com', '[]', 0, '2018-05-11 16:02:39', '2018-05-11 16:02:39', '2019-05-11 17:02:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'nDFfeFYz7zsEVF2kzhKYMbjpgwaXRy31NLeXWcHv', 'http://localhost', 1, 0, 0, '2018-03-28 13:22:06', '2018-03-28 13:22:06'),
(2, NULL, 'Laravel Password Grant Client', 'GB1sUS2CoMywVFYZqaJJmZln8Y22efv9tvnPR4PO', 'http://localhost', 0, 1, 0, '2018-03-28 13:22:06', '2018-03-28 13:22:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-03-28 13:22:06', '2018-03-28 13:22:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `prioritys`
--

DROP TABLE IF EXISTS `prioritys`;
CREATE TABLE IF NOT EXISTS `prioritys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `color` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `prioritys`
--

INSERT INTO `prioritys` (`id`, `name`, `icon`, `color`) VALUES
(1, 'Baixa', 'fas fa-hourglass-start', '#78CE30'),
(2, 'Normal', 'fas fa-hourglass-half', '#579BFC'),
(3, 'Urgente', 'fas fa-fighter-jet', '#FF8E00'),
(4, 'Imediato', 'fab fa-hotjar', '#C9302C');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projects`
--

DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `time` date NOT NULL,
  `status_id` int(1) NOT NULL,
  `evolution` int(11) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `time`, `status_id`, `evolution`, `start`, `end`, `created_at`, `updated_at`) VALUES
(1, 'Suporte Geral', 'Todos os pedidos de suporte geral', '2018-03-05', 1, 0, NULL, NULL, '2018-03-05 14:44:09', '2018-05-07 10:58:10'),
(2, 'Site E-dieta', 'Plataforma edieta', '2018-05-01', 6, 61, NULL, NULL, '2018-03-05 14:49:19', '2018-05-11 09:51:00'),
(3, 'Site Absorvit', 'Website do produto Absorvit Vitaminas;', '2018-04-30', 2, 49, NULL, NULL, '2018-03-05 16:41:06', '2018-05-11 09:08:53'),
(4, 'Portal de Gestão de Pedidos/Projetos TI', 'Desenvolvimento do Portal de Gestão de Pedidos/Projetos da equipa TI', '2018-03-30', 1, 0, NULL, NULL, '2018-03-12 11:52:04', '0000-00-00 00:00:00'),
(5, 'D3P - loja on-line', '<p>Desenvolvimento do Portal Dieta3Passos Loja Online V2</p>', '2018-07-30', 1, 74, NULL, NULL, '2018-03-12 12:23:15', '2018-05-11 09:11:15'),
(6, 'Site Dieta-Easy-Slim', 'Integração Back End Wordpress, Estilizar Menu Ajustar HTML/CSS implementado', '2018-08-30', 2, 0, NULL, NULL, '2018-03-12 12:48:38', '0000-00-00 00:00:00'),
(7, 'D3P -Portal Nutrição - Melhoria ao Portal das Nuticionistas', 'Melhoria ao Portal das Nuticionistas', '2018-03-21', 1, 0, NULL, NULL, '2018-03-12 18:04:13', '0000-00-00 00:00:00'),
(8, 'Logistica', 'Desenvolver nova Versão de Software PDA Logistica', '2018-03-13', 2, 0, NULL, NULL, '2018-03-13 15:13:45', '0000-00-00 00:00:00'),
(9, 'CRM Zeltica', 'Implementação de melhorias', '2018-03-13', 3, 0, NULL, NULL, '2018-03-13 15:15:03', '0000-00-00 00:00:00'),
(10, 'CRM Dietética - criação portal', 'Será implementado com o mesmo layout da Farmácia assim que o CRM farmácia estiver fechado;', '2018-03-12', 2, 0, NULL, NULL, '2018-03-13 15:16:41', '0000-00-00 00:00:00'),
(11, 'D3P -Portal Clinicas', 'Portal Clínicas 2.0.Terminar a agenda, com interface de Clinica', '2018-03-21', 1, 0, NULL, NULL, '2018-03-13 15:17:37', '0000-00-00 00:00:00'),
(12, 'D3P -Portal Nutrição - Adaptação de todo o Front End', 'Adaptação de todo o Front End', '2018-03-21', 1, 0, NULL, NULL, '2018-03-13 15:21:15', '0000-00-00 00:00:00'),
(13, 'Qualidade', 'No PHC fazer o controlo de qualidade do Produto', '2018-03-21', 2, 0, NULL, NULL, '2018-03-13 15:24:24', '0000-00-00 00:00:00'),
(14, 'Controlo de Crédito', 'Desenvolvimento do Portal', '2018-03-30', 2, 0, NULL, NULL, '2018-03-13 15:27:56', '0000-00-00 00:00:00'),
(15, 'CRM Farmácia', 'Devem ser criados no PHC os espaços para os objetivos desta equipa: Loring, Tattu, PharmaTea', '2018-03-30', 2, 0, NULL, NULL, '2018-03-13 15:29:03', '0000-00-00 00:00:00'),
(16, 'Portal Nutrição', 'Portal Nutricionistas + Nova Versão Campanha Amigo: - Implementar a agenda da nutricionista (D3P, Bio3, EasySlim)', '2018-03-21', 1, 0, NULL, NULL, '2018-03-13 15:37:48', '0000-00-00 00:00:00'),
(17, 'CRM - Gestão de Frotas', 'CRM - Gestão de Frotas', '2018-03-30', 2, 0, NULL, NULL, '2018-03-13 15:56:20', '0000-00-00 00:00:00'),
(18, 'Assiduidade', 'Desenvolvimento do Portal de Gestão de Recursos Humanos', '2018-03-30', 1, 0, NULL, NULL, '2018-03-13 15:57:01', '0000-00-00 00:00:00'),
(19, 'CRM Dieta3P', 'Desenvolvimento do Portal', '2018-04-30', 1, 0, NULL, NULL, '2018-03-13 16:01:43', '0000-00-00 00:00:00'),
(20, 'EasySlim - Portal Farmácias', 'Desenvolvimento do Portal', '2018-05-30', 2, 0, NULL, NULL, '2018-03-13 16:02:36', '0000-00-00 00:00:00'),
(21, 'CRM Dietética - criar configurações', 'Criar BackOffice de todo o portal permitindo parametrizações pelo administrador sem ser necessário o programador', '2018-04-30', 1, 0, NULL, NULL, '2018-03-13 16:03:52', '0000-00-00 00:00:00'),
(22, 'EasySlim - Portal Farmácias', 'Desenvolvimento do Portal', '2018-04-30', 1, 0, NULL, NULL, '2018-03-13 16:16:52', '0000-00-00 00:00:00'),
(23, 'APP Sirius', 'Optimizar estrutura funcional de desenvolvimento e optimização de páginas', '2018-04-30', 2, 0, NULL, NULL, '2018-03-13 16:21:10', '0000-00-00 00:00:00'),
(24, 'Bio3 - Portal Dietéticas', 'Desenvolvimento do Portal', '2018-04-30', 2, 0, NULL, NULL, '2018-03-13 16:26:45', '0000-00-00 00:00:00'),
(25, 'office 365', 'analise da solucao office 365', '2018-04-21', 1, 0, NULL, NULL, '2018-03-21 14:53:10', '0000-00-00 00:00:00'),
(27, 'Outros', 'Projecto para criação de outro tipo de registo de actividades', '2023-12-21', 1, 0, NULL, NULL, '2018-03-23 09:46:59', '0000-00-00 00:00:00'),
(28, 'Projecto teste 2', 'teste desc', '2018-03-05', 3, 1, NULL, NULL, '2018-04-05 14:08:25', '2018-05-07 11:00:45');

-- --------------------------------------------------------

--
-- Estrutura da tabela `project_files`
--

DROP TABLE IF EXISTS `project_files`;
CREATE TABLE IF NOT EXISTS `project_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_files_project_id_idx` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `recoveries`
--

DROP TABLE IF EXISTS `recoveries`;
CREATE TABLE IF NOT EXISTS `recoveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `passwords_user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'Developer'),
(2, 'Administrador'),
(3, 'Colaborador');

-- --------------------------------------------------------

--
-- Estrutura da tabela `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `status`
--

INSERT INTO `status` (`id`, `name`, `icon`) VALUES
(1, 'Pendente', 'far fa-clock'),
(2, 'Em Curso', 'fas fa-file-code'),
(3, 'Concluído', 'fa fa-check-square'),
(4, 'Inativo', 'fa fa-ban'),
(6, 'Concluído/Melhorias', 'fas fa-wrench');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tickets`
--

DROP TABLE IF EXISTS `tickets`;
CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(21000) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_id` int(1) NOT NULL,
  `user_solicitation_id` int(11) NOT NULL,
  `priority_id` int(11) NOT NULL,
  `worker_id` int(11) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `prevend` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tickets_projects_idx` (`project_id`),
  KEY `tickets_users_idx` (`user_id`),
  KEY `tickets_workers_idx` (`worker_id`),
  KEY `tickets_user_solicitation_idx` (`user_solicitation_id`),
  KEY `tickets_prioritys_idx` (`priority_id`),
  KEY `tickets_status_idx` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tickets`
--

INSERT INTO `tickets` (`id`, `name`, `description`, `project_id`, `user_id`, `status_id`, `user_solicitation_id`, `priority_id`, `worker_id`, `start`, `prevend`, `end`, `created_at`, `updated_at`) VALUES
(14, 'Portal Absorvit-Estilizar', '<p>Estilizar o Menu e um ou outro plugin/bloco/ajuste de HTML/CSS implementado, de acordo com os documentos entregues pelo dep. Design. 2</p>', 3, 3, 2, 3, 4, 6, NULL, NULL, NULL, '2018-03-12 12:54:17', '2018-04-27 10:14:43'),
(15, 'Portal Absorvit - Integração Back End Wordpress', '<p>teste ao pedido</p>', 2, 3, 2, 8, 2, 1, NULL, NULL, NULL, '2018-03-12 13:00:26', '2018-04-24 14:27:13'),
(17, 'Pedidos de recolha Às clinicas 2', '<p>Não consigo fazer pedidos de recolha nas clinicas aparece este erro. O que fazer para resolver este erro? </p><p class=\"MsoNormal\">Gabriela Santos 2</p>', 1, 3, 1, 13, 2, 5, NULL, NULL, NULL, '2018-03-12 14:16:22', '2018-05-11 14:03:05'),
(18, 'ERRO URGENTE: Importação de Encomendas 2', '<p>ERRO URGENTE: Importação de Encomendas - Email Automatico</p>', 1, 3, 3, 3, 4, 3, '2018-03-14', '2018-03-14', '2018-03-14', '2018-03-12 15:08:49', '2018-04-02 09:44:10'),
(22, '700| Alteração encomenda 2', '<p>Acrescento o pedido de a Gabriela e eu termos acesso a acrescentar linhas às encomendas. </p><p>A Gabriela também não consegue gravar. </p><p><br /></p><p>Nas ausências da Mariana, a Gabriela e eu deveremos ter acesso a todas as suas funções. </p>', 1, 3, 1, 12, 1, 3, NULL, NULL, NULL, '2018-03-12 17:02:43', '2018-04-04 09:13:38'),
(23, 'ERRO URGENTE: Importação de Encomendas', '<p>ERRO URGENTE: Importa&ccedil;&atilde;o de Encomendas - 13.30 (2&ordf;x) - Email automatico</p>', 1, 3, 3, 3, 4, 5, '2018-03-12', '2018-03-12', NULL, '2018-03-12 17:11:24', NULL),
(24, 'Actualizar dados CRM', '<p>Actualizar dados CRM</p>', 1, 3, 3, 8, 3, 5, '2018-03-12', '2018-03-12', NULL, '2018-03-12 17:20:59', NULL),
(25, 'Obter dados das Leads 2', '<p>Obter dados da Bd sobre os pedidos (Leads). Ver Email do Ricardo Rodrigues com os dados necessarios</p>\r\n<p>Indicadores relacionados com o Projeto das melhorias ao Portal das agendas das&nbsp; Nutricionistas (Pedido 91 do ficheiro excel)</p>', 1, 3, 2, 3, 1, 3, NULL, NULL, NULL, '2018-03-12 17:25:19', '2018-04-23 13:02:44'),
(26, 'Geração de encomendas de livros', '<p>Gera&ccedil;&atilde;o de encomendas de Livros easy -slim</p>', 1, 3, 3, 13, 3, 5, '2018-03-12', '2018-03-12', NULL, '2018-03-12 17:51:10', NULL),
(27, 'Encomendas Farma Bem Estar', '<p>Encomendas Farma Bem Estar Vejo algumas encomendas apenas de Loring, mas n&atilde;o t&ecirc;m vendedor.Vejo algumas encomendas apenas de Loring, mas n&atilde;o t&ecirc;m vendedor</p>', 1, 3, 3, 8, 3, 5, '2018-03-12', '2018-03-12', NULL, '2018-03-12 17:56:28', NULL),
(28, 'Lista de Vendas e objectivos gerais (Clínicas)', '<p>&nbsp;</p>\r\n<p style=\"margin: 0cm 0cm 0pt;\"><span style=\"color: #1f497d; font-family: \'Calibri\',sans-serif; font-size: 11pt;\">Bom dia,</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin: 0cm 0cm 0pt;\"><span style=\"color: #1f497d; font-family: \'Calibri\',sans-serif; font-size: 11pt;\">Continuamos sem vendas de s&aacute;bado e ontem no PHC</span></p>', 1, 3, 3, 12, 3, 5, '2018-03-13', '2018-03-13', NULL, '2018-03-13 11:39:14', NULL),
(29, 'Alteração de designação de referencia existente - Pic-Zero', '<p>&nbsp;</p>\r\n<p style=\"margin: 0cm 0cm 0pt;\"><font size=\"3\" face=\"Calibri\">A altera&ccedil;&atilde;o necess&aacute;ria fazer &eacute; a seguinte:</font></p>\r\n<p style=\"margin: 0cm 0cm 0pt;\"><font size=\"3\" face=\"Times New Roman\"><font size=\"3\" face=\"Calibri\">A refer&ecirc;ncia&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <span style=\"background: yellow; mso-highlight: yellow;\">ADV54</span>&nbsp; Advancis Pic-Zero Adesivos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font></font></p>\r\n<p><font size=\"3\" face=\"Calibri\">Ir&aacute; alterar para <span style=\"background: yellow; mso-highlight: yellow;\">PIC01</span>&nbsp; Pic-Zero Adesivo Protetor 24 uni.&nbsp; (Fam&iacute;lia: DIV. FARM. BE)</font></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin: 0cm 0cm 0pt;\"><font size=\"3\" face=\"Calibri\">&nbsp;<font size=\"3\" face=\"Times New Roman\"><font size=\"3\" face=\"Calibri\">Se conseguir fazer esta altera&ccedil;&atilde;o n&atilde;o ser&aacute; necess&aacute;rio criar nova refer&ecirc;ncia com novo CNP e c&oacute;digo de barras correto?</font></font></font></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin: 0cm 0cm 0pt;\"><font size=\"3\" face=\"Calibri\">&nbsp;<font size=\"3\" face=\"Times New Roman\"><font size=\"3\" face=\"Calibri\">Relembro que agregado a este produto est&atilde;o as seguintes referencia que tamb&eacute;m elas precisam de ser alteradas:</font></font></font></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin: 0cm 0cm 0pt;\"><font size=\"3\" face=\"Calibri\">&nbsp;<font size=\"3\" face=\"Times New Roman\"><font size=\"3\" face=\"Calibri\">BULKADV54&nbsp; Bulk Advancis Pic-Zero Adesivos&nbsp;&nbsp; para&nbsp;&nbsp; BULKPIC01&nbsp; Bulk Pic-Zero Adesivo Protetor uni.</font></font></font></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin: 0cm 0cm 0pt;\"><font size=\"3\" face=\"Calibri\">&nbsp;<font size=\"3\" face=\"Times New Roman\"><font size=\"3\" face=\"Calibri\">CARTADV54&nbsp; Cart. Advancis Pic-Zero Adesivos&nbsp;&nbsp; para CARTPIC01 Cart. Pic-Zero Adesivo Protetor 24 uni.</font></font></font></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin: 0cm 0cm 0pt;\"><font size=\"3\" face=\"Calibri\">&nbsp;<font size=\"3\" face=\"Calibri\">PROMO16/ADV54&nbsp; Rotulo Advancis Pic-Zero&nbsp; para&nbsp; &nbsp;ROTPIC01&nbsp; Rotulo Pic-Zero Adesivo Protetor 24 uni.</font></font></p>\r\n<p>&nbsp;</p>', 1, 3, 1, 14, 1, 5, NULL, NULL, NULL, '2018-03-13 14:23:00', NULL),
(30, 'Ajuda - Imprimir Documentos Transferência entre Armazéns', '<p class=\"MsoNormal\">N&atilde;o estou a conseguir imprimir o documento de transfer&ecirc;ncia entre armaz&eacute;ns.</p>\r\n<p class=\"MsoNormal\">&nbsp;Segue o erro que me aparece:</p>', 1, 3, 3, 18, 1, 5, NULL, NULL, NULL, '2018-03-13 15:42:12', NULL),
(31, 'Encomenda em falta clinica Girassol cl 164', '<p class=\"MsoNormal\">&nbsp;Podem por favor ver porque &eacute; que esta encomenda n&atilde;o foi aprovada (aprova&ccedil;&atilde;o autom&aacute;tica pelo sistema):</p>', 1, 3, 3, 18, 1, 3, NULL, NULL, NULL, '2018-03-13 15:48:17', NULL),
(33, 'factura logifarma', '<p>alterar factura logifarma</p>\r\n<p>Sofia Cristiv&atilde;o</p>', 1, 3, 1, 19, 1, 5, NULL, NULL, NULL, '2018-03-13 18:08:02', NULL),
(34, 'Envio de Faturas', '<p>Envio de Faturas por email automatico</p>', 1, 3, 1, 20, 2, 7, NULL, NULL, NULL, '2018-03-14 10:47:12', '2018-04-04 09:13:48'),
(35, 'Importaçao entrega farm', '<p>Importaçao entrega farm</p>', 1, 3, 1, 21, 1, 2, NULL, NULL, NULL, '2018-03-14 10:52:09', '2018-04-04 09:13:57'),
(36, 'Abatimento de comissões à conta corrente', '<p>Abatimento de comissoes à conta corrente</p>', 1, 3, 1, 5, 3, 7, NULL, NULL, NULL, '2018-03-14 10:53:21', '2018-04-04 09:14:05'),
(37, 'Preparar a Clinica com 4 digitos 2', '<p>Preparar a Clinica com 4 digitos</p>', 1, 3, 1, 5, 1, 7, NULL, NULL, NULL, '2018-03-14 10:54:24', '2018-04-04 09:14:14'),
(38, 'Composição de artigos', '<p>No PHC - Criação de composições de Artigos via Excel</p>', 1, 3, 1, 14, 1, 5, NULL, NULL, NULL, '2018-03-14 10:57:09', '2018-04-03 12:25:20'),
(39, 'Mapas de Ferias PHC', '<p>Disponibilizar no NAS os mapas de ferias do PHC</p>', 1, 3, 1, 5, 1, 5, NULL, NULL, NULL, '2018-03-14 10:58:54', NULL),
(40, 'ZELTICA, Mapa de depreciações e Amortizações', '<p>ZELTICA, Mapa de deprecia&ccedil;&otilde;es e Amortiza&ccedil;&otilde;es</p>', 1, 3, 1, 8, 3, 5, NULL, NULL, NULL, '2018-03-14 11:00:45', NULL),
(41, 'Faturas on-line', '<p>As faturas online est&atilde;o a aparecer no PHC sem os dados do cliente:</p>\r\n<p>(Teremos problemas com os clientes uma vez que estas faturas n&atilde;o v&atilde;o aparecer no Efaturas uma vez que segue sem contribuinte)</p>', 1, 3, 3, 5, 1, 5, '2018-02-23', '2018-02-27', NULL, '2018-03-14 11:03:58', NULL),
(42, 'PHC - Erro ao entrar na análise', '<p>PHC - Erro ao entrar na an&aacute;lise</p>', 1, 3, 1, 22, 1, 5, NULL, NULL, NULL, '2018-03-14 11:24:35', NULL),
(43, 'Contas correntes', '<p>Contas correntes versus contas n&atilde;o regularizadas</p>\r\n<p>Elisabete Silva</p>', 1, 3, 1, 5, 2, 5, NULL, NULL, NULL, '2018-03-14 11:45:27', NULL),
(44, 'PHC - Criar opção de pagamento', '<p>PHC - Criar op&ccedil;&atilde;o de pagamento de publicidade nas encomendas</p>\r\n<p>Alexandra Pedro</p>', 1, 3, 1, 5, 1, 5, NULL, NULL, NULL, '2018-03-14 11:47:04', NULL),
(45, 'D3P -Portal Nutrição - Front End', '<p>D3P -Portal Nutri&ccedil;&atilde;o - Adapta&ccedil;&atilde;o de todo o Front End</p>', 12, 3, 3, 9, 1, 6, NULL, NULL, NULL, '2018-03-14 11:53:52', NULL),
(46, 'PHC-Vendas com linha de produto a zero', '<p>Desde dezembro que h&aacute; vendas com linha de produto vazia, corrigir</p>', 1, 3, 1, 8, 2, 5, NULL, NULL, NULL, '2018-03-14 11:57:44', NULL),
(47, 'Site Dieta Easy-Slim', '<p><strong><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: \'Times New Roman\'; mso-bidi-theme-font: minor-bidi; mso-ansi-language: PT; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">Site Dieta Easy-Slim -&nbsp;</span></strong><font face=\"Calibri, sans-serif\"><span style=\"font-size: 14.6667px;\"><strong>Estilizar o Menu e um ou outro plugin/bloco/ajuste de HTML/CSS implementado, de acordo com os documentos entregues pelo dep. Design</strong></span></font></p>', 6, 3, 1, 8, 2, 6, NULL, '2018-04-30', NULL, '2018-03-14 12:50:07', NULL),
(48, 'Picking Armazém Novo', '<p>Picking Armaz&eacute;m Novo</p>', 1, 10, 1, 10, 2, 5, NULL, NULL, NULL, '2018-03-14 13:41:56', NULL),
(49, 'Telefone 121', '<p>o telefone n&ordm; 121 est&aacute; a procura de base(Erro)no armazem novo</p>', 1, 10, 3, 10, 3, 7, NULL, NULL, NULL, '2018-03-14 13:44:07', NULL),
(50, 'Armazem novo Farmodietica', '<p>Armazem novo Farmodietica</p>', 1, 3, 3, 10, 3, 7, '2018-03-09', '2018-03-12', NULL, '2018-03-14 13:45:48', NULL),
(52, 'Recuperar emails', '<p>Recuperar emails enviados dos ultimos 2 anos</p>', 1, 3, 1, 10, 1, 7, '2018-03-12', '2018-03-24', NULL, '2018-03-14 15:28:05', NULL),
(53, 'alimentação em falta', '<p>alimenta&ccedil;&atilde;o em falta -&nbsp;<span style=\"color: #1f497d; font-family: Calibri, sans-serif; font-size: 11pt;\">Pe&ccedil;o acesso a esta an&aacute;lise</span></p>', 1, 3, 3, 12, 1, 3, NULL, NULL, NULL, '2018-03-14 16:50:41', NULL),
(54, 'Farma do Ave - Diferença de valor lista de vendas', '<p><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: PT; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">Pedia-vos o favor de verificarem o valor que surge no mail da lista de vendas da Cristina Ventura, no que respeita ao cliente Farma do Ave, pois o valor da fatura (FT. 1462 &ndash; 173,55&euro;) n&atilde;o &eacute; o apresentado:</span></p>', 1, 3, 3, 24, 1, 5, NULL, NULL, NULL, '2018-03-14 16:59:13', NULL),
(55, 'Integração do E-dieta com Dieta3Passos', '<p>Integração do E-dieta com o site dieta3passos.pt</p>', 3, 1, 2, 9, 3, 6, NULL, NULL, NULL, '2018-03-14 17:15:53', '2018-04-03 13:27:47'),
(56, 'criação de uma conta de email para ser utilizado no portal(email enviado Ricardo Mecha)', '<p class=\"MsoNormal\">Solicito a cria&ccedil;&atilde;o de uma conta de email para ser utilizado no portal para envios externos como por exemplo a recupera&ccedil;&atilde;o da password, avisos, etc., e a respetiva password da conta de email.</p>\r\n<p class=\"MsoNormal\">Atualmente estou a usar a minha conta de email no c&oacute;digo, mas quero retirar.</p>\r\n<p class=\"MsoNormal\">&nbsp;</p>\r\n<p class=\"MsoNormal\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sugest&atilde;o: <a href=\"mailto:no-reply@farmodietica.com\">no-reply@farmodietica.com</a></p>', 1, 3, 4, 2, 1, 7, '2018-03-13', '2018-03-20', NULL, '2018-03-14 17:15:55', NULL),
(57, 'Contas correntes', '<p>Contas correntes versus contas n&atilde;o regularizadas</p>\r\n<p>Elisabete Silva</p>', 1, 3, 1, 23, 1, 5, NULL, NULL, NULL, '2018-03-14 18:28:30', NULL),
(58, 'opção de pagamento de publicidade nas encomendas', '<p>PHC - Criar op&ccedil;&atilde;o de pagamento de publicidade nas encomendas</p>', 1, 3, 1, 24, 2, 5, NULL, NULL, NULL, '2018-03-14 18:30:02', NULL),
(59, 'vendas com linha de produto vazia', '<p>Desde dezembro que h&aacute; vendas com linha de produto vazia, corrigir</p>', 1, 3, 1, 8, 1, 5, NULL, NULL, NULL, '2018-03-14 18:31:49', NULL),
(60, 'Faturas sem dados clientes loja On-line Dieta3Passos_emissao novas faturas', '<p>Na sequ&ecirc;ncia do erro com as faturas da loja on-line que aparecem sem os dados solicitados pelos clientes (nomeadamente sem NIF) e aparecem apenas como Cliente WEB, precisamos de enviar as faturas corrigidas a estes clientes, pelo que solicito a anula&ccedil;&atilde;o (nota de credito) das faturas sem dados e a emiss&atilde;o de novas faturas com dados.</p>\r\n<p>Preciso por favor que me digam quando poder&atilde;o solucionar este problema.</p>\r\n<p>Como serei eu a enviar estas faturas para os clientes quero saber como as posso imprimir (ou criar doc. PDF), uma vez que n&atilde;o tenho permiss&atilde;o para o fazer.</p>', 1, 3, 1, 11, 1, 5, NULL, NULL, NULL, '2018-03-14 18:35:19', NULL),
(61, '(Pedido 138)FW: Listagem de Conta Corrente da Clínica 765', '<p>Bom dia,&nbsp;</p>\r\n<p>E se em vez de extrair e colar no corpo de texto num e-mail, passar a enviar a informa&ccedil;&atilde;o em excel num anexo no e-mail? &Eacute; que quando se vai directamente &agrave; conta corrente no PHC consegue-se extrair para excel.&nbsp;</p>\r\n<p>Nota Filipe: &eacute; possivel ver esta informa&ccedil;&atilde;o no Portal</p>', 1, 3, 1, 12, 1, 3, NULL, NULL, NULL, '2018-03-14 18:40:46', NULL),
(62, '(Pedido 140)FW: 379 BC Alfragide | stock', '<p>Tinha ficado com a ideia de que a utiliza&ccedil;&atilde;o deste c&oacute;digo automaticamente movimentaria os produtos em quest&atilde;o. Aparecendo ent&atilde;o os c&oacute;digos no &ldquo;An&aacute;lise de Armaz&eacute;ns (Cl&iacute;nicas)&rdquo; terei ent&atilde;o de adicionar os movimentos dos dois produtos envolvidos, certo?</p>', 1, 3, 1, 12, 1, 5, NULL, NULL, NULL, '2018-03-14 18:42:41', NULL),
(63, 'O cliente recebeu esta encomenda 2 vezes…', '<p>O cliente recebeu esta encomenda 2 vezes&hellip;</p>\r\n<p>Em rela&ccedil;&atilde;o ao chocolate quente, n&atilde;o tenho como confirmar se o erro foi meu ou do armaz&eacute;m, pois n&atilde;o me aparece nas encomendas importadas, mas na encomenda do cliente apenas pediram 2 e foram 12&hellip;. (12x2).</p>\r\n<p>O cliente vai ficar com algumas coisas, mas &eacute; poss&iacute;vel que tenha de pedir para recolherem produto&hellip;</p>\r\n<p>Podem verificar o que aconteceu?</p>', 1, 3, 1, 12, 1, 5, NULL, NULL, NULL, '2018-03-14 18:47:02', NULL),
(64, 'pasta na Dropbox das encomendas do pferreira', '<p class=\"MsoNormal\">Continuo sem acesso &agrave; pasta na Dropbox das encomendas do pferreira (pedido desde Janeiro) e da jcarvalhinho.</p>', 1, 3, 1, 12, 1, 7, '2018-01-30', '2018-03-20', NULL, '2018-03-15 08:37:34', NULL),
(66, 'recolhas chronopost', '<p>Nao est&atilde;o a passar as recolhas chronopost</p>', 1, 3, 1, 12, 3, 5, '2018-03-15', '2018-03-15', NULL, '2018-03-15 11:08:46', NULL),
(67, 'Alteração de facturas', '<p>Alterar as facturas(Creditar?)</p>\r\n<p>Facturas n&ordm;:</p>\r\n<p>78 de 1-03-2018</p>\r\n<p>7&nbsp; de 22-09-2017</p>\r\n<p>47 de 4-12-2017</p>', 1, 3, 1, 19, 1, 5, NULL, NULL, NULL, '2018-03-16 11:27:47', NULL),
(68, 'Lançamento de facturas de comissoes', '<p>Lan&ccedil;amento de facturas de comissoes (melhorar)</p>', 1, 3, 1, 3, 1, 5, NULL, NULL, NULL, '2018-03-16 11:30:57', NULL),
(69, 'Loja online - verificar importação', '<p>Loja online - verificar importa&ccedil;&atilde;o (moradas)</p>', 1, 3, 1, 5, 1, 5, NULL, NULL, NULL, '2018-03-16 11:52:37', NULL),
(70, 'preparaçao dos lotes para o Keyinvoice', '<p>Clinicas Lotes, prepara&ccedil;ao dos lotes para o Keyinvoice</p>', 1, 3, 1, 5, 1, 5, NULL, NULL, NULL, '2018-03-16 12:41:38', NULL),
(71, 'proposta PHC', '<p>Pedir proposta - M&oacute;dulo de PHC - \"Logistica\" , \"Controlo Doc\" , \"Consolida&ccedil;&atilde;o\"</p>', 1, 3, 1, 5, 1, 5, NULL, NULL, NULL, '2018-03-16 12:42:32', NULL),
(72, 'configuracao de assinaturas de email', '<p>configuracao de assinaturas de email (126 contas de email)</p>', 1, 3, 2, 7, 1, 7, '2018-03-01', '2018-03-30', NULL, '2018-03-16 12:44:25', NULL),
(73, 'Reorganizar cabos bastidor( a espera de switch e cabos Ricardo Mecha)', '<p>Reorganizar cabos bastidor( a espera de switch e cabos ricardo mecha)</p>', 1, 3, 2, 7, 1, 7, '2018-03-20', '2018-03-20', NULL, '2018-03-16 12:44:57', NULL),
(74, 'Retificao fatura zeltica', '<p>Retificao fatura zeltica</p>', 1, 3, 1, 7, 1, 7, NULL, NULL, NULL, '2018-03-16 12:45:43', NULL),
(75, 'No PHC fazer o controlo de qualidade do Produto', '<p>No PHC fazer o controlo de qualidade do Produto&nbsp;</p>', 1, 3, 1, 17, 1, 5, NULL, NULL, NULL, '2018-03-16 12:50:23', NULL),
(76, 'Receber emails - saldos em aberto do Paulo Ferreira e as vendas da Joana Carvalhinho', '<p>&Eacute; necess&aacute;rio que a Filipa Mendon&ccedil;a comece a receber os emails dos saldos em aberto do Paulo Ferreira e as vendas da Joana Carvalhinho.</p>\r\n<p>A Rita Inoc&ecirc;ncio tamb&eacute;m continua sem receber as vendas gerais</p>\r\n<p>A Joana Carvalhinho tamb&eacute;m deixou de receber o email das vendas das suas clinicas tamb&eacute;m</p>', 1, 3, 1, 18, 1, 5, NULL, NULL, NULL, '2018-03-16 12:52:49', NULL),
(77, 'Receber emails - A Joana Albino e a Sónia Pinto continuam sem receber este e-mail', '<p>A Joana Albino e a S&oacute;nia Pinto continuam sem receber este e-mail (nunca receberam) e a Ana Rita Inoc&ecirc;ncio continua sem receber desde Fevereiro.</p>\r\n<p>&nbsp;</p>\r\n<p>Por favor inclui tamb&eacute;m linha das vendas do Vendedor D3P-WEB. quest&atilde;o da fatura online da 164 que est&aacute; a dobrar.</p>', 1, 3, 1, 12, 2, 5, NULL, NULL, NULL, '2018-03-16 12:53:53', NULL),
(78, 'Alterar nome nas contas correntes/Filipe Costa', '<p>Ainda est&aacute; o meu nome nas Contas Corrente quando h&aacute; movimentos em numer&aacute;rio. N&atilde;o faz sentido (e sinceramente n&atilde;o adoro) . <br /> Podes corrigir nas Contas de todas as cl&iacute;nicas sff?&nbsp;</p>', 1, 3, 1, 12, 1, 5, '2018-03-20', '2018-03-20', NULL, '2018-03-16 12:55:14', NULL),
(79, 'Erros de Importação', '<p>Erros de importa&ccedil;&atilde;o do m&ecirc;s passado. As comiss&otilde;es j&aacute; foram processadas.&nbsp;</p>', 1, 3, 1, 12, 1, 5, NULL, NULL, NULL, '2018-03-16 12:57:01', NULL),
(81, 'Lista de Documentos nao regularizados', '<p>&nbsp;Lista de Documentos nao regularizados do delegado: Maria Bas&iacute;lio.</p>\r\n<p>Nestes envios faz mais sentido a factura&ccedil;&atilde;o vir com iva j&aacute; que a cl&iacute;nica tem de nos devolver todo esse valor.&nbsp;</p>', 1, 3, 3, 12, 1, 5, NULL, NULL, NULL, '2018-03-16 12:58:36', NULL),
(82, 'GT aparece logo com “produtos esgotados” awdawd', '<p>Esta GT aparece logo com &ldquo;produtos esgotados&rdquo; no topo mas os produtos est&atilde;o com lote &agrave; exce&ccedil;&atilde;o do &uacute;ltimo&hellip;. Porqu&ecirc;?</p>\n<p>awdwadaw</p>', 1, 3, 1, 12, 1, 5, NULL, NULL, NULL, '2018-03-16 13:00:09', '2018-03-29 16:36:52'),
(83, 'Notas de credito', '<p>Não sei se foram anuladas (suponho que sim, mas não sei como se pode verificar). E Notas de credito só consigo encontrar 3 (1 de 2017 e 2 de 2018).</p><p>Ex.:</p><p>- 2018 está feita a correspondência das faturas PHC com os dados dos clientes online que solicitaram NIF sendo que não consigo fazer a correspondência com o PHC de 2 clientes online ( Sandra Marques e Selma Figueiredo).</p><p>- Produtos esgotados: detetei, em 2018, que existem faturas que seguiram com produtos esgotados, como saber se estes produtos esgotados foram posteriormente enviados aos clientes? (ver coluna comentários)</p>', 1, 3, 1, 3, 1, 3, NULL, NULL, NULL, '2018-03-16 13:01:11', '2018-04-04 09:14:50'),
(84, 'Email de Vendas Gerais', '<p>A Rita Inoc&ecirc;ncio tamb&eacute;m continua sem receber as vendas gerais</p>\r\n<p>A Joana Carvalhinho tamb&eacute;m deixou de receber o email das vendas das suas clinicas tamb&eacute;m</p>', 1, 3, 1, 18, 1, 5, NULL, NULL, NULL, '2018-03-16 14:35:12', NULL),
(86, 'exclusão de um produto na nota de encomenda', '<p>exclus&atilde;o de um produto na nota de encomenda,&nbsp;Refor&ccedil;o tamb&eacute;m o pedido (n&atilde;o encontro o e-mail mas foi pedido pelo menos pessoalmente) de ter acesso &agrave; inclus&atilde;o ou exclus&atilde;o de um produto na nota de encomenda:</p>', 1, 3, 1, 12, 1, 5, NULL, NULL, NULL, '2018-03-16 15:11:03', NULL),
(87, 'O envio da encomenda D3P1142WEB foi processada.', '<p>&nbsp;</p>\r\n<p><span style=\"font-size: 12.0pt; color: black;\">Passados 15 dias e ainda n&atilde;o recebi qualquer tipo de factura. Gostaria que fizessem o envio da mesma o mais depressa poss&iacute;vel ou terei que fazer uma reclama&ccedil;&atilde;o.&nbsp;</span></p>', 1, 3, 1, 11, 1, 5, NULL, NULL, NULL, '2018-03-16 15:13:50', NULL),
(88, 'Análise de facturação', '<p>An&aacute;lise de factura&ccedil;&atilde;o</p>', 1, 3, 1, 5, 1, 5, NULL, NULL, NULL, '2018-03-16 15:14:42', NULL),
(89, 'PHC - Objetivos Dietética', '<p>PHC - Objetivos Diet&eacute;tica - Solicito que seja inclu&iacute;do nos objetivos os seguintes campos, de modo a que possa registar os de Mar&ccedil;o/18.&nbsp;</p>\r\n<p>&bull; Auchan</p>\r\n<p>&bull; Alimenta&ccedil;&atilde;o</p>', 1, 3, 1, 24, 1, 5, NULL, NULL, NULL, '2018-03-16 15:37:45', NULL),
(90, 'taxa de conversão Leads sites', '<p>taxa de convers&atilde;o Leads sites,&nbsp;An&aacute;lise de taxa de convers&atilde;o Leads sites. Com o objectivo de analisar mensalmente a taxa de convers&atilde;o das leads que recebemos nos nossos sites</p>', 7, 3, 1, 13, 1, 2, NULL, NULL, NULL, '2018-03-16 15:51:40', NULL),
(91, 'Dietimport/Sup- Rocha - Descontos.', '<p>Dietimport/Sup- Rocha - Descontos. Mensalmente temos produtos em campanha c/ os clientes Dietimport e Supermercado Rocha, sendo que os descontos habituais s&atilde;o de 50% + 3%, embora no caso dos produtos em campanha o desconto seja 50% + 10%.</p>\r\n<p>Gostaria de saber a viabilidade de colocarmos a passar automaticamente para a Nota de Encomenda, os descontos mensais que poderiam ser registados no PHC, tal como fa&ccedil;o para as campanhas.</p>\r\n<p>Vantagem: minimizar a quantidade de notas de cr&eacute;dito que fazemos por erro de desconto, a este cliente</p>', 1, 3, 1, 24, 1, 5, NULL, NULL, NULL, '2018-03-16 15:54:03', NULL),
(92, 'Facturas Keyinvoice (6,7)', '<p>A fatura n&ordm;6 e n&ordm;7 aparece no Keyinvoice e n&atilde;o aparece na extra&ccedil;&atilde;o das comiss&otilde;es do PHC.</p>', 1, 3, 1, 18, 1, 5, NULL, NULL, NULL, '2018-03-16 15:54:51', NULL),
(93, 'Outlook Carla Saraiva - Cheio', '<p>Outlook Carla Saraiva - Cheio</p>', 1, 3, 3, 24, 1, 7, '2018-03-20', '2018-03-20', '2018-03-20', '2018-03-19 09:22:46', NULL),
(95, 'Portatil novo', '<p>Portatil Novo - Ana Mendes</p>', 1, 3, 3, 25, 1, 7, '2018-03-20', '2018-03-20', '2018-03-20', '2018-03-19 09:31:07', NULL),
(96, 'informação das Finanças sobre facturas', '<p>Responder ao pedido de informa&ccedil;&atilde;o das Finan&ccedil;as sobre facturas</p>', 1, 3, 3, 23, 1, 5, NULL, NULL, NULL, '2018-03-19 09:55:59', NULL),
(97, 'Parceria do cliente MAT. CIR. C. SOARES, LDA', '<p>A parceria do cliente MAT. CIR. C. SOARES, LDA. foi criada de raiz no final de 2017. Foi realizada uma contabiliza&ccedil;&atilde;o juntamente c/ o cliente e emitida uma Guia de Consigna&ccedil;&atilde;o Inicial.&nbsp;</p>\r\n<p>Na altura foi acordado que esta seria uma das parcerias que passaria a ser monitorizada/atualizada automaticamente. Temos entretanto, desde a guia de consigna&ccedil;&atilde;o inicial, a emiss&atilde;o de novos documentos, quer de envio quer de devolu&ccedil;&atilde;o.</p>\r\n<p>Questiono como deveremos proceder para que a parceria seja atualizada?&nbsp;</p>', 1, 3, 1, 24, 1, 5, NULL, NULL, NULL, '2018-03-19 10:04:13', NULL),
(98, 'Listagem de Farmácia/colaboradores Norte', '<p>\"Precisamos de uma listagem com o nomes das farm&aacute;cias da Zona do Norte; por&nbsp; delegado;&nbsp;</p>\r\n<p>com moradas; contribuintes; grupos e por classifica&ccedil;&atilde;o se &eacute; cliente de Farm&aacute;cia ou de Dieta&nbsp;</p>\r\n<p>s&oacute; para os delegados do Norte.</p>\r\n<p>Delegados Norte</p>\r\n<p>Anabela Farias,Brigite,Elsa,Joel,M&aacute;rcio,Ricardo,Rodolfo</p>\r\n<p>\"</p>', 1, 3, 3, 20, 1, 3, '2018-03-05', '2018-03-19', '2018-03-19', '2018-03-19 10:35:59', NULL),
(99, 'vendas online em nome “Cliente Web”', '<p>Continuamos a ter vendas online em nome &ldquo;Cliente Web&rdquo;.</p>\r\n<p>A &uacute;ltima foi a encomenda D3P1197WEB (Bruno Ribeiro)</p>', 1, 3, 1, 18, 1, 5, NULL, NULL, NULL, '2018-03-19 10:43:52', NULL),
(100, 'Erro PHC - Dietética - só faz guia de transporte sem stock , não gera a fatura', '<p>Erro PHC - Dietetica - Ao colocarmos a informa&ccedil;&atilde;o acima o sistema apenas faz a guia de transporte sem stock , n&atilde;o gera a fatura&nbsp;</p>\r\n<p>Temos agora aqui 3 encomendas para qual o sistema apenas gerou guia de transporte,4880,5458,5459,</p>', 1, 3, 1, 24, 2, 5, NULL, NULL, NULL, '2018-03-19 11:28:07', NULL),
(101, 'Carla Saraiva nao envia emails', '<p>Carla Saraiva nao consegue enviar emails</p>', 1, 3, 3, 24, 1, 7, '2018-03-19', '2018-03-19', '2018-03-20', '2018-03-19 15:24:05', NULL),
(102, 'Assiduidade (Faltas e Férias) – Interface utilizador', '<p>Assiduidade (Faltas e F&eacute;rias) &ndash; Interface utilizador</p>', 18, 3, 1, 26, 1, 2, NULL, NULL, NULL, '2018-03-19 15:37:07', NULL),
(103, 'Assiduidade (Faltas e Férias) – Interface Superior Hierárquico', '<p>Assiduidade (Faltas e F&eacute;rias) &ndash; Interface Superior Hier&aacute;rquico</p>', 18, 3, 1, 26, 1, 2, NULL, NULL, NULL, '2018-03-19 15:56:30', NULL),
(104, 'O CRM de dietética não funciona minimamente e agora nem o PHC', '<p>O CRM de diet&eacute;tica n&atilde;o funciona minimamente e agora nem o PHC</p>', 21, 3, 1, 8, 1, 2, NULL, NULL, NULL, '2018-03-19 16:01:35', NULL),
(105, 'Portátil Novo', '<p>Port&aacute;til Novo - novo colaborador Rui Cruz</p>', 1, 3, 3, 3, 1, 7, '2018-03-20', '2018-03-20', '2018-03-20', '2018-03-19 16:09:04', NULL),
(106, 'Rectificar a importação do shopify/filipe costa', '<p>Rectificar a importa&ccedil;&atilde;o do shopify</p>', 1, 3, 1, 11, 1, 5, '2018-03-20', '2018-03-20', NULL, '2018-03-19 16:16:24', NULL),
(107, 'Diagnostico de rede (a espera do switch Ricardo Mecha)', '<p>Diagnostico de rede - Problemas críticos da rede interna</p>', 1, 3, 1, 8, 1, 7, '2018-03-20', '2018-03-20', NULL, '2018-03-19 16:17:02', '2018-04-03 10:11:59'),
(108, 'associar os dossiers pendentes das farmácias', '<p>Queria pedir que associassem os dossiers pendentes das farm&aacute;cias ao estabelecimento e n&atilde;o ao NIF.</p>\r\n<p>H&aacute; farm&aacute;cias com o mesmo NIF e quando vamos colocar os pendentes aparecem os das duas farm&aacute;cias.</p>', 1, 3, 1, 20, 1, 5, NULL, NULL, NULL, '2018-03-19 16:18:39', NULL),
(109, 'Actualizar Notas de Encomendas dos delegados', '<p>Precisamos que as Notas de Encomendas dos delegados sejam atualizadas, as linhas a amarelo devem desaparecer da nova Nota de Encomenda</p>', 1, 3, 1, 20, 1, 5, NULL, NULL, NULL, '2018-03-19 16:24:39', NULL),
(110, 'PHC - Exportar NE', '<p>Segue em anexo a altera&ccedil;&otilde;es que s&atilde;o necess&aacute;rias fazer no PHC para exportarmos para as NE, pode ajudar por favor.</p>', 1, 3, 1, 20, 1, 5, NULL, NULL, NULL, '2018-03-19 16:25:51', NULL),
(111, 'Produtos não contabilizados', '<p>Tivemos em Novembro, Dezembro e Janeiro uma campanha em que com um de dois c&oacute;digos eram facturados dois produtos simultaneamente, mas quando extra&iacute;mos o stock da cl&iacute;nica n&atilde;o me parece que estes produtos estejam contabilizados porque n&atilde;o aparecem no movimento desses produtos. Pessoalmente posso explicar melhor mas pe&ccedil;o que agendem para se verificar esta situa&ccedil;&atilde;o, sff.&nbsp;</p>', 1, 3, 1, 12, 1, 5, NULL, NULL, NULL, '2018-03-19 16:28:32', NULL),
(113, 'Diferença KI / PHC', '<p>Diferen&ccedil;a KI / PHC</p>', 1, 3, 1, 5, 1, 5, NULL, NULL, NULL, '2018-03-19 16:30:29', NULL),
(114, 'envio Email de vendas', '<p>&nbsp;J&aacute; n&atilde;o &eacute; preciso envio das vendas da MP</p>\r\n<p>&nbsp;J&aacute; n&atilde;o &eacute; preciso envio das vendas da FM</p>\r\n<p>&nbsp;J&aacute; n&atilde;o &eacute; preciso envio das vendas do FC</p>\r\n<p>&nbsp;Filipamendonca@farmodietica.com j&aacute; n&atilde;o precisa de receber as vendas do MCS</p>', 1, 3, 1, 12, 1, 5, NULL, NULL, NULL, '2018-03-19 16:31:41', NULL),
(115, 'Lançar o inventário da Zeltica', '<p>Lan&ccedil;ar o invent&aacute;rio da Zeltica</p>', 1, 3, 1, 28, 1, 5, NULL, NULL, NULL, '2018-03-19 16:32:59', NULL),
(116, 'PHC - objetivos da equipa  Loring, Tattu, PharmaTea', '<p>Devem ser criados no PHC os espa&ccedil;os para os objetivos desta equipa: Loring, Tattu, PharmaTea</p>', 1, 3, 1, 28, 1, 5, '2018-03-06', '2018-03-30', NULL, '2018-03-19 16:36:06', NULL),
(118, 'Dificuldade a fazer uma factura', '<p>O envio da encomenda D3P1142WEB foi processada. Ser&aacute; que podemos passar esta fatura com os dados para a senhora.</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: PT; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">O contribuinte &eacute; o 507880889</span></p>\r\n<p><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: PT; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\">Email da Gabriela</span></p>', 1, 3, 1, 11, 1, 5, NULL, NULL, NULL, '2018-03-19 16:43:17', NULL),
(119, 'Facturas que não saíram', '<p>URGENTE - Solucionar c/ Armaz&eacute;m- A Rute informou que no dia 07/03 teve problemas na emiss&atilde;o de faturas no fecho dia dia e que vos alertou para tal.</p>\r\n<p>Uma das faturas eu n&atilde;o saiu &eacute; a eu segue em baixo, mas segundo o armaz&eacute;m existe mais nestas condi&ccedil;&otilde;es e que eles n&atilde;o conseguem encontrar.</p>\r\n<p>Pedia-vos a v/ ajuda urgente uma vez que s&atilde;o encomendas que j&aacute; deviam estar no cliente.&nbsp;</p>', 1, 3, 1, 5, 1, 5, NULL, NULL, NULL, '2018-03-19 16:49:13', NULL),
(120, 'Permitir no acesso dos GZ ver a quantidade de stock', '<p>&nbsp;feedback Portal- Aqui vai em anexo a listagem actual de e-mails das cl&iacute;nicas ativas.&nbsp;</p>\r\n<p>Novas sugest&otilde;es:</p>\r\n<p>Stocks:</p>\r\n<p>permitir no acesso dos GZ ver a quantidade de stock conforme est&aacute; no PHC. N&atilde;o no acesso das cl&iacute;nicas.</p>\r\n<p>Colocar encomenda (pelo GZ e pela cl&iacute;nica)</p>', 1, 3, 1, 12, 1, 5, NULL, NULL, NULL, '2018-03-19 16:51:15', NULL),
(121, 'Gestão de pedidos/projetos', '<p>Abertura dos pedidos/projetos Pendentes no Portal ; organiza&ccedil;&atilde;o Pedidos/projetos pendentes;parametriza&ccedil;&otilde;es do Portal de Pedidos</p>', 1, 3, 1, 3, 1, 3, NULL, NULL, NULL, '2018-03-19 17:41:31', NULL),
(122, 'crm - coordenação - rrelatórios visitas - dá erro', '<p>CRM&nbsp;- Coordena&ccedil;&atilde;o - rrelat&oacute;rios visitas - d&aacute; erro</p>', 1, 3, 1, 8, 1, 5, NULL, NULL, NULL, '2018-03-20 09:41:24', NULL),
(124, 'PDA´S novos - Configurações', '<p>PDA&acute;S novos - Configura&ccedil;&otilde;es</p>', 1, 3, 1, 10, 2, 7, '2018-03-20', '2018-03-23', NULL, '2018-03-20 11:24:38', NULL),
(125, 'Algumas correções ao portal das clínicas:', '<p class=\"MsoNormal\">Algumas corre&ccedil;&otilde;es ao portal das cl&iacute;nicas:</p>\r\n<p>Vendas e consultas</p>', 1, 3, 1, 9, 3, 5, '2018-03-20', '2018-03-23', NULL, '2018-03-20 14:17:26', NULL),
(126, 'Portal clínicas - valores a zero', '<p class=\"MsoNormal\">Este &eacute; um screenshot da cl&iacute;nica 101 &ndash; Clin&aacute;lamo na p&aacute;gina inicial.</p>\r\n<p class=\"MsoNormal\">Outra coisa que j&aacute; tinha sido falada &eacute; ao alterar o nome da cl&iacute;nica ou o ano em alguma p&aacute;gina do portal, essa altera&ccedil;&atilde;o deve ser assumida para as outras p&aacute;ginas do portal. Por exemplo, se estiver na agenda e alterar a cl&iacute;nica para 101, ao ir consultar as vendas devo estar logo a consultar as vendas cesta cl&iacute;nica. Isso n&atilde;o est&aacute; a acontecer.</p>\r\n<p class=\"MsoNormal\">&nbsp;O mesmo digo em rela&ccedil;&atilde;o ao ano.&nbsp;</p>\r\n<p class=\"MsoNormal\">Agora na p&aacute;gina &ldquo;Vendas&rdquo;, por exemplo, vendas por cliente:</p>\r\n<p class=\"MsoNormal\">(ver Email do hugo Teixeira)</p>\r\n<p class=\"MsoNormal\">&nbsp;</p>', 1, 3, 1, 9, 3, 5, '2018-03-20', '2018-03-23', NULL, '2018-03-20 14:29:06', NULL),
(127, 'Lista de Vendas e objectivos gerais (Clínicas)', '<p class=\"MsoNormal\"><font color=\"#1f497d\" face=\"Calibri, sans-serif\"><span style=\"font-size: 14.6667px;\">Lista de Vendas e objectivos gerais (Cl&iacute;nicas) -&nbsp;</span></font><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; color: #1f497d;\">N&atilde;o acabou a importa&ccedil;&atilde;o de 6&ordf; e n&atilde;o tem feito desde ent&atilde;o. A tua ajuda, sff. </span></p>\r\n<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; color: #1f497d;\">Continuamos com as diferen&ccedil;as do dia 7 e 9 e agora dia 15 tamb&eacute;m.</span></p>\r\n<p class=\"MsoNormal\"><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; color: #1f497d;\">(Ver email da Madalena)</span></p>', 1, 3, 1, 12, 3, 5, '2018-03-20', '2018-03-23', NULL, '2018-03-20 14:37:53', NULL),
(128, 'crm não está a funcionar. - dashboard farma', '<p>crm n&atilde;o est&aacute; a funcionar. - dashboard farma. vis&atilde;o geral de Farmacia - Farmacia saude&nbsp;</p>\r\n<p>(ver email da Francisca)</p>', 1, 3, 1, 8, 3, 2, '2018-03-20', '2018-03-23', NULL, '2018-03-20 14:55:41', NULL),
(129, 'crm não está a funcionar. - dashboard farma - Não se consegue selecionar março', '<p>RE: crm n&atilde;o est&aacute; a funcionar. - dashboard farma -&nbsp;</p>\r\n<p class=\"MsoNormal\">N&atilde;o se consegue selecionar mar&ccedil;o</p>', 1, 3, 1, 8, 3, 2, NULL, '2018-03-23', NULL, '2018-03-20 15:09:41', NULL),
(130, 'Marcia Cardoso - Falta de espaço na DropBox', 'Falta de espaço na DropBox\r\n\r\nEnviado email com as instruções.\r\n\r\nligamos-nos por teamviewer a 21-03-2018, executamos as instruções para enviar convite \"marciacardoso20@farmodietica.com\".', 1, 3, 3, 3, 3, 7, '2018-03-20', '2018-03-21', NULL, '2018-03-20 16:00:51', NULL),
(131, 'Portal Farmacias - DES', '<p>Front End</p>', 20, 6, 3, 2, 1, 6, NULL, NULL, NULL, '2018-03-20 18:07:05', NULL),
(132, 'Front end - DB3 Dietéticas', '<p>front end.</p>', 24, 6, 3, 2, 1, 6, NULL, NULL, NULL, '2018-03-20 18:08:23', NULL),
(133, 'configuração de  equipamentos para novos colaboradores', 'instalação e configuração', 1, 7, 3, 25, 1, 7, '2018-03-20', '2018-03-20', NULL, '2018-03-21 09:34:01', NULL),
(134, 'reconfiguracao de rede armazem mem martins', 'reconfiguracao de rede wireless do aramzem e configuração de ips de workstation', 1, 7, 3, 10, 1, 7, '2018-03-19', '2018-03-19', NULL, '2018-03-21 09:55:44', NULL),
(135, 'criacao de email farmacias@farmodietica.com(aprovacao francisca)', '<p>criacao de email farmacias@farmodietica.com e configuracao no outlook</p>\r\n<p>&nbsp;</p>\r\n<p>nao foi aprovado pela franciscas ficou sem efeito&nbsp;</p>', 1, 7, 3, 7, 1, 7, NULL, NULL, '2018-03-23', '2018-03-21 10:03:37', NULL),
(136, 'configuracao de impressoras nos macs Design', '<p>configuracao de impressoras nos macs Design</p>', 1, 7, 3, 7, 1, 7, NULL, NULL, NULL, '2018-03-21 10:05:14', NULL),
(137, 'partilha de pasta nasdesign (a espera de Ricardo Mecha)', '<p>partilha de pasta nasdesign (a espera de Ricardo Mecha) no pc Ana Correia , solicitado por Andre Loureiro.</p>', 1, 7, 1, 7, 1, 7, NULL, NULL, NULL, '2018-03-21 10:11:11', NULL),
(140, 'aquisicao e configuracao de licencas office ,(a espera de Ricardo Mecha)', '<p>aquisicao e configuracao de licencas office , colaborades , ana correia , Joana Carneiro , Rui Cruz , Ana mendes&nbsp;</p>', 1, 7, 1, 7, 1, 7, '2018-03-21', '2018-03-28', NULL, '2018-03-21 10:15:01', NULL),
(141, 'Portal de Nutriçao - Testes portal e elaboração do manual', '<p>Testes ao portal de nutri&ccedil;&atilde;o,&nbsp;elabora&ccedil;&atilde;o&nbsp;de manual</p>\r\n<p>Actualiza&ccedil;&atilde;o do documento de especifica&ccedil;&atilde;o com as melhorias pedidas em reuni&atilde;o.</p>', 16, 3, 1, 9, 2, 3, NULL, NULL, NULL, '2018-03-21 10:20:02', NULL),
(142, 'Manutenção de Impressora Xerox-Nutricao', '<p>Manuten&ccedil;&atilde;o de Impressora Xerox-Nutricao&nbsp;</p>', 1, 7, 3, 7, 1, 7, NULL, NULL, NULL, '2018-03-21 10:28:05', NULL),
(143, 'tomadas de rede Pedro Teixeira', '<p>tomadas de rede Pedro Teixeria</p>', 1, 7, 1, 7, 1, 7, NULL, NULL, NULL, '2018-03-21 10:29:59', NULL),
(144, 'Rever as linhas de Produto', '<p class=\"MsoNormal\">Algu&eacute;m mexeu nas linhas de produto?</p>\r\n<p class=\"MsoNormal\">Vejo produtos j&aacute; com as novas linhas e outros que est&atilde;o antigos. Est&aacute; uma confus&atilde;o. podem esclarecer-me pf? Por exemplo aparecem estes amarelos e depois tb os individuais.</p>\r\n<p class=\"MsoNormal\">(ver email da Francisca)</p>', 1, 3, 1, 8, 3, 5, '2018-03-20', '2018-03-23', NULL, '2018-03-21 11:13:33', NULL),
(145, 'equipamento para formacao de ingles', '<p class=\"MsoNormal\">Ricardo,</p>\r\n<p class=\"MsoNormal\">&nbsp;</p>\r\n<p class=\"MsoNormal\">Conforme falamos, durante as pr&oacute;ximas semanas, das 17h &ndash; 19h ir&aacute; haver uma forma&ccedil;&atilde;o de ingl&ecirc;s em que o formador precisa de um computador para colocar uma pen e ligar &agrave; TV/projetor da sala de reuni&otilde;es maior.</p>\r\n<p class=\"MsoNormal\">&nbsp;</p>\r\n<p class=\"MsoNormal\">Adicionalmente, tamb&eacute;m amanh&atilde; por volta das 11h30 e das 17h00 vamos estar com candidatos a uma vaga na empresa, em que gostar&iacute;amos que fizessem uns exerc&iacute;cios (excel e word). &Eacute; poss&iacute;vel que o fa&ccedil;am num computador &ldquo;extra&rdquo; para esse efeito, e n&atilde;o no meu?</p>\r\n<p>Filipa Grilo</p>', 1, 7, 3, 7, 1, 7, NULL, NULL, NULL, '2018-03-21 11:17:54', NULL),
(146, 'instalação e configuração outlook Francisca', '<p>instala&ccedil;&atilde;o e configura&ccedil;&atilde;o outlook Francisca&nbsp;</p>', 1, 7, 1, 7, 1, 7, NULL, NULL, NULL, '2018-03-21 11:19:40', NULL),
(147, 'Melhorias ao Portal da Nutrição', '<p>Melhorias ao Portal da Nutri&ccedil;&atilde;o - de acordo com o que est&aacute; no documento de especifica&ccedil;&atilde;o que est&aacute; na DropBOX (PROJETOS_Documentos_ESPECIFICA&Ccedil;&Atilde;O) n&atilde;o fechar o pedido at&eacute; estar tudo validado (Elsa)</p>', 16, 3, 1, 9, 3, 2, '2018-02-20', NULL, NULL, '2018-03-21 11:51:43', NULL),
(148, 'Front End Páginas de Blog, Receitas, Produtos e Casos Sucesso', '<ol>\r\n<li>Finaliza&ccedil;&atilde;o da p&aacute;gina de Blog.</li>\r\n<li>Finaliza&ccedil;&atilde;o do m&oacute;dulo de pagina&ccedil;&atilde;o.</li>\r\n<li>Finaliza&ccedil;&atilde;o do m&oacute;dulo de caminho (breadcrumbs) para p&aacute;gina de Receitas e Produtos.</li>\r\n<li>in&iacute;cio da P&aacute;gina de \"Casos de Sucesso\":</li>\r\n<ul>\r\n<li>Investiga&ccedil;&atilde;o de solu&ccedil;&atilde;o para banner.</li>\r\n<li>in&iacute;cio vers&atilde;o Desktop (vers&atilde;o Wide desktop)</li>\r\n</ul>\r\n</ol>', 6, 6, 1, 8, 1, 6, NULL, NULL, NULL, '2018-03-21 13:08:48', NULL),
(149, 'solução interna para partilha de ficheiros(email enviado proposta filerun)', '<p>solu&ccedil;&atilde;o interna para parilha de ficheiros</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 1, 7, 1, 7, 1, 7, '2018-03-19', '2018-03-31', NULL, '2018-03-21 14:11:16', NULL),
(150, 'instalacao e configuracao de office no servidor de encomendas', '<p>instalacao e configuracao de office no servidor de encomendas&nbsp;</p>', 1, 7, 3, 5, 1, 7, '2018-03-22', '2018-03-22', NULL, '2018-03-22 12:30:52', NULL),
(151, 'istalacao de software mac sergio design(falta pen drive)', '<p>istalacao de software mac sergio design</p>', 1, 7, 1, 7, 1, 7, '2018-03-23', '2018-03-31', NULL, '2018-03-22 12:40:02', NULL),
(152, 'reconfiguracao de servidor de encomendas', '<p>reconfiguracao de servidor de encomendas&nbsp;</p>', 1, 7, 3, 7, 1, 7, NULL, NULL, NULL, '2018-03-22 12:40:46', NULL),
(154, 'Estilizar botão dos depósitos.', '<p>Estilizar bot&atilde;o dos dep&oacute;sitos.</p>', 11, 2, 1, 2, 1, 6, NULL, NULL, NULL, '2018-03-23 09:37:32', NULL),
(155, 'Férias', '<p>Pedido&nbsp;para cria&ccedil;&atilde;o de f&eacute;rias</p>', 27, 1, 1, 4, 1, 3, NULL, NULL, NULL, '2018-03-23 09:50:02', NULL),
(156, 'Férias', '<p>Pedido de f&eacute;rias para registo de atividade</p>', 27, 1, 1, 4, 1, 1, NULL, NULL, NULL, '2018-03-23 10:02:06', NULL),
(157, 'Férias', '<p>Pedido de f&eacute;rias para registo de actividades</p>', 27, 1, 1, 4, 1, 5, NULL, NULL, NULL, '2018-03-23 10:03:18', NULL),
(158, 'Férias', '<p>Pedido de f&eacute;rias para registo de actividades</p>', 27, 1, 1, 4, 1, 7, NULL, NULL, NULL, '2018-03-23 10:03:47', NULL),
(159, 'Férias', '<p>Pedido de f&eacute;rias para registo de actividades</p>', 27, 1, 1, 4, 1, 2, NULL, NULL, NULL, '2018-03-23 10:04:13', NULL),
(160, 'Férias', '<p>Pedido de f&eacute;rias para registo de actividades</p>', 27, 1, 1, 4, 1, 6, NULL, NULL, NULL, '2018-03-23 10:05:12', NULL),
(161, 'Ausência', '<p>Pedido de aus&ecirc;ncia para registo de actividades</p>', 27, 1, 1, 4, 1, 7, NULL, NULL, NULL, '2018-03-23 10:33:52', NULL),
(162, 'Ausência', '<p>Pedido de&nbsp;aus&ecirc;ncia&nbsp;para registo de actividades</p>', 27, 1, 1, 4, 1, 2, NULL, NULL, NULL, '2018-03-23 10:34:24', NULL),
(163, 'Ausência', '<p>Pedido de&nbsp;aus&ecirc;ncia&nbsp;para registo de actividades</p>', 27, 1, 1, 4, 1, 1, NULL, NULL, NULL, '2018-03-23 10:34:45', NULL),
(164, 'Ausência', '<p>Pedido de&nbsp;aus&ecirc;ncia&nbsp;para registo de actividades</p>', 27, 1, 1, 4, 1, 5, NULL, NULL, NULL, '2018-03-23 10:35:39', NULL),
(165, 'Ausência', '<p>Pedido de&nbsp;aus&ecirc;ncia&nbsp;para registo de actividades</p>', 27, 1, 1, 4, 1, 3, NULL, NULL, NULL, '2018-03-23 10:36:04', NULL),
(166, 'Ausência', '<p>Pedido de&nbsp;aus&ecirc;ncia&nbsp;para registo de actividades</p>', 27, 1, 1, 4, 1, 6, NULL, NULL, NULL, '2018-03-23 10:36:44', NULL),
(167, 'Reunião', '<p>Pedido de reuni&atilde;o para registo de actividades</p>', 27, 1, 1, 4, 1, 7, NULL, NULL, NULL, '2018-03-23 10:37:34', NULL),
(168, 'Reunião', '<p>Pedido de reuni&atilde;o para registo de actividades</p>', 27, 1, 1, 4, 1, 2, NULL, NULL, NULL, '2018-03-23 10:37:59', NULL),
(169, 'Reunião', '<p>Pedido de reuni&atilde;o para registo de actividades</p>', 27, 1, 1, 4, 1, 1, NULL, NULL, NULL, '2018-03-23 10:38:26', NULL),
(170, 'Reunião', '<p>Pedido de reuni&atilde;o para registo de actividades</p>', 27, 1, 1, 4, 1, 5, NULL, NULL, NULL, '2018-03-23 10:39:09', NULL),
(171, 'Reunião', '<p>Pedido de reuni&atilde;o para registo de actividades</p>', 27, 1, 1, 4, 1, 3, NULL, NULL, NULL, '2018-03-23 10:39:29', NULL),
(172, 'Reunião', '<p>Pedido de reuni&atilde;o para registo de actividades</p>', 27, 1, 1, 4, 1, 6, NULL, NULL, NULL, '2018-03-23 10:40:00', NULL),
(173, 'Melhorias na Plataforma (Paginação de pedidos, pesquisa, registo atividade)', '<ol>\r\n<li>Remover Pagina&ccedil;&atilde;o;</li>\r\n<li>Pesquisa Normalizada na p&aacute;gina pedidos;</li>\r\n<li>Cria&ccedil;&atilde;o de projecto Outros e respetivos pedidos para registo de atividade;</li>\r\n</ol>', 4, 1, 2, 2, 1, 1, NULL, NULL, NULL, '2018-03-23 11:10:51', '2018-04-03 13:33:03'),
(174, 'importação do keyinvoice', '<p>importa&ccedil;&atilde;o do keyinvoice</p>', 1, 7, 1, 12, 1, 7, NULL, NULL, NULL, '2018-03-23 11:23:33', NULL),
(175, 'ANA CORREIA', '<p>ANA CORREIA</p>', 1, 7, 3, 7, 1, 7, NULL, NULL, NULL, '2018-03-23 11:25:47', NULL),
(176, 'Fecho do Ano', '<p>Fecho do Ano</p>', 1, 5, 1, 23, 1, 5, NULL, NULL, NULL, '2018-03-23 11:57:44', NULL),
(177, 'Teste ao pedido', '<div id=\"Content\" style=\"margin: 0px; padding: 0px; position: relative; font-family: \'Open Sans\', Arial, sans-serif; text-align: center;\">\n<div class=\"boxed\" style=\"margin: 10px 28.7969px; padding: 0px; clear: both;\">\n<div id=\"lipsum\" style=\"margin: 0px; padding: 0px; text-align: justify;\">\n<p style=\"margin: 0px 0px 15px; padding: 0px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in enim dignissim, imperdiet sapien et, posuere eros. Sed magna magna, varius at nibh a, pretium tempus diam. Vestibulum lacinia leo at lobortis dictum. Ut quis auctor ipsum. Suspendisse ac purus id purus facilisis gravida a eget tellus. Duis feugiat ultricies tortor sodales dapibus. Quisque metus dui, volutpat sed tempor sed, ullamcorper eu mauris. Nullam fermentum felis dolor, id convallis ligula pharetra vitae. Fusce pulvinar eget sem a auctor. Duis sed nisi at felis molestie maximus. Cras et turpis tincidunt, convallis velit in, sollicitudin ex. Quisque facilisis lorem nec ex semper, et lacinia arcu vestibulum. Duis sed mi eu diam ornare laoreet. Praesent varius sit amet dui sit amet cursus.</p>\n<p style=\"margin: 0px 0px 15px; padding: 0px;\">Nunc congue scelerisque purus eu luctus. Proin vulputate viverra maximus. Praesent elementum magna est, vel placerat urna euismod quis. Praesent sollicitudin, leo posuere pulvinar congue, erat dui vestibulum magna, at bibendum tortor arcu ac neque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin a laoreet ipsum. In at urna nunc.</p>\n<p style=\"margin: 0px 0px 15px; padding: 0px;\">Morbi varius, turpis et iaculis sodales, neque nulla egestas ex, sed semper arcu dolor sit amet augue. Maecenas ipsum libero, pretium sodales elit eu, blandit imperdiet neque. Curabitur maximus lorem libero, ut porttitor elit convallis eget. Nulla at gravida nulla, ut suscipit sem. Aenean mattis leo et lectus viverra efficitur. Nulla tincidunt imperdiet metus, non commodo magna porttitor id. Quisque sed lectus ornare ex lobortis molestie. Sed vulputate libero quis dolor consectetur semper. Cras vitae metus mi. Ut arcu mauris, pellentesque ut arcu id, molestie rhoncus magna. Donec a enim eu mi rutrum pulvinar sed vitae elit. Proin eget nisi sed nisl facilisis efficitur quis nec mi. Fusce sit amet pharetra sem.</p>\n</div>\n</div>\n</div>', 2, 1, 1, 2, 1, 1, NULL, NULL, NULL, '2018-04-04 15:12:34', '2018-04-26 14:12:36'),
(178, 'Teste 2', '<p>Descri&ccedil;&atilde;o 2</p>', 3, 1, 1, 3, 1, 6, NULL, NULL, NULL, '2018-04-04 15:18:33', '2018-04-04 15:18:33'),
(179, 'Teste ao pedido editado', '<p>Teste ao pedido 3 mesmo</p>', 5, 1, 1, 2, 1, 5, NULL, NULL, NULL, '2018-04-04 15:27:25', '2018-04-04 16:19:14'),
(180, 'Teste 4', '<p>teste mesmo 4</p>', 2, 1, 1, 2, 1, 1, NULL, NULL, NULL, '2018-04-04 15:31:18', '2018-04-04 15:31:18'),
(181, 'Teste 5', '<p>Descri&ccedil;&atilde;o 5</p>', 2, 1, 1, 4, 1, 1, NULL, NULL, NULL, '2018-04-04 15:37:46', '2018-04-04 15:37:46'),
(182, 'Teste 6', '<p>Descri&ccedil;&atilde;o 6</p>', 3, 1, 1, 3, 1, 6, NULL, NULL, NULL, '2018-04-04 15:39:04', '2018-04-04 15:39:04'),
(183, 'Teste 7', '<p>Descri&ccedil;&atilde;o 7</p>', 3, 1, 1, 2, 1, 6, NULL, NULL, NULL, '2018-04-04 15:40:02', '2018-04-04 15:40:02'),
(184, 'Teste 8', '<p>Descri&ccedil;&atilde;o 8</p>', 3, 1, 1, 2, 1, 6, NULL, NULL, NULL, '2018-04-04 15:45:29', '2018-04-04 15:45:29'),
(185, 'Pedido teste elsa', '<p>Descri&ccedil;&atilde;owawdawda</p>', 3, 1, 1, 3, 1, 1, NULL, NULL, NULL, '2018-04-04 16:14:20', '2018-04-20 14:17:28'),
(186, 'Teste Pedido na fase final 2', '<p>Descrição do pedido na fase final</p>', 2, 1, 1, 2, 1, 1, NULL, NULL, NULL, '2018-04-23 14:13:38', '2018-04-27 08:41:19'),
(187, 'TEste ao erro', 'Descrição', 2, 1, 1, 2, 1, 1, NULL, NULL, NULL, '2018-04-27 09:22:16', '2018-04-27 09:22:16'),
(188, 'Pedidos na data de hoje', '<p>Descri&ccedil;&atilde;o de hoje</p>', 2, 1, 1, 9, 1, 1, NULL, NULL, NULL, '2018-05-11 15:22:45', '2018-05-11 15:22:45');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ticket_files`
--

DROP TABLE IF EXISTS `ticket_files`;
CREATE TABLE IF NOT EXISTS `ticket_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `size` int(11) NOT NULL,
  `url` text NOT NULL,
  `ext` text NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ticket_files_ticket_id_idx` (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ticket_files`
--

INSERT INTO `ticket_files` (`id`, `name`, `size`, `url`, `ext`, `ticket_id`, `created_at`, `updated_at`) VALUES
(46, 'Portal  Nutricionista_alteração.docx', 167103, 'public/upload/Portal  Nutricionista_alteração.docx', 'docx', 14, '2018-05-04 14:28:03', '2018-05-04 14:28:03'),
(48, 'd3p-logo_v2.png', 15128, 'public/upload/d3p-logo_v2.png', 'png', 15, '2018-05-07 08:06:46', '2018-05-07 08:06:46'),
(53, 'Desert-300x188.jpg', 9389, 'public/upload/Desert-300x188.jpg', 'jpg', 15, '2018-05-08 10:58:00', '2018-05-08 10:58:00'),
(55, 'tickets.csv', 44387, 'public/upload/tickets.csv', 'csv', 15, '2018-05-08 11:00:26', '2018-05-08 11:00:26');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` char(128) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `department_id` int(11) NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `users_role_id_idx` (`role_id`),
  KEY `users_department_id_idx` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `role_id`, `status`, `department_id`, `image_url`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Filipe Covas', 'filipecovas@farmodietica.com', 'filipecovas', '$2y$10$tHQMj2dR.xCEBxAJgUt9r.KJU1HpTlFjEA6hwNqrwD3Aho13xtFP.', 1, 1, 1, NULL, NULL, NULL, '2018-05-09 12:14:08'),
(2, 'Nuno Lopes', 'nunolopes@farmodietica.com', 'nunolopes', '$2y$10$Kl4W/EEg5iTdwfrNo3uXUOxYUUjAm58rJdoWC63svIPAtFuVMROiO', 1, 1, 1, NULL, NULL, NULL, '2018-04-24 15:15:37'),
(3, 'Elsa Costa', 'elsacosta@farmodietica.com', 'elsacosta', '65371b344a53db63ac189540cf43f35bb580a4802b1e7ad88acc1e62348ea468cde14386de25c73c595ace0331a1342b58849cc7699d3eb87e9a63e9c951f474', 1, 1, 1, NULL, NULL, NULL, NULL),
(4, 'Colaborador', 'colaborador@farmodietica.com', 'colaborador', '8720e382291b8c6f6226e6c6f1c3e94df0738532ba19939c5483081d658c88860f44dab0a85dc4a5ac2ba73ea53c869ef8992fd11e90f6de2d54f57c8d78707c', 3, 1, 1, NULL, NULL, NULL, NULL),
(5, 'Filipe Costa', 'filipecosta@farmodietica.com', 'filipecosta', 'd498c40aaf2b249dba040cd8e9e53e4a3d986af209e5c4960e558eef36c2a9c81e09ec966cfca4daa8d987a759036444b62c566a5a2df14699a559b901cbd8b0', 1, 1, 1, NULL, NULL, NULL, NULL),
(6, 'David Miguel', 'davidmiguel@farmodietica.com', 'davidmiguel', '4cc25b37639237b8a3dc2ecf8074c68a30d07d57735263ddc56aeb82c2e7a454c7d427ea5bb57963fb09895ae880e55db4d7d477c386f50ee807baf374212dcb', 1, 1, 1, NULL, NULL, NULL, NULL),
(7, 'Ricardo Nunes', 'ricardonunes@farmodietica.com', 'ricardonunes', '65371b344a53db63ac189540cf43f35bb580a4802b1e7ad88acc1e62348ea468cde14386de25c73c595ace0331a1342b58849cc7699d3eb87e9a63e9c951f474', 1, 1, 1, NULL, NULL, NULL, NULL),
(8, 'Francisca Menezes', 'franciscamenezes@farmodietica.com', 'franciscamenezes', '$2y$10$Kl4W/EEg5iTdwfrNo3uXUOxYUUjAm58rJdoWC63svIPAtFuVMROiO', 2, 1, 11, NULL, NULL, NULL, NULL),
(9, 'Hugo Teixeira', 'hugoteixeira@farmodietica.com', 'hugoteixeira', '2b812652c4514cb45ec56de2b70302ffa6278e41a3aebfec123bee87393ebe63e35ebb574d5bc74c1ec0c3b6f42c12360e21f0aaf7b9014db0b5653796495e0e', 2, 1, 11, NULL, NULL, NULL, NULL),
(10, 'Nuno Caldas', 'nunocaldas@farmodietica.com', 'nunocaldas', '0cea6edb52f2367f0dad677fc23c633ad6678b26026232009502b94e246787fd9edcc6ac77ed80d9c01ec82662baf4692b5045f22f7355ba046ca5d1fa7f329e', 3, 1, 5, NULL, NULL, NULL, NULL),
(11, 'Gabriela Santos', 'gabrielasantos@farmodietica.com', 'gabrielasantos', '128b136f45e396bf550f2c55a04ad2b068c9f7d1a2a5e03878eadf5735fad82d21e44d226f552bcfa40ee12b3ae0242d0cf840e272b6ce458ea52689bb9723fe', 3, 1, 8, NULL, NULL, NULL, NULL),
(12, 'Madalena Vidal', 'madalenavidal@farmodietica.com', 'madalenavidal', '2bf9fdbdcd05a93ae0d0920605560ac9c65b778dfd095ff2823edc6c7d6585427e1e951cea320ad4e2d5e804b5b0f99e0d43bc0f0b20c61b39b51303f1a3b9cc', 3, 1, 21, NULL, NULL, NULL, NULL),
(13, 'Ricardo Rodrigues', 'ricardorodrigues@farmodietica.com', 'ricardorodrigues', '3ad15885e11f01539e364d58c6f593162c51e88ecc84c47fc297aa60ecb1c7b7791e239a1467281db677c14d32b3492d98584e1348bbfd1e2dce43b9cc1243a0', 3, 1, 15, NULL, NULL, NULL, NULL),
(14, 'Eder Cabral', 'edercabral@farmodietica.com', 'edercabral', '8b00dde28d14ed9904fdfffc90e0f23f4fc5a47982cdb3af19af5de26c82ee982cbdf8873e930bde9693906c1137f2d21b08e3fbb926b01ca2461255d7068f4d', 3, 1, 15, NULL, NULL, NULL, NULL),
(15, 'Pedro Teixeira', 'pedroteixeira@farmodietica.com', 'pedroteixeira', '039ccc6ff06e0d6a7972d203cabad437e92c14650e1493a7554678cf2d9452767202e78b57c1fc1521f5b357ced42fe6143b425ac07e6db256aa3b2b9d0af395', 2, 1, 11, NULL, NULL, NULL, NULL),
(16, 'Sonia Carvalho', 'soniacarvalho@farmodietica.com', 'soniacarvalho', 'a907a58faaf1295de78c71abfc1465050820395c90400c9f78ac98a0e84e67b12df142663131203edd7e29bd74406bd4970ca6086c232ecb1ba4ae4fdedd5f1d', 3, 1, 4, NULL, NULL, NULL, NULL),
(17, 'Márcia Reis', 'marciareis@farmodietica.com', 'marciareis', '6f21a247f809b8a8298b06bcb50ea28054b3f68e22e21ec33b102e03a33b44fa44fd0baadea726a9b511994c7095ba1f4e367a2922237b5510f172b83016ce4e', 3, 1, 18, NULL, NULL, NULL, NULL),
(18, 'Mariana Vieira', 'marianavieira@farmodietica.com', 'marianavieira', '41c05698eff8b1f859aeb163e0e26430eeb5033b64c19f8118af7da3f18a3a48255870073030087bfe6bc87d4e941a1606b14299f2d33800d9a84298953d1472', 3, 1, 8, NULL, NULL, NULL, NULL),
(19, 'Sofia Cristóvão', 'sofiacristovao@farmodietica.com', 'sofia cristovao', '9f569fa3312b4ad70e51a9b696e743facb63deb1eefc45f398016d9820db88ce6708726f952bbd1c50e7c1e69e40cdfb9699e53044078c9d7d630bdc98f7fe29', 3, 1, 17, NULL, NULL, NULL, NULL),
(20, 'Patrícia Cruz', 'patriciacruz@farmodietica.com', 'patriciacruz', 'e3c9fc3daa1aa3bec4922a37b4a107757683d947162f0dd3a753921d8355f937d30ae1a9410c6fe96793302b8ce0bc6ed34e50e1a2ceb0b12f4414fff6578023', 3, 1, 4, NULL, NULL, NULL, NULL),
(21, 'Nuno Carvalho', 'nunocarvalho@farmodietica.com', 'nunocarvalho', '1039fd9f8833bc92f106bc1e580be9abbac459c7a9943c6145ae5e93ae33e5f15ac31a1f1b0d8db1bad181b2d161e7f210ad4fae9f8d3928ac636b5cd261cd04', 3, 1, 4, NULL, NULL, NULL, NULL),
(22, 'Bruno Senna', 'brunosena@farmodietica.com', 'brunosena', 'ef552bd5adddc76c6a4bbd201a9681a2424f6ee3f1d48418a6867e92b81b87a6e98385632645f45e1d111657f4dc7b6499f96fe68807b72ef7247dcbf7a91598', 3, 1, 16, NULL, NULL, NULL, NULL),
(23, 'Elisabete Silva', 'elisabetesilva@farmodietica.com', 'elisabetesilva', '75dc8e11ffa0b1e965d832f6b35663a7ac3d3eeec6ca2ee1dbe5c43edc32a984fae50a461b19fe2b94d6a97080910d690df3dc1e108c3dc050a63645d49cfedd', 3, 1, 17, NULL, NULL, NULL, NULL),
(24, 'Alexandra Pedro', 'alexandrapedro@farmodietica.com', 'alexandrapedro', '2b4e99ab1f15c04533fa865dac42d66ed79b9d83325fa0d7c960e284743f8fec3b1bba963c197e269b529563119e842907e5fe531b6326620f57f37aaf243c20', 3, 1, 16, NULL, NULL, NULL, NULL),
(25, 'Inês Perestrelo', 'inesperestrelo@farmodietica.com', 'inesperestrelo', 'f808eee50e62521d5b2cc1803678842908798439c1df14827119f67b3bc2bd326517b5b1b5dc668ca29805b56c019f53a51e7d9e110b180deaa860978a80fe17', 3, 1, 15, NULL, NULL, NULL, NULL),
(26, 'Filipa Barreto', 'filipabarreto@farmodietica.com', 'filipabarreto', 'b7957c8e19a6ffa1d0507706d65757dd763dc06fa552b0dea0236c24b0b5da887b06aeac864891b3c540ffcd8f3060aa13a8e4696e5fdad6ba8e22243fb1b87a', 3, 1, 7, NULL, NULL, NULL, NULL),
(27, 'Ricardo Mecha', 'ricardomecha@farmodietica.com', 'ricardomecha', 'f5f3689328b60c00282b88ffe91edc8a2bd2e1f75a2793e62c33c0de693cfb1396bd6ed97f4eb0518f4cf26101adaf4a97e1d5961114b7e0d24498983fe7c6a0', 3, 1, 12, NULL, NULL, NULL, NULL),
(28, 'Pedro Baldo', 'pedrobaldo@farmodietica.com', 'pedrobaldo', '8ccb1b06a37e019c96081a689e5e201fb532f05d948d21af340936bea2fdf64eba57ad885cacca1f19f26a7627af0aa66a34d4f54abac850ce73213a11ba1309', 3, 1, 17, NULL, NULL, NULL, NULL),
(41, 'Filipe Costa', 'filipecosta11@farmodietica.com', 'filipecosta', '$2y$10$O054y029yDJzAmdnxrLT2e1LxC7GcKsra6m1hyyPbw/EmlIT5.VMO', 1, 1, 4, NULL, NULL, '2018-03-28 15:12:00', '2018-03-28 15:12:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_projects`
--

DROP TABLE IF EXISTS `user_projects`;
CREATE TABLE IF NOT EXISTS `user_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_projects_users_idx` (`user_id`),
  KEY `user_projects_projects_idx` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `user_projects`
--

INSERT INTO `user_projects` (`id`, `user_id`, `project_id`, `created_at`, `updated_at`) VALUES
(9, 5, 1, NULL, NULL),
(15, 7, 1, NULL, NULL),
(16, 3, 1, NULL, NULL),
(19, 1, 3, NULL, NULL),
(21, 1, 5, NULL, NULL),
(22, 5, 5, NULL, NULL),
(25, 1, 4, NULL, NULL),
(26, 3, 4, NULL, NULL),
(27, 1, 2, NULL, NULL),
(29, 5, 8, NULL, NULL),
(30, 2, 9, NULL, NULL),
(31, 2, 10, NULL, NULL),
(33, 6, 12, NULL, NULL),
(34, 5, 14, NULL, NULL),
(36, 5, 15, NULL, NULL),
(38, 2, 17, NULL, NULL),
(39, 2, 18, NULL, NULL),
(40, 2, 19, NULL, NULL),
(42, 2, 22, NULL, NULL),
(44, 2, 23, NULL, NULL),
(46, 2, 21, NULL, NULL),
(47, 5, 13, NULL, NULL),
(48, 2, 16, NULL, NULL),
(49, 3, 16, NULL, NULL),
(50, 6, 16, NULL, NULL),
(51, 1, 6, NULL, NULL),
(52, 6, 6, NULL, NULL),
(53, 2, 7, NULL, NULL),
(54, 6, 7, NULL, NULL),
(55, 2, 1, NULL, NULL),
(56, 2, 20, NULL, NULL),
(57, 6, 20, NULL, NULL),
(58, 2, 24, NULL, NULL),
(59, 6, 24, NULL, NULL),
(60, 7, 25, NULL, NULL),
(67, 2, 11, NULL, NULL),
(68, 6, 11, NULL, NULL),
(69, 1, 27, NULL, NULL),
(70, 2, 27, NULL, NULL),
(71, 3, 27, NULL, NULL),
(72, 5, 27, NULL, NULL),
(73, 6, 27, NULL, NULL),
(74, 7, 27, NULL, NULL),
(75, 6, 1, '2018-05-07 14:28:33', '2018-05-07 14:28:33'),
(76, 3, 2, '2018-05-07 14:30:53', '2018-05-07 14:30:53'),
(78, 7, 2, '2018-05-07 14:46:23', '2018-05-07 14:46:23'),
(79, 5, 2, '2018-05-07 14:46:36', '2018-05-07 14:46:36'),
(80, 4, 4, '2018-05-08 09:34:03', '2018-05-08 09:34:03'),
(84, 6, 3, '2018-05-08 10:41:45', '2018-05-08 10:41:45'),
(85, 2, 3, '2018-05-11 09:08:48', '2018-05-11 09:08:48'),
(86, 3, 5, '2018-05-11 09:10:38', '2018-05-11 09:10:38');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_tickets`
--

DROP TABLE IF EXISTS `user_tickets`;
CREATE TABLE IF NOT EXISTS `user_tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_tickets_users_idx` (`user_id`),
  KEY `user_tickets_tickets_idx` (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `events_ticket_id` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `events_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD CONSTRAINT `login_attempts_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `project_files`
--
ALTER TABLE `project_files`
  ADD CONSTRAINT `project_files_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `recoveries`
--
ALTER TABLE `recoveries`
  ADD CONSTRAINT `passwords_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_projects` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tickets_user_solicitation` FOREIGN KEY (`user_solicitation_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tickets_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tickets_workers` FOREIGN KEY (`worker_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `ticket_files`
--
ALTER TABLE `ticket_files`
  ADD CONSTRAINT `ticket_files_ticket_id` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_department_id` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `users_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `user_projects`
--
ALTER TABLE `user_projects`
  ADD CONSTRAINT `user_projects_projects` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_projects_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `user_tickets`
--
ALTER TABLE `user_tickets`
  ADD CONSTRAINT `user_tickets_tickets` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_tickets_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
