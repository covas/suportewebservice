<?php
use Illuminate\Http\Request;
use App\Ticket;
use App\User;
use App\Department;
use App\Project;
use App\Priority;
use App\Status;
use App\Event;
use App\UserProject;
use App\TicketFile;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\File;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


