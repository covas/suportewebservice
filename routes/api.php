<?php

use Illuminate\Http\Request;
use App\Ticket;
use App\User;
use App\Department;
use App\Project;
use App\Priority;
use App\Status;
use App\Event;
use App\UserProject;
use App\TicketFile;
use App\NoteTicket;
use App\PreTicket;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\File;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    /* Mail::send( 'mail', ['titulo' => 'Teste pedido', 'numero_pedido' => '1212', 'descricao' => 'teste descrição'], function($message){
        $message->to('fcovas10@hotmail.com', 'Filipe Covas')->from('suporte@farmodietica.com', 'Suporte IT')->subject('Welcome!');
        return ['Email enviado com sucesso!'];
    }); */
}); 



// ACTIVIDADES/EVENTS

//ACTIVIDADES/EVENTS - LIST

Route::middleware('auth:api')->get('/actividades/{id}', function (Request $request, $id) {

    $id = $id;
    $events = Event::All()->where('user_id', $id);;
   


    $returnevents = [];



    foreach($events as $event){

        $startdate = new DateTime($event->start, new DateTimeZone("UTC"));
        $startdate = $startdate->format('Y-m-d\TH:i:s');
        $enddate = new DateTime($event->end, new DateTimeZone("UTC"));
        $enddate = $enddate->format('Y-m-d\TH:i:s');
       // $startdate = $startdate->date;

        array_push($returnevents, [
            'id' => $event->id ,
            'title' => $event->title, 
            'start' => $startdate, 
            'end' => $enddate
        ]);
    }

    

    return response($returnevents);
});

//ACTIVIDADES/EVENTS - SHOW

Route::middleware('auth:api')->get('/actividade/{id}', function (Request $request, $id) {
    $id = $id;
    $event = Event::findOrFail($id);


    $event->user_name = $event->user_obj['name'];

    $event->project_name = $event->project_obj['name'];

    $event->ticket_name = $event->ticket_obj['name'];

    $event->ticket_description = $event->ticket_obj['description'];

    $event->ticket_status = $event->ticket_obj->status_obj['name'];

    return $event;
});


//ACTIVIDADES/EVENTS - UPDATE

Route::middleware('auth:api')->put('actividade/{id}', function(Request $request, $id) {

    $data = $request->all();
    $id = $id;

    //SISTEMA DE VALIDAÇÃO

    /* $validacao = Validator::make($request->all(), [
        'title' => 'string',
        'start' => 'datetime',
        'end' => 'datetime',
        'user_id' => 'number',
        'project_id' => 'number',
        'ticket_id' => 'number'
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    } */


    $event = Event::findOrFail($id);
    $event->update($data);

    return $event;
});


//ACTIVIDADES/EVENTS - POST/CREATE
Route::middleware('auth:api')->post('actividade/{concluirPedido}', function(Request $request, $concluirPedido) {

    $concluirPedido = $concluirPedido;

    $data = $request->all();

    if($concluirPedido == 1){
        $ticket = Ticket::findOrFail($data['ticket_id']);
        
        if($ticket->status_id !== 3){
            $ticket->status_id = 3;
            $ticket->save();

            //ENVIAR EMAIL SE CONCLUIDO

            $notas = NoteTicket::All()->where('ticket_id', $ticket->id)->whereNotIn('public', 0);

            foreach($notas as $nota){
                $nota->user_name = $nota->user_obj['name'];
            }

            if($ticket->status_id === 3){
                Mail::send( 'mail', ['notas' => $notas, 'worker_name' => $ticket->worker_obj['name'], 'worker_imgurl' => $ticket->worker_obj['image_url'], 'solicitation_email' => $ticket->solicitation_obj['email'], 'solicitation_name' => $ticket->solicitation_obj['name'], 'titulo' => $ticket->name, 'numero_pedido' => $ticket->id, 'descricao' => html_entity_decode(strip_tags($ticket->description))], function($message){
                    $message->from('suporte@farmodietica.com', 'Suporte IT');
                });
            }
        }
        
        
    } else{
        $ticket = Ticket::findOrFail($data['ticket_id']);

        if($ticket->status_id == 1){
            $ticket->status_id = 2;
            $ticket->save();
        }
        
    }

    


/*     $data->start = date_format($data->start, 'Y-m-d');
    $data->prevend = date_format($data->prevend, 'Y-m-d'); */

    //SISTEMA DE VALIDAÇÃO

    /* $validacao = Validator::make($request->all(), [
        'title' => 'required',
        'start' => 'required',
        'end' => 'required',
        'user_id' => 'required',
        'project_id' => 'required',
        'ticket_id' => 'required'
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    } */

    $event = Event::create($data);

    return ["Actividade criada com sucesso!"];
});


//ACTIVIDADES/EVENTS - DELETE
Route::middleware('auth:api')->delete('deleteactividade/{id}', function(Request $request, $id) {
    $id = $id;
    $event = Event::findOrFail($id);
    $event->delete();
});





//PROJECTOS


//MEUS PROJECTOS

Route::middleware('auth:api')->get('/meusprojectos/{user_id}', function (Request $request, $user_id) {

    $user_id = $user_id;

    $userprojects = UserProject::All()->where('user_id', $user_id)->whereNotIn('status_id', 3)->whereNotIn('status_id', 4);



    foreach($userprojects as $userproject){

        $userproject->name = $userproject->project_obj['name'];
        $userproject->description = $userproject->project_obj['description'];
        $userproject->evolution = $userproject->project_obj['evolution'];

    } 

    $returnuserprojects = [];

    foreach($userprojects as $userproject){
        array_push($returnuserprojects, $userproject);
    }



    return response($returnuserprojects)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 




//PROJECTOS - LIST EXPORT

Route::middleware('auth:api')->get('/projectosexport', function () {
    $projects = Project::All()->whereNotIn('id', 27)->whereNotIn('status_id', 3)->whereNotIn('status_id', 4);

    $returnprojects = [];

    foreach($projects as $project){

        if($project->id != 27){
        
            $user_projects = UserProject::All()->where('project_id', $project->id);
            $workers = '';
            foreach($user_projects as $user_project){

                if($workers == ''){
                    $workers = $user_project->user_obj->name;
                } else{
                    $workers = $workers . ', ' . $user_project->user_obj->name;
                }

                

            }
            
            
            $project->status_name = $project->status_obj['name'];

            $start = new DateTime($project->start, new DateTimeZone("UTC"));
            $start = $start->format('d-m-Y');
            $prevend = new DateTime($project->prevend, new DateTimeZone("UTC"));
            $prevend = $prevend->format('d-m-Y');


            $new_project = [
                'id' => $project->id,
                'name' => $project->name,
                'description' => html_entity_decode(strip_tags($project->description)),
                'created_at' => date_format($project->created_at, 'd-m-Y'),
                'prevend' => $prevend,
                'workers' => $workers
            ];

            array_push($returnprojects, $new_project);
        }
        


    }



    return response($returnprojects)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 




//PROJECTO - LIST

Route::middleware('auth:api')->get('/arquivoprojectos', function (Request $request) {
    $projects = Project::whereIn('status_id', [3, 4])->get()->whereNotIn('id', 27);

    $returnprojects = [];

    foreach($projects as $project){

        if($project->id != 27){
        
            $user_projects = UserProject::All()->where('project_id', $project->id);
            $worker = [];
            foreach($user_projects as $user_project){

                array_push( $worker, $user_project->user_obj);

            }
            
            
            $project->status_name = $project->status_obj['name'];

            $project->workers = $worker;

            array_push($returnprojects, $project);
        }
        


    }

    

    return response($returnprojects)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');
});



//PROJECTO - LIST

Route::middleware('auth:api')->get('/projectos', function (Request $request) {
    $projects = Project::All()->whereNotIn('id', 27)->whereNotIn('status_id', 3)->whereNotIn('status_id', 4);

    $returnprojects = [];

    foreach($projects as $project){

        if($project->id != 27){
        
            $user_projects = UserProject::All()->where('project_id', $project->id);
            $worker = [];
            foreach($user_projects as $user_project){

                array_push( $worker, $user_project->user_obj);

            }
            
            
            $project->status_name = $project->status_obj['name'];

            $project->workers = $worker;

            array_push($returnprojects, $project);
        }
        


    }

    

    return response($returnprojects)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');
});

//PROJECTO - SHOW

Route::middleware('auth:api')->get('/projecto/{id}', function (Request $request, $id) {
    $id = $id;
    $project = Project::findOrFail($id);
    return $project;
});


//PROJECTO - UPDATE

Route::middleware('auth:api')->put('projecto/{id}', function(Request $request, $id) {

    $data = $request->all();
    $id = $id;

    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'name' => 'required',
        'description' => 'required',
        'time' => 'required',
        'status_id' => 'required',
        'evolution' => 'required'
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    } 


    $project = Project::findOrFail($id);
    $project->update($data);

    return $project;
});


//PROJECTO - POST/CREATE
Route::middleware('auth:api')->post('projecto', function(Request $request) {

    $data = $request->all();


/*     $data->start = date_format($data->start, 'Y-m-d');
    $data->prevend = date_format($data->prevend, 'Y-m-d'); */

    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'name' => 'string|required',
        'description' => 'required',
        'time' => 'required',
        'status_id' => 'required',
        'evolution' => 'required'
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    }

    $project = Project::create($data);

    return ["Projecto criado com sucesso!"];
});



//PROJECTO - ADICIONAR RESPONSÁVEIS
Route::middleware('auth:api')->post('addworker', function(Request $request) {

    $data = $request->all();


/*     $data->start = date_format($data->start, 'Y-m-d');
    $data->prevend = date_format($data->prevend, 'Y-m-d'); */

    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'project_id' => 'required',
        'user_id' => 'required'
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    }

    $worker = UserProject::create($data);
});

//PROJECTO - DELETE RESPONSÁVEL

Route::middleware('auth:api')->delete('deleteworker/{id}', function(Request $request, $id) {
    $id = $id;
    $project = UserProject::findOrFail($id);
    $project->delete();
});



//PROJECTO - DELETE

Route::middleware('auth:api')->delete('projecto/{id}', function(Request $request, $id) {
    $id = $id;

    $project = Project::findOrFail($id);
    $project->delete();

    return ["Projecto eliminado com Sucesso!"];
});




//PRIORIDADE


//PRIORIDADE - LIST

Route::middleware('auth:api')->get('/prioridades', function (Request $request) {
    $prioritys = Priority::All();
    return $prioritys;
});

//PRIORIDADE - SHOW

Route::middleware('auth:api')->get('/prioridade/{id}', function (Request $request, $id) {
    $id = $id;
    $priority = Priority::findOrFail($id);
    return $priority;
});




//ESTADO


//ESTADO - LIST

Route::middleware('auth:api')->get('/estados', function (Request $request) {
    $status = Status::All();
    return $status;
});

//ESTADO - SHOW

Route::middleware('auth:api')->get('/estado/{id}', function (Request $request, $id) {
    $id = $id;
    $statu = Status::findOrFail($id);
    return $statu;
});






//PEDIDOS


//NOTAS DE PEDIDOS



//SHOW NOTAS PEDIDO
Route::middleware('auth:api')->get('/notaspedido/{pedido_id}', function (Request $request, $pedido_id) {

    $pedido_id = $pedido_id;
    $notas = NoteTicket::All()->where('ticket_id', $pedido_id);

    $notasreturn = [];

    foreach($notas as $nota){

        $nota->user_name = $nota->user_obj['name'];

        array_push($notasreturn, $nota);
    }



    return response($notasreturn)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 


//CREATE NOTA PEDIDO
Route::middleware('auth:api')->post('/addnotapedido', function (Request $request) {

    $data = $request->all();


/*     $data->start = date_format($data->start, 'Y-m-d');
    $data->prevend = date_format($data->prevend, 'Y-m-d'); */

    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'ticket_id' => 'required',
        'user_id' => 'required',
        'note' => 'required',
        'public' => 'required'
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    }

    $nota = NoteTicket::create($data);

    return ["Nota criada com sucesso!"];
});

//NOTA PEDIDO - DELETE
Route::middleware('auth:api')->delete('deletenotapedido/{id}', function(Request $request, $id) {
    $id = $id;
    $nota = NoteTicket::findOrFail($id);
    $nota->delete();
});


//MEUS PEDIDOS

Route::middleware('auth:api')->get('/meuspedidos/{user_id}', function (Request $request, $user_id) {

    $user_id = $user_id;

    $usertickets = Ticket::All()->where('worker_id', $user_id)->whereNotIn('project_id', 27)->whereNotIn('status_id', 3)->whereNotIn('status_id', 4);



    foreach($usertickets as $userticket){

        $userticket->user_name = $userticket->user_obj['name'];
        $userticket->solicitation_name = $userticket->solicitation_obj['name'];
        $userticket->worker_name = $userticket->worker_obj['name'];
        $userticket->project_name = $userticket->project_obj['name'];
        $userticket->priority_name = $userticket->priority_obj['name'];
        $userticket->priority_icon = $userticket->priority_obj['icon'];
        $userticket->priority_color = $userticket->priority_obj['color'];
        $userticket->status_name = $userticket->status_obj['name'];

    } 

    $returnusertickets = [];

    foreach($usertickets as $userticket){
        array_push($returnusertickets, $userticket);
    }



    return response($returnusertickets)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 



//MEUS PEDIDOS (SOLICITADOR)

Route::middleware('auth:api')->get('/meuspedidossolicitador/{user_id}', function (Request $request, $user_id) {

    $user_id = $user_id;

    $usertickets = Ticket::All()->where('user_solicitation_id', $user_id)->whereNotIn('project_id', 27)->whereNotIn('status_id', 3)->whereNotIn('status_id', 4);



    foreach($usertickets as $userticket){

        $userticket->user_name = $userticket->user_obj['name'];
        $userticket->solicitation_name = $userticket->solicitation_obj['name'];
        $userticket->worker_name = $userticket->worker_obj['name'];
        $userticket->project_name = $userticket->project_obj['name'];
        $userticket->priority_name = $userticket->priority_obj['name'];
        $userticket->priority_icon = $userticket->priority_obj['icon'];
        $userticket->priority_color = $userticket->priority_obj['color'];
        $userticket->status_name = $userticket->status_obj['name'];

    } 

    $returnusertickets = [];

    foreach($usertickets as $userticket){
        array_push($returnusertickets, $userticket);
    }



    return response($returnusertickets)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 



//PEDIDOS - AQUIVO LIST

Route::middleware('auth:api')->get('/arquivopedidos', function () {
    $tickets = Ticket::All()->where('status_id', '>=', 3)->whereNotIn('project_id', 27);

    foreach($tickets as $ticket){

        $ticket->user_name = $ticket->user_obj['name'];
        $ticket->solicitation_name = $ticket->solicitation_obj['name'];
        $ticket->worker_name = $ticket->worker_obj['name'];
        $ticket->project_name = $ticket->project_obj['name'];
        $ticket->priority_name = $ticket->priority_obj['name'];
        $ticket->priority_icon = $ticket->priority_obj['icon'];
        $ticket->status_name = $ticket->status_obj['name'];
        $ticket->status_icon = $ticket->status_obj['icon'];

    } 

    $returntickets = [];

    foreach($tickets as $ticket){
        array_push($returntickets, $ticket);
    }



    return response($returntickets)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 



//PEDIDOS DO PROJECTO - LIST

Route::middleware('auth:api')->get('/pedidosdoprojecto/{project_id}', function (Request $request, $project_id) {
    $project_id = $project_id;

    $tickets = Ticket::All()->where('project_id', $project_id)->whereNotIn('status_id', 3)->whereNotIn('status_id', 4);

    foreach($tickets as $ticket){

        $ticket->user_name = $ticket->user_obj['name'];
        $ticket->solicitation_name = $ticket->solicitation_obj['name'];
        $ticket->worker_name = $ticket->worker_obj['name'];
        $ticket->project_name = $ticket->project_obj['name'];
        $ticket->priority_name = $ticket->priority_obj['name'];
        $ticket->priority_icon = $ticket->priority_obj['icon'];
        $ticket->status_name = $ticket->status_obj['name'];
        $ticket->status_icon = $ticket->status_obj['icon'];

    } 

    $returntickets = [];

    foreach($tickets as $ticket){
        array_push($returntickets, $ticket);
    }



    return response($returntickets)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 





//PEDIDOS - LIST EXPORT

Route::middleware('auth:api')->get('/pedidosexport', function () {
    $tickets = Ticket::All()->whereNotIn('project_id', 27)->whereNotIn('status_id', 3)->whereNotIn('status_id', 4);

    foreach($tickets as $ticket){

        $ticket->user_name = $ticket->user_obj['name'];
        $ticket->solicitation_name = $ticket->solicitation_obj['name'];
        $ticket->worker_name = $ticket->worker_obj['name'];
        $ticket->project_name = $ticket->project_obj['name'];
        $ticket->priority_name = $ticket->priority_obj['name'];
        $ticket->priority_icon = $ticket->priority_obj['icon'];
        $ticket->status_name = $ticket->status_obj['name'];
        $ticket->status_icon = $ticket->status_obj['icon'];

    } 

    $returntickets = [];

    foreach($tickets as $ticket){

        $start = new DateTime($ticket->start, new DateTimeZone("UTC"));
        $start = $start->format('d-m-Y');
        $prevend = new DateTime($ticket->prevend, new DateTimeZone("UTC"));
        $prevend = $prevend->format('d-m-Y');

        $solicitator_id = $ticket->user_solicitation_id;

        $solicitator = User::findOrFail($solicitator_id);

        $department = $solicitator->department_obj['name'];

        $new_ticket = [
            'id' => $ticket->id,
            'name' => $ticket->name,
            'created_at' => date_format($ticket->created_at, 'd-m-Y'),
            'project' => $ticket->project_name,
            'description' => html_entity_decode(strip_tags($ticket->description)),
            'priority' => $ticket->priority_name,
            'status' => $ticket->status_name,
            'worker' => $ticket->worker_name,
            'department' => $department,
            'user_solicitation' => $ticket->solicitation_name,
            'start' => $start,
            'prevend' => $prevend,
        ];
        array_push($returntickets, $new_ticket);
    }



    return response($returntickets)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 



//PEDIDOS - LIST

Route::middleware('auth:api')->get('/pedidos', function () {
    $tickets = Ticket::All()->whereNotIn('project_id', 27)->whereNotIn('status_id', 3)->whereNotIn('status_id', 4);

    foreach($tickets as $ticket){

        $ticket->user_name = $ticket->user_obj['name'];
        $ticket->solicitation_name = $ticket->solicitation_obj['name'];
        $ticket->worker_name = $ticket->worker_obj['name'];
        $ticket->project_name = $ticket->project_obj['name'];
        $ticket->priority_name = $ticket->priority_obj['name'];
        $ticket->priority_icon = $ticket->priority_obj['icon'];
        $ticket->status_name = $ticket->status_obj['name'];
        $ticket->status_icon = $ticket->status_obj['icon'];

    } 

    $returntickets = [];

    foreach($tickets as $ticket){
        array_push($returntickets, $ticket);
    }



    return response($returntickets)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 

//PEDIDOS - SHOW

Route::middleware('auth:api')->get('/pedido/{id}', function (Request $request, $id) {

    $id = $id;
    $ticket = Ticket::findOrFail($id);



    $ticket->user_name = $ticket->user_obj['name'];
    $ticket->solicitation_name = $ticket->solicitation_obj['name'];
    $ticket->worker_name = $ticket->worker_obj['name'];
    $ticket->project_name = $ticket->project_obj['name'];
    $ticket->priority_name = $ticket->priority_obj['name'];
    $ticket->priority_icon = $ticket->priority_obj['icon'];
    $ticket->status_name = $ticket->status_obj['name'];
    $ticket->status_icon = $ticket->status_obj['icon'];

    
    return response($ticket)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 



//PEDIDOS DO PROJECTO - SHOW

Route::middleware('auth:api')->get('/pedidosdoprojecto/{project_id}/{user_id}', function (Request $request, $project_id, $worker_id) {

    $project_id = $project_id;
    $worker_id = $worker_id;
    $tickets = Ticket::All()->where('project_id', $project_id)->where('worker_id', $worker_id)->whereNotIn('status_id', 3)->whereNotIn('status_id', 4);

    $returntickets = [];

    foreach($tickets as $ticket){
        array_push($returntickets, $ticket);
    }

    
    return response($returntickets)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 

//PEDIDO - PUT
Route::middleware('auth:api')->put('pedido/{id}', function(Request $request, $id) {

    $data = $request->all();
    $id = $id;

/*     $data->start = date_format($data->start, 'Y-m-d');
    $data->prevend = date_format($data->prevend, 'Y-m-d'); */

    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'name' => 'string|required',
        'description' => 'string|required',
        'user_solicitation_id' => 'required',
        'project_id' => 'required',
        'status_id' => 'required',
        'priority_id' => 'required',
        'worker_id' => 'required'
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    }

    $ticket = Ticket::findOrFail($id);


    if($ticket->status_id !== 3){
        $sendemail = true;
    } else{
        $sendemail = false;
    }

    $ticket->update($data);

    if( ($sendemail === true) && ($ticket->status_id === 3) ){

        //ENVIAR EMAIL SE CONCLUIDO

        $notas = NoteTicket::All()->where('ticket_id', $ticket->id)->whereNotIn('public', 0);

        foreach($notas as $nota){
            $nota->user_name = $nota->user_obj['name'];
        }


        Mail::send( 'mail', ['notas' => $notas, 'worker_name' => $ticket->worker_obj['name'], 'worker_imgurl' => $ticket->worker_obj['image_url'], 'solicitation_email' => $ticket->solicitation_obj['email'], 'solicitation_name' => $ticket->solicitation_obj['name'], 'titulo' => $ticket->name, 'numero_pedido' => $ticket->id, 'descricao' => html_entity_decode(strip_tags($ticket->description))], function($message){
            $message->from('suporte@farmodietica.com', 'Suporte IT');
        });

        
    }

    

    $ticket->user_name = $ticket->user_obj['name'];
    $ticket->solicitation_name = $ticket->solicitation_obj['name'];
    $ticket->worker_name = $ticket->worker_obj['name'];
    $ticket->project_name = $ticket->project_obj['name'];
    $ticket->priority_name = $ticket->priority_obj['name'];
    $ticket->priority_icon = $ticket->priority_obj['icon'];
    $ticket->status_name = $ticket->status_obj['name'];
    $ticket->status_icon = $ticket->status_obj['icon'];

    return $ticket;
});


//PEDIDO - POST
Route::middleware('auth:api')->post('pedido', function(Request $request) {

    $data = $request->all();


/*     $data->start = date_format($data->start, 'Y-m-d');
    $data->prevend = date_format($data->prevend, 'Y-m-d'); */

    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'name' => 'string|required',
        'description' => 'required',
        'user_solicitation_id' => 'required',
        'project_id' => 'required',
        'status_id' => 'required',
        'priority_id' => 'required',
        'worker_id' => 'required'
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    }

    $ticket = Ticket::create($data);

    return ["Pedido criado com sucesso!"];
});



//PEDIDO NO CHECK-IN - POST
Route::middleware('auth:api')->post('pedidocheckin/{prepedido_id}', function(Request $request, $prepedido_id) {

    $data = $request->all();


/*     $data->start = date_format($data->start, 'Y-m-d');
    $data->prevend = date_format($data->prevend, 'Y-m-d'); */

    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'name' => 'string|required',
        'description' => 'required',
        'user_solicitation_id' => 'required',
        'project_id' => 'required',
        'status_id' => 'required',
        'priority_id' => 'required',
        'worker_id' => 'required'
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    }

    $ticket = Ticket::create($data);

    $prepedido = PreTicket::findOrFail($prepedido_id);

    $prepedido->ticket_id = $ticket->id;

    $prepedido->save();


    return ["Pedido criado com sucesso!"];
});























//PRÉ-PEDIDOS - LIST

Route::middleware('auth:api')->get('/prepedidos/{user_solicitation_id}', function (Request $request, $user_solicitation_id) {
    $pretickets = PreTicket::All()->where('user_solicitation_id', $user_solicitation_id)->sortByDesc('created_at');

    foreach($pretickets as $preticket){
        $preticket->description = html_entity_decode($preticket->description);
        $preticket->solicitation_name = $preticket->solicitation_obj['name'];
        $preticket->priority_name = $preticket->priority_obj['name'];
        $preticket->priority_icon = $preticket->priority_obj['icon'];

    } 

    $returnpretickets = [];

    foreach($pretickets as $preticket){
        array_push($returnpretickets, $preticket);
    }



    return response($returnpretickets)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 





//PRÉ-PEDIDOS - LIST ALL

Route::middleware('auth:api')->get('/prepedidosall', function (Request $request) {
    $pretickets = PreTicket::All()->where('ticket_id', null)->sortByDesc('created_at');

    foreach($pretickets as $preticket){
        $preticket->description = html_entity_decode($preticket->description);
        $preticket->solicitation_name = $preticket->solicitation_obj['name'];
        $preticket->priority_name = $preticket->priority_obj['name'];
        $preticket->priority_icon = $preticket->priority_obj['icon'];
        $preticket->department_name = Department::find($preticket->solicitation_obj['department_id']);
        $preticket->department_name = $preticket->department_name->name;

    } 

    $returnpretickets = [];

    foreach($pretickets as $preticket){
        array_push($returnpretickets, $preticket);
    }



    return response($returnpretickets)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 



//PRÉ-PEDIDOS - LIST COUNT

Route::middleware('auth:api')->get('/prepedidoscount', function (Request $request) {
    $pretickets_count = PreTicket::All()->where('ticket_id', null)->count();


    return response($pretickets_count)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 



//PRÉ-PEDIDOS - DELETE
Route::middleware('auth:api')->delete('deleteprepedido/{id}', function(Request $request, $id) {
    $id = $id;
    $prepedido = PreTicket::findOrFail($id);
    $prepedido->delete();
});

//PRÉ-PEDIDOS - SHOW

/* Route::middleware('auth:api')->get('/pedido/{id}', function (Request $request, $id) {

    $id = $id;
    $ticket = Ticket::findOrFail($id);



    $ticket->user_name = $ticket->user_obj['name'];
    $ticket->solicitation_name = $ticket->solicitation_obj['name'];
    $ticket->worker_name = $ticket->worker_obj['name'];
    $ticket->project_name = $ticket->project_obj['name'];
    $ticket->priority_name = $ticket->priority_obj['name'];
    $ticket->priority_icon = $ticket->priority_obj['icon'];
    $ticket->status_name = $ticket->status_obj['name'];
    $ticket->status_icon = $ticket->status_obj['icon'];

    
    return response($ticket)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');


}); 
 */




//PRÉ-PEDIDOS - PUT
Route::middleware('auth:api')->put('prepedido/{id}', function(Request $request, $id) {

    $data = $request->all();
    $id = $id;

/*     $data->start = date_format($data->start, 'Y-m-d');
    $data->prevend = date_format($data->prevend, 'Y-m-d'); */

    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'name' => 'string|required',
        'description' => 'string|required',
        'user_solicitation_id' => 'required',
        'priority_id' => 'required',
        'ticket_id' => ''
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    }

    $preticket = PreTicket::findOrFail($id);

    $preticket->update($data);

    

    $preticket->solicitation_name = $preticket->solicitation_obj['name'];
    $preticket->priority_name = $preticket->priority_obj['name'];
    $preticket->priority_icon = $preticket->priority_obj['icon'];

    return $preticket;
});


//PRÉ-PEDIDOS - POST
Route::middleware('auth:api')->post('prepedido', function(Request $request) {

    $data = $request->all();


/*     $data->start = date_format($data->start, 'Y-m-d');
    $data->prevend = date_format($data->prevend, 'Y-m-d'); */

    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'name' => 'string|required',
        'description' => 'string|required',
        'user_solicitation_id' => 'required',
        'priority_id' => 'required',
        'ticket_id' => ''
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    }

    $preticket = PreTicket::create($data);

    return ["Pré-Pedido criado com sucesso!"];
});





















//DEPARTAMENTOS

//DEPARTAMENTOS - LIST
Route::get('/departments', function () {
    $departments = Department::All();

    return $departments;
});

//DEPARTAMENTO - UPDATE
Route::put('/departments/{id}', function(Request $request, $id) {
    $data = $request->all();
    $id = $id;

    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'name' => 'string|max:255'
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    }


    $departamento = Department::findOrFail($id);
    $departamento->update($data);

    return $departamento;
});


//UTILIZADORES



//UTILIZADORES (RESPONSÁVEIS) POR PROJECTO - LIST

Route::middleware('auth:api')->get('/responsaveis/{project_id}', function (Request $request, $project_id) {
    $project_id = $project_id;
    $workers = UserProject::All();

    $i = 0;

    foreach($workers as $worker){
        //Adiciona infos de tabelas relacionadas
        if($worker->project_id != $project_id){

            unset($workers[$i]);
            
        } else{
            $worker->user_name = $worker->user_obj['name'];
            $worker->project_name = $worker->project_obj['name'];
        }

        $i++;

    } 

    $returnworkers = [];

    foreach($workers as $worker){
        array_push($returnworkers, $worker);
    }


    return response($returnworkers)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');
});




//UTILIZADORES (RESPONSÁVEIS) - LIST

Route::middleware('auth:api')->get('/responsaveis', function (Request $request) {

    $workers = User::All()->where('department_id', 1);


    $returnworkers = [];

    foreach($workers as $worker){
        array_push($returnworkers, $worker);
    }


    return response($returnworkers)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');
});



//UTILIZADORES - LIST

Route::middleware('auth:api')->get('/utilizadores', function (Request $request) {
    $users = User::All();

    foreach($users as $user){
        $user->department_name = $user->department_obj['name'];
    }

    

    //$worker->user_name = $worker->user_obj['name'];

    return response($users)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');
    //return 'yes';
});

//UTILIZADOR - SHOW

Route::middleware('auth:api')->get('/utilizador/{id}', function (Request $request, $id) {
    $id = $id;
    $user = User::findOrFail($id);

    $user->token = $user->createToken($user->email)->accessToken;
    $user->department_name = $user->department_obj['name'];
    $user->role_name = $user->role_obj['role'];
    
    return response($user)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');
});


//UTILIZADORES - UPDATE

Route::middleware('auth:api')->put('utilizador/{id}', function(Request $request, $id) {

    $data = $request->all();
    $id = $id;

    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'name' => 'string|max:255|required',
        'password' => ''
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    }


    $user = User::findOrFail($id);


    
    $user->name = $request->name;

    if(($request->password !== null)){

        $user->password = Hash::make($request->password);

    }
    
    $user->save();

    return $user;
});

//UTILIZADORES - DELETE

Route::middleware('auth:api')->delete('utilizador/{id}', function(Request $request, $id) {
    $id = $id;

    $user = User::findOrFail($id);
    $user->delete();

    return ["Utilizador eliminado com Sucesso!"];
});

//REGISTAR UTILIZADOR


Route::post('/registar', function (Request $request) {

    $data = $request->all();
    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6',
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    }

    /*---------------------------------------------------------------------*/

    $user = User::create([
        'name' => $data['name'],
        'username' => $data['username'],
        'email' => $data['email'],
        'password' => Hash::make($data['password']),
        'department_id' => $data['department_id'],
        'role_id' => $data['role_id'],
        'status' => $data['status']
    ]);

    $user->token = $user->createToken($user->email)->accessToken;

    return $user;   
    


});

//LOGIN DA APP

Route::post('/login', function (Request $request){

    //SISTEMA DE VALIDAÇÃO

    $validacao = Validator::make($request->all(), [
        'email' => 'required|string|email|max:255',
        'password' => 'required|string',
    ]); 

    if($validacao->fails()){
        return $validacao->errors();
    }

    if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
        // Authentication passed...
        $user = Auth()->user();

        $user->token = $user->createToken($user->email)->accessToken;
        $user->department_name = $user->department_obj['name'];
        $user->role_name = $user->role_obj['role'];

        return $user;
    } else{
        return ['erro' => 'Password ou Email incorrectos!'];
    }
});




//UPLOAD FILES PEDIDOS

Route::middleware('auth:api')->post('storepedidofiles/{pedido_id}', function (Request $request, $pedido_id){
    $pedido_id = $pedido_id;

    if($request->hasFile('file')){

        $filename = $request->file->getClientOriginalName();

        $filesize = $request->file->getClientSize();

        $file_ext = $request->file->getClientOriginalExtension();

        $fileurl = $request->file->storeAs('public/upload', $filename);

        $file = TicketFile::create([
            'name' => $filename,
            'size' => $filesize,
            'ticket_id' => $pedido_id,
            'url' => $fileurl,
            'ext' => $file_ext
        ]);

        return response($file)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');
    
    }
});

//FILES PEDIDOS SHOW
Route::middleware('auth:api')->get('storepedidofiles/{pedido_id}', function (Request $request, $pedido_id){
    $pedido_id = $pedido_id;

    $files = TicketFile::All()->where('ticket_id', $pedido_id);


    $returnfiles = [];

    foreach($files as $file){
        array_push($returnfiles, $file);
    }

    return response($returnfiles)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');
    
    
});

//DOWNLOAD DE FICHEIROS
Route::middleware('auth:api')->get('file/{filename}', function (Request $request, $filename){
    
    
    return response()->download( storage_path('app/public/upload/' . $filename) );

    
    
});


//DELETE
Route::middleware('auth:api')->delete('file/{filename}/{file_id}', function (Request $request, $filename, $file_id){
    

    $file_id = $file_id;

    $file = TicketFile::findOrFail($file_id);
    $file->delete();

    if(file_exists(storage_path('app/public/upload/' . $filename))){
        unlink(storage_path('app/public/upload/' . $filename));
    }else{
        return ["Ficheiro não existe"];
    }
    
    return ["Ficheiro Eliminado"];
});









//UPLOAD AVATAR


Route::middleware('auth:api')->post('avatar/{user_id}', function (Request $request, $user_id){
    $user_id = $user_id;

    if($request->hasFile('file')){

        $filename = $request->file->getClientOriginalName();

        $file_ext = $request->file->getClientOriginalExtension();

        $fileurl = $request->file->storeAs('public/upload', $filename);

        if(($file_ext === 'png') || ($file_ext === 'jpg') || ($file_ext === 'jpeg')){
            
            $user = User::findOrFail($user_id);
            $user->image_url = $filename;
            $user->save();
        }

        

        return response($user)
        ->header('Connection', 'Keep-alive')
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Credentials', 'true');
    
    }
});



//DELETE AVATAR
Route::middleware('auth:api')->delete('deleteavatar/{user_id}', function (Request $request, $user_id){
    

    $user_id = $user_id;


    

    $user = User::findOrFail($user_id);

    $url = $user->image_url;

    $path = parse_url($url, PHP_URL_PATH);
    $pathFragments = explode('/', $path);
    $filename = end($pathFragments);

    if($filename === null){
        return ['erro ao cortar url'];
    }

    

    if(file_exists(storage_path('app/public/upload/' . $filename))){
        unlink(storage_path('app/public/upload/' . $filename));

        $user->image_url = null;
        $user->save();
    }else{
        return ['Ficheiro não existe'];
    }
    
    return ['Ficheiro Eliminado:' . $filename];
});





/*
*
* ESTATISTICAS DASHBOARD
*
*/
Route::middleware('auth:api')->get('/estatisticas/carbon', function (Request $request) {

    $hoje = date('Y-m-d');

    $menos7dias = date('Y-m-d',strtotime('-7 days'));
    $tickets = Ticket::whereRaw("created_at >= ? AND created_at <= ?", array($menos7dias." 00:00:00", $hoje." 23:59:59"))->get();


    return $tickets;
});

//PEDIDOS  

Route::middleware('auth:api')->get('/estatisticas/pedidos/{filtro}', function (Request $request, $filtro) {

    $hoje = date('Y-m-d');

    $semanal = date('Y-m-d',strtotime('-7 days'));

    $mensal = date('Y-m-d',strtotime('-30 days'));
    
    if($filtro === 'Sempre'){

        //PEDIDOS

        $total_pedidos = Ticket::All()->whereNotIn('project_id', 27)->count();

        $pendentes_pedidos = Ticket::All()->whereNotIn('project_id', 27)->where('status_id', 1)->count();

        $emcurso_pedidos = Ticket::All()->whereNotIn('project_id', 27)->where('status_id', 2)->count();

        $concluidos_melhorias_pedidos = Ticket::All()->whereNotIn('project_id', 27)->where('status_id', 6)->count();
        $concluidos_concluidos_pedidos = Ticket::All()->whereNotIn('project_id', 27)->where('status_id', 3)->count();
        $concluidos_pedidos = $concluidos_melhorias_pedidos + $concluidos_concluidos_pedidos;

        $inativos_pedidos = Ticket::All()->whereNotIn('project_id', 27)->where('status_id', 4)->count();



        //PROJECTOS

        $total_projectos = Project::All()->whereNotIn('id', 27)->count();

        $pendentes_projectos = Project::All()->whereNotIn('id', 27)->where('status_id', 1)->count();

        $emcurso_projectos = Project::All()->whereNotIn('id', 27)->where('status_id', 2)->count();

        $concluidos_melhorias_projectos = Project::All()->whereNotIn('id', 27)->where('status_id', 6)->count();
        $concluidos_concluidos_projectos = Project::All()->whereNotIn('id', 27)->where('status_id', 3)->count();
        $concluidos_projectos = $concluidos_melhorias_projectos + $concluidos_concluidos_projectos;

        $inativos_projectos = Project::All()->whereNotIn('id', 27)->where('status_id', 4)->count();


        //DIVISÔES

        $divisao_des = Ticket::All()->where('des', 1)->whereNotIn('project_id', 27)->count();
        $divisao_d3p = Ticket::All()->where('d3p', 1)->whereNotIn('project_id', 27)->count();
        $divisao_db3 = Ticket::All()->where('db3', 1)->whereNotIn('project_id', 27)->count();
        $divisao_none = Ticket::All()->where('des', 0)->where('d3p', 0)->where('db3', 0)->count();

    } elseif($filtro === 'Semanal'){

        //PEDIDOS

        $total_pedidos = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->count();

        $pendentes_pedidos = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 1)->count();

        $emcurso_pedidos = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 2)->count();

        $concluidos_melhorias_pedidos = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 6)->count();
        $concluidos_concluidos_pedidos = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 3)->count();
        $concluidos_pedidos = $concluidos_melhorias_pedidos + $concluidos_concluidos_pedidos;


        $inativos_pedidos = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 4)->count();



        //PROJECTOS

        $total_projectos = Project::whereRaw("id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->count();

        $pendentes_projectos = Project::whereRaw("id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 1)->count();

        $emcurso_projectos = Project::whereRaw("id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 2)->count();


        $concluidos_melhorias_projectos = Project::whereRaw("id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 6)->count();
        $concluidos_concluidos_projectos = Project::whereRaw("id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 3)->count();
        $concluidos_projectos = $concluidos_melhorias_projectos + $concluidos_concluidos_projectos;


        $inativos_projectos = Project::whereRaw("id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 4)->count();


        //DIVISÔES
        $divisao_des = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('des', 1)->count();
        $divisao_d3p = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('d3p', 1)->count();
        $divisao_db3 = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('db3', 1)->count();
        $divisao_none = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $semanal." 00:00:00", $hoje." 23:59:59"))->where('des', 0)->where('d3p', 0)->where('db3', 0)->count();


    } elseif($filtro === 'Mensal'){

        //PEDIDOS

        $total_pedidos = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $mensal." 00:00:00", $hoje." 23:59:59"))->count();

        $pendentes_pedidos = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 1)->count();

        $emcurso_pedidos = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 2)->count();

        $concluidos_melhorias_pedidos = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 6)->count();
        $concluidos_concluidos_pedidos = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 3)->count();
        $concluidos_pedidos = $concluidos_melhorias_pedidos + $concluidos_concluidos_pedidos;

        $inativos_pedidos = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 4)->count();



        //PROJECTOS

        $total_projectos = Project::whereRaw("status_id != ? AND id != ? AND updated_at >= ? AND updated_at <= ?", array(6, 27, $mensal." 00:00:00", $hoje." 23:59:59"))->count();

        $pendentes_projectos = Project::whereRaw("status_id != ? AND id != ? AND updated_at >= ? AND updated_at <= ?", array(6, 27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 1)->count();

        $emcurso_projectos = Project::whereRaw("status_id != ? AND id != ? AND updated_at >= ? AND updated_at <= ?", array(6, 27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 2)->count();

        $concluidos_melhorias_projectos = Project::whereRaw("id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 6)->count();
        $concluidos_concluidos_projectos = Project::whereRaw("id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 3)->count();
        $concluidos_projectos = $concluidos_melhorias_projectos + $concluidos_concluidos_projectos;

        $inativos_projectos = Project::whereRaw("status_id != ? AND id != ? AND updated_at >= ? AND updated_at <= ?", array(6, 27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('status_id', 4)->count();


         //DIVISÔES
         $divisao_des = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('des', 1)->count();
         $divisao_d3p = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('d3p', 1)->count();
         $divisao_db3 = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('db3', 1)->count();
         $divisao_none = Ticket::whereRaw("project_id != ? AND updated_at >= ? AND updated_at <= ?", array(27, $mensal." 00:00:00", $hoje." 23:59:59"))->where('des', 0)->where('d3p', 0)->where('db3', 0)->count();
    }

    


    //TOP SOLICITADORES
    if($filtro === 'Sempre'){
    
        $solicitadores = Ticket::select(DB::raw('user_solicitation_id'), DB::raw('count(*) as count'))
        ->groupBy('user_solicitation_id')
        ->orderBy('count', 'desc')
        ->take(5)
        ->get();

    } elseif($filtro === 'Semanal'){

        $solicitadores = Ticket::whereRaw("created_at >= ? AND created_at <= ?", array($semanal." 00:00:00", $hoje." 23:59:59"))->select(DB::raw('user_solicitation_id'), DB::raw('count(*) as count'))
        ->groupBy('user_solicitation_id')
        ->orderBy('count', 'desc')
        ->take(5)
        ->get();

    } elseif($filtro === 'Mensal'){
        $solicitadores = Ticket::whereRaw("created_at >= ? AND created_at <= ?", array($mensal." 00:00:00", $hoje." 23:59:59"))->select(DB::raw('user_solicitation_id'), DB::raw('count(*) as count'))
        ->groupBy('user_solicitation_id')
        ->orderBy('count', 'desc')
        ->take(5)
        ->get();

    }

    $return_solicitadores = [];

    foreach($solicitadores as $solicitador){
        $solicitador_select = User::findOrFail($solicitador->user_solicitation_id);

        $solicitador_select->project_name = $solicitador_select->department_obj['name'];


        $solicitador_select = [
            'name' => $solicitador_select->name,
            'count' => $solicitador->count,
            'department' => $solicitador_select->project_name
        ];

        array_push($return_solicitadores, $solicitador_select);

    }



    //TOP DEPARTAMENTOS

    if($filtro === 'Sempre'){
    
        $tickets = Ticket::All();

    } elseif($filtro === 'Semanal'){

        $tickets = Ticket::whereRaw("created_at >= ? AND created_at <= ?", array($semanal." 00:00:00", $hoje." 23:59:59"))->get();

    } elseif($filtro === 'Mensal'){
        $tickets = Ticket::whereRaw("created_at >= ? AND created_at <= ?", array($mensal." 00:00:00", $hoje." 23:59:59"))->get();

    }
    
    
    
    
    $departments = [];
    
    foreach($tickets as $ticket ){
        $ticket->department_id = $ticket->solicitation_obj['department_id'];

       /*  $department = Department::findOrFail($department_id);

        

        array_push($departments, $department); */

    }

    $departments = $tickets->groupBy('department_id')->sortByDesc('occurrences')->take(5);


    $return_departments = [];
    foreach($departments as $key => $department){

        $department_name = Department::find($key);

        $department = [
            'name' => $department_name->name,
            'count' => $department->count()
        ];

        array_push($return_departments, $department);
    }

    


    $sort = array();
    foreach($return_departments as $k=>$v) {
        $sort['count'][$k] = $v['count'];
    }

    array_multisort($sort['count'], SORT_DESC, $return_departments);




    

    if($filtro === 'Sempre'){


        //MVP SEMPRE
        $mvp = Ticket::select(DB::raw('count(*) as count, worker_id'))
        ->where('status_id', '=', 3)
        ->groupBy('worker_id')
        ->orderBy('count', 'desc')
        ->take(1)
        ->get();

        foreach($mvp as $obj_mvp){
            $obj_mvp->name = $obj_mvp->worker_obj['name'];
        }

    } elseif($filtro === 'Semanal'){

        //MVP SEMANAL

        $mvp = Ticket::select(DB::raw('count(*) as count, worker_id'))->whereRaw("updated_at >= ? AND updated_at <= ?", array($semanal." 00:00:00", $hoje." 23:59:59"))
        ->where('status_id', '=', 3)
        ->groupBy('worker_id')
        ->orderBy('count', 'desc')
        ->take(1)
        ->get();

        foreach($mvp as $obj_mvp){
            $obj_mvp->name = $obj_mvp->worker_obj['name'];
        }

        

    } elseif($filtro === 'Mensal'){

        //MVP MENSAL

        $mvp = Ticket::select(DB::raw('count(*) as count, worker_id'))->whereRaw("updated_at >= ? AND updated_at <= ?", array($mensal." 00:00:00", $hoje." 23:59:59"))
        ->where('status_id', '=', 3)
        ->groupBy('worker_id')
        ->orderBy('count', 'desc')
        ->take(1)
        ->get();

        foreach($mvp as $obj_mvp){
            $obj_mvp->name = $obj_mvp->worker_obj['name'];
        }

    }




    
    
    
    
    
    
    
    //PEDIDOS POR PROGRAMADOR (GRÁFICO DE BARRAS)


    $pedidos_programador = User::All()->where('role_id', 1);

    $return_workers = [];
    $return_pendentes_count = [];
    $return_emcurso_count = [];
    $return_concluidos_count = [];
    $return_inativos_count = [];


    if($filtro === 'Sempre'){


        
        foreach($pedidos_programador as $programador){
            array_push($return_workers, $programador->name);
            array_push($return_pendentes_count, $programador->workering_tickets->where('status_id', 1)->whereNotIn('project_id', 27)->count());
            array_push($return_emcurso_count, $programador->workering_tickets->where('status_id', 2)->whereNotIn('project_id', 27)->count());
            array_push($return_concluidos_count, $programador->workering_tickets->where('status_id', 3)->whereNotIn('project_id', 27)->count());
            array_push($return_inativos_count, $programador->workering_tickets->where('status_id', 4)->whereNotIn('project_id', 27)->count());
        }

    

    } elseif($filtro === 'Semanal'){


        foreach($pedidos_programador as $programador){
            array_push($return_workers, $programador->name);
            array_push($return_pendentes_count, $programador->workering_tickets->where('status_id', 1)->where('updated_at', '>=', $semanal." 00:00:00")->where('updated_at', '<=', $hoje." 23:59:59")->whereNotIn('project_id', 27)->count());
            array_push($return_emcurso_count, $programador->workering_tickets->where('status_id', 2)->where('updated_at', '>=', $semanal." 00:00:00")->where('updated_at', '<=', $hoje." 23:59:59")->whereNotIn('project_id', 27)->count());
            array_push($return_concluidos_count, $programador->workering_tickets->where('status_id', 3)->where('updated_at', '>=', $semanal." 00:00:00")->where('updated_at', '<=', $hoje." 23:59:59")->whereNotIn('project_id', 27)->count());
            array_push($return_inativos_count, $programador->workering_tickets->where('status_id', 4)->where('updated_at', '>=', $semanal." 00:00:00")->where('updated_at', '<=', $hoje." 23:59:59")->whereNotIn('project_id', 27)->count());
         }

        

    } elseif($filtro === 'Mensal'){


        foreach($pedidos_programador as $programador){
            array_push($return_workers, $programador->name);
            array_push($return_pendentes_count, $programador->workering_tickets->where('status_id', 1)->where('updated_at', '>=', $mensal." 00:00:00")->where('updated_at', '<=', $hoje." 23:59:59")->whereNotIn('project_id', 27)->count());
            array_push($return_emcurso_count, $programador->workering_tickets->where('status_id', 2)->where('updated_at', '>=', $mensal." 00:00:00")->where('updated_at', '<=', $hoje." 23:59:59")->whereNotIn('project_id', 27)->count());
            array_push($return_concluidos_count, $programador->workering_tickets->where('status_id', 3)->where('updated_at', '>=', $mensal." 00:00:00")->where('updated_at', '<=', $hoje." 23:59:59")->whereNotIn('project_id', 27)->count());
            array_push($return_inativos_count, $programador->workering_tickets->where('status_id', 4)->where('updated_at', '>=', $mensal." 00:00:00")->where('updated_at', '<=', $hoje." 23:59:59")->whereNotIn('project_id', 27)->count());
         }

       

    }




    
    $estatisticas = [
        'total_pedidos' => $total_pedidos,
        'pendentes_pedidos' => $pendentes_pedidos,
        'emcurso_pedidos' => $emcurso_pedidos,
        'concluidos_pedidos' => $concluidos_pedidos,
        'inativos_pedidos' => $inativos_pedidos,
        'total_projectos' => $total_projectos,
        'pendentes_projectos' => $pendentes_projectos,
        'emcurso_projectos' => $emcurso_projectos,
        'concluidos_projectos' => $concluidos_projectos,
        'inativos_projectos' => $inativos_projectos,
        'divisao_des' => $divisao_des,
        'divisao_d3p' => $divisao_d3p,
        'divisao_db3' => $divisao_db3,
        'divisao_none' => $divisao_none,
        'top_solicitadores' => $return_solicitadores,
        'top_departamentos' => $return_departments,
        'mvp' => $mvp,
        'grafico_pedidos' => [
            'workers' => $return_workers,
            'pendentes' => [
                'data' => $return_pendentes_count,
                'label' => 'Pendentes'
            ],
            'emcurso' => [
                'data' => $return_emcurso_count,
                'label' => 'Em Curso'
            ],
            'concluidos' => [
                'data' => $return_concluidos_count,
                'label' => 'Concluidos'
            ],
            'inativos' => [
                'data' => $return_inativos_count,
                'label' => 'Inativos'
            ]
        ]
        
    ];



    return response($estatisticas)
    ->header('Connection', 'Keep-alive')
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Credentials', 'true');
});