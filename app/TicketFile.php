<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketFile extends Model
{
    //RELACIONAMENTO

    public function ticket_obj()
    {
        return $this->belongsTo('App\Ticket','ticket_id');
    }


    protected $table = 'ticket_files';

    protected $fillable = [
        'name', 'size', 'ticket_id', 'url', 'ext'
    ];
}
