<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

    public function users()
    {
        return $this->hasMany('App\User');
    }

    protected $table = 'Departments';

    protected $fillable = [
        'id', 'name'
    ];

}
