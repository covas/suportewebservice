<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable
{
    use  HasApiTokens, Notifiable;

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }


    public function workering_tickets()
    {
        return $this->hasMany('App\Ticket', 'worker_id');
    }

    public function department_obj() {
        return $this->belongsTo('App\Department','department_id');
    }

    public function role_obj() {
        return $this->belongsTo('App\Role','role_id');
    }

    protected $table = 'users';
    
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'department_id', 'status', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
