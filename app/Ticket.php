<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{



    public function worker_obj() {
        return $this->belongsTo('App\User','worker_id');
    }

    public function user_obj() {
        return $this->belongsTo('App\User','user_id');
    }

    public function solicitation_obj() {
        return $this->belongsTo('App\User', 'user_solicitation_id');
    }

    public function priority_obj() {
        return $this->belongsTo('App\Priority','priority_id');
    }

    

    public function project_obj() {
        return $this->belongsTo('App\Project','project_id');
    }

    public function status_obj() {
        return $this->belongsTo('App\Status','status_id');
    }


    
    protected $table = 'tickets';

    protected $fillable = [
        'name', 'description', 'project_id', 'user_id', 'status_id', 'user_solicitation_id', 'priority_id', 'worker_id', 'start', 'prevend', 'end', 'des', 'd3p', 'db3', 'created_at', 'updated_at'
    ];
}
