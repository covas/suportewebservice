<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteTicket extends Model
{
    protected $table = 'note_tickets';

    public function user_obj() {
        return $this->belongsTo('App\User','user_id');
    }

    protected $fillable = [
        'id', 'ticket_id', 'user_id', 'note', 'public', 'created_at', 'updated_at'
    ];
}
