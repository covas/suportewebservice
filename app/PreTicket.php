<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreTicket extends Model
{

    public function solicitation_obj() {
        return $this->belongsTo('App\User', 'user_solicitation_id');
    }

    public function priority_obj() {
        return $this->belongsTo('App\Priority','priority_id');
    }

    public function ticket_obj() {
        return $this->belongsTo('App\Ticket','ticket_id');
    }


    
    protected $table = 'pre_tickets';

    protected $fillable = [
        'name', 'description', 'user_solicitation_id', 'priority_id', 'ticket_id', 'created_at', 'updated_at'
    ];
}
