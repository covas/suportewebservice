<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProject extends Model
{

    //RELACIONAMENTO
    
    public function user_obj()
    {
        return $this->belongsTo('App\User','user_id');
    }


    public function project_obj()
    {
        return $this->belongsTo('App\Project','project_id');
    }


    protected $table = 'user_projects';

    protected $fillable = [
        'project_id', 'user_id'
    ];
    
}
