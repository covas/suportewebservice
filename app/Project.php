<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    public function status_obj() {
        return $this->belongsTo('App\Status','status_id');
    }

    public function user_projects() {
        return $this->hasMany('App\UserProject');
    }


    
    protected $table = 'projects';

    protected $fillable = [
        'name', 'description', 'time', 'status_id', 'evolution', 'start', 'end', 'created_at', 'updated_at'
    ];

}
