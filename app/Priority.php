<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    protected $table = 'prioritys';

    protected $fillable = [
        'id', 'name', 'icon'
    ];

}
