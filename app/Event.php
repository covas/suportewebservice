<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function user_obj() {
        return $this->belongsTo('App\User','user_id');
    }

    public function project_obj() {
        return $this->belongsTo('App\Project','project_id');
    }

    public function ticket_obj() {
        return $this->belongsTo('App\Ticket','ticket_id');
    }

    protected $table = 'events';

    protected $fillable = [
        'id', 'title', 'start', 'end', 'user_id', 'project_id', 'ticket_id', 'created_at'
    ];


}
